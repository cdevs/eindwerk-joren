/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "Queue.h"
#include "Simulator.h"

using namespace cdevs;
using namespace cdevs_examples;

bool TerminateWhenStateIsReached(DevsTime clock, BaseDevs * model)
{
	return false;
}

int main(int argc, char * argv[])
{
	BaseDevs * model = new Queue(2);

	Simulator sim(model);

	// TODO: give termination time priority to condition?
	// sim.setTerminationCondition(std::function<bool(DevsTime, BaseDevs *)>(TerminateWhenStateIsReached));

	sim.setTerminationTime(5.0);

	 sim.setVerbose();
	// sim.setXml("classic.xml");
	// sim.setJson("classic.json");

	sim.setClassicDEVS();

	sim.Simulate();

	delete model;

	return 0;
}
