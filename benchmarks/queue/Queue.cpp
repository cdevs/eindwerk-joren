/*
 * Queue.cpp
 *
 *  Created on: May 21, 2015
 *      Author: lotte
 */

#include "Queue.h"

double cdevs_examples::getRandom(int randomSeed)
{
	return 0.5;
	long int seed = randomSeed ? randomSeed : rand();
	seed = (1664525 * seed + 1013904223) % 4294967296;
	return float(int(float(seed) / 4294967296 * 1000)) / 1000;
}

cdevs_examples::Event::Event(unsigned eventSize)
	: eventSize_(eventSize)
{
}

cdevs_examples::ProcessorState::ProcessorState()
	: State(""), event1Counter_(std::numeric_limits<double>::infinity()), event1_(), queue_()
{
}

cdevs_examples::Processor::Processor(std::string name)
	: cdevs::AtomicDevs(name)
{
	state_ = new ProcessorState();
	recv_event1_ = AddInPort("in_event1");
	send_event1_ = AddOutPort("out_event1");
}

double cdevs_examples::Processor::TimeAdvance()
{
	return dynamic_cast<ProcessorState*>(state_)->event1Counter_;
}

cdevs::State* cdevs_examples::Processor::IntTransition()
{
	ProcessorState* state = dynamic_cast<ProcessorState*>(state_);
	if(state->event1Counter_ == std::numeric_limits<double>::infinity())
		return state_;
	state->event1Counter_ -= TimeAdvance();
	if (state->event1Counter_ == 0 && state->queue_.empty()) {
		state->event1Counter_ = std::numeric_limits<double>::infinity();
		state->event1_ = Event();
	} else {
		state->event1Counter_ = getRandom();
		state->event1_ = state->queue_.front();
		state->queue_.pop_front();
	}
	return state_;
}

cdevs::State* cdevs_examples::Processor::ExtTransition(std::map<cdevs::Port*, std::list<std::string> > inputs)
{
	ProcessorState* state = dynamic_cast<ProcessorState*>(state_);
	state->event1Counter_ -= elapsed_;
	for (std::string ev : inputs.at(recv_event1_)) {
		Event event = Event(std::atoi(ev.c_str()));
		if (state->event1_.eventSize_ == -1) {
			state->event1_ = event;
			state->event1Counter_ = getRandom();
		} else {
			state->queue_.push_front(event);
		}
	}
	return state_;
}

std::map<cdevs::Port*, std::list<std::string> > cdevs_examples::Processor::OutputFunction()
{
	std::map<cdevs::Port*, std::list<std::string> > out;
	std::list<std::string> strs;
	strs.push_back(std::to_string(dynamic_cast<ProcessorState*>(state_)->event1_.eventSize_));
	out.insert(std::make_pair(send_event1_, strs));
	return out;
}

cdevs_examples::Generator::Generator()
	: cdevs::AtomicDevs("Generator")
{
	state_ = new GeneratorState("gen_event1");
	std::cout << "State: " << state_->string() << std::endl;
	send_event1_ = AddOutPort("out_event1");
}

double cdevs_examples::Generator::TimeAdvance()
{
	return 1.0;
}

cdevs::State* cdevs_examples::Generator::IntTransition()
{
	return state_;
}

cdevs::State* cdevs_examples::Generator::ExtTransition(std::map<cdevs::Port*, std::list<std::string> > inputs)
{
}

std::map<cdevs::Port*, std::list<std::string> > cdevs_examples::Generator::OutputFunction()
{
	std::map<cdevs::Port*, std::list<std::string> > out;
	std::list<std::string> strs;
	strs.push_back("1");
	out.insert(std::make_pair(send_event1_, strs));
	return out;
}

cdevs_examples::Queue::Queue(unsigned width)
	: cdevs::CoupledDevs("Queue")
{
	generator_ = dynamic_cast<Generator*>(AddSubModel(new Generator()));
	Processor * prev;
	std::string name = "Processor0";
	Processor* m = dynamic_cast<Processor*>(AddSubModel(new Processor(name)));
	ConnectPorts(generator_->send_event1_, m->recv_event1_);
	prev = m;
	for (unsigned i = 1; i < width; i++) {
		name = "Processor" + std::to_string(i);
		m = dynamic_cast<Processor*>(AddSubModel(new Processor(name)));
		ConnectPorts(prev->send_event1_, m->recv_event1_);
		prev = m;
	}
}

cdevs::BaseDevs* cdevs_examples::Queue::Select(std::list<cdevs::BaseDevs*> imm_children) const
{
}
/*
 extern "C" {
 #if defined(_MSC_VER) // Microsoft compiler
 _declspec(dllexport)
 #endif
 cdevs_examples::Queue * construct()
 {
 return new cdevs_examples::Queue("queue");
 }*/
