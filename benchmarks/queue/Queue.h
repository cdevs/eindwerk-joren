#ifndef BENCHMARKS_QUEUE_H_
#define BENCHMARKS_QUEUE_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"
#include <limits>
#include <cstdlib>
#include <sstream>

namespace cdevs_examples {

double getRandom(int randomSeed = 0);

class Event
{
public:
	Event(unsigned eventSize = -1);
	unsigned eventSize_;
};

class ProcessorState: public cdevs::State
{
	friend class Processor;
public:
	ProcessorState();
	virtual ~ProcessorState(){}
	std::string string() const
	{
		return std::to_string(event1Counter_);
	}
	std::string toXML() const
	{
		return "";
	}
	std::string toJson() const
	{
		return "";
	}
protected:
	double event1Counter_;
	Event event1_;
	std::list<Event> queue_;
};

class Processor: public cdevs::AtomicDevs
{
	friend class Queue;
public:
	Processor(std::string name);
	virtual ~Processor(){}
	cdevs::State * ExtTransition(std::map<cdevs::Port *, std::list<std::string> > inputs);
	cdevs::State * IntTransition();

	std::map<cdevs::Port *, std::list<std::string> > OutputFunction();

	double TimeAdvance();
private:
	cdevs::Port * recv_event1_;
	cdevs::Port * send_event1_;
};

class GeneratorState: public cdevs::State
{
	friend class Generator;
public:
	GeneratorState(std::string str):str_(str){}
	virtual ~GeneratorState(){}
	std::string string() const
	{
		return str_;
	}
	std::string toXML() const
	{
		return "";
	}
	std::string toJson() const
	{
		return "";
	}
protected:
	std::string str_;
};

class Generator: public cdevs::AtomicDevs
{
	friend class Queue;
public:
	Generator();
	virtual ~Generator(){}
	cdevs::State * ExtTransition(std::map<cdevs::Port *, std::list<std::string> > inputs);
	cdevs::State * IntTransition();

	std::map<cdevs::Port *, std::list<std::string> > OutputFunction();

	double TimeAdvance();
private:
	cdevs::Port * send_event1_;
};

class Queue: public cdevs::CoupledDevs
{
public:
	Queue(unsigned width = 1);
	virtual ~Queue(){}
	Queue(std::string width)
		: Queue(std::atoi(width.c_str()))
	{
	}
	cdevs::BaseDevs * Select(std::list<BaseDevs *> imm_children) const; // TODO: list?
private:
	Generator * generator_;
	Processor * processor_;
};

} /* namespace cdevs_examples */

#endif /* BENCHMARKS_QUEUE_H_ */
