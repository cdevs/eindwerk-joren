#include "PHOLD.h"
#include <chrono>
#include <thread>

cdevs_examples::PHOLDModelState::PHOLDModelState()
	: events_()
{
}

cdevs_examples::PHOLDModelState::~PHOLDModelState()
{
}

double cdevs_examples::getProcTime(int event)
{
	 unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed1);
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	return distribution(generator);
}

int cdevs_examples::getRand(int event)
{
	 unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed1);
	std::uniform_int_distribution<int> distribution(0, 60000);
	return distribution(generator);
}

int cdevs_examples::getNextDestination(int event, int nodenum, std::vector<int> local, std::vector<int> remote,
        double percentageremote)
{
	 unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed1);
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	if (distribution(generator) > percentageremote || remote.empty()) {
		std::uniform_int_distribution<int> distribution2(0, local.size()-1);
		return local.at(distribution2(generator));
	} else {
		std::uniform_int_distribution<int> distribution2(0, remote.size()-1);
		return remote.at(distribution2(generator));
	}
}

cdevs_examples::HeavyPHOLDProcessor::HeavyPHOLDProcessor(std::string name, int iterations, int totalAtomics,
        int modelnumber, std::vector<int> local, std::vector<int> remote, double percentageremotes)
	: cdevs::AtomicDevs(name), percentageRemote_(percentageremotes), outports_(), totalAtomics_(totalAtomics), modelnumber_(
	        modelnumber), iterations_(iterations), local_(local), remote_(remote)
{
	inport_ = AddInPort("inport");
	state_ = new PHOLDModelState();
	for (unsigned i = 0; i < totalAtomics; i++) {
		std::string name = "outport_" + std::to_string(i);
		outports_.push_back(AddOutPort(name));
	}
	dynamic_cast<PHOLDModelState*>(state_)->events_.push_back(
	        std::make_pair(modelnumber, getProcTime(modelnumber)));
	std::cout << "End Constructor" << dynamic_cast<PHOLDModelState*>(state_)->events_.size() << std::endl;
}

double cdevs_examples::HeavyPHOLDProcessor::TimeAdvance()
{
	PHOLDModelState* state = dynamic_cast<PHOLDModelState*>(state_);
	if (!state->events_.empty()) {
		return state->events_.front().second;
	} else {
		return std::numeric_limits<int>::infinity();
	}
}

cdevs::State* cdevs_examples::HeavyPHOLDProcessor::ExtTransition(std::map<cdevs::Port*, std::list<std::string> > inputs)
{
	PHOLDModelState* state = dynamic_cast<PHOLDModelState*>(state_);
	if (!state->events_.empty()) {
		state->events_.front().second -= elapsed_;
	}
	for (std::string i : inputs.at(inport_)) {
		int j = std::atoi(i.c_str());
		state->events_.push_back(std::make_pair(j, getProcTime(j)));
		for (unsigned k = 0; k < iterations_; k++) {
			std::chrono::milliseconds timespan(5); // or whatever

			std::this_thread::sleep_for(timespan);; //TODO: does this keep the process busy?
		}
	}
	std::cout<<"Time elapsed: "<<elapsed_<<std::cout;
	return state_;
}

cdevs::State* cdevs_examples::HeavyPHOLDProcessor::IntTransition()
{
	PHOLDModelState* p = dynamic_cast<PHOLDModelState*>(state_);

	if (p->events_.empty())
		return state_;
	//std::cout << "Events is empty!" << std::endl;
	else
		p->events_.pop_front();
	std::cout<<"Time elapsed: "<<elapsed_<<std::cout;
	return state_;
}

cdevs::State* cdevs_examples::HeavyPHOLDProcessor::ConfTransition(
        std::map<cdevs::Port*, std::list<std::string> > inputs)
{
	PHOLDModelState* state = dynamic_cast<PHOLDModelState*>(state_);
	if (state->events_.size() > 1) {
		state->events_.pop_front();
	} else {
		state->events_.clear();
	}
	for (std::string& i : inputs.at(inport_)) {
		int j = std::atoi(i.c_str());
		state->events_.push_back(std::make_pair(j, getProcTime(j)));
		for (unsigned k = 0; k < iterations_; k++) {
			continue; //TODO: does this keep the process busy?
		}
	}
	return state_;
}

std::map<cdevs::Port*, std::list<std::string> > cdevs_examples::HeavyPHOLDProcessor::OutputFunction()
{
	PHOLDModelState* state = dynamic_cast<PHOLDModelState*>(state_);
	std::map<cdevs::Port*, std::list<std::string> > out;
	if (!state->events_.empty()) {
		auto i = state->events_.front();
		int next = getNextDestination(i.first, modelnumber_, local_, remote_, percentageRemote_);
		std::cout << next << std::endl;
		std::list<std::string> str;
		str.push_back(std::to_string(getRand(i.first)));
		out.insert(std::make_pair(outports_.at(next), str));
	}
	return out;
}

cdevs_examples::PHOLD::PHOLD(int nodes, int atomicsPerNode, int iterations, double percentageremotes)
	: cdevs::CoupledDevs("PHOLD"), processors_()
{
	distributed_ = false;
	int have = 0;
	int cntr = 0;
	int totalAtomics = nodes * atomicsPerNode;
	std::vector<std::vector<int> > procs;
	for (unsigned node = 0; node < nodes; node++) {
		std::vector<int> l = std::vector<int>();
		for (unsigned i = 0; i < atomicsPerNode; i++) {
			l.push_back(atomicsPerNode * node + i);
		}
		procs.push_back(l);
	}
	unsigned e = 0;
	for (auto i : procs) {
		std::vector<int> allnoi;
		unsigned e2 = 0;
		for (auto j : procs) {
			if (e2 != e) {
				for (int el : j) {
					allnoi.push_back(el);
				}
			}
			++e2;
		}
		for (auto j : i) {
			std::vector<int> inoj = std::vector<int>(i);
			inoj.erase(std::remove(inoj.begin(), inoj.end(), j), inoj.end());
			std::string name = "Processor_" + std::to_string(cntr);
			HeavyPHOLDProcessor* m = dynamic_cast<HeavyPHOLDProcessor*>(AddSubModel(
			        (new HeavyPHOLDProcessor(name, iterations, totalAtomics, cntr, inoj, allnoi,
			                percentageremotes)), (distributed_ ? e : 0)));
			processors_.push_back(m);
			std::cout << dynamic_cast<PHOLDModelState*>(m->state_)->events_.size() << std::endl;
			++cntr;
		}
		++e;
	}
	for (unsigned i = 0; i < processors_.size(); i++) {
		for (unsigned j = 0; j < processors_.size(); j++) {
			if (i == j) {
				continue;
			}else{
			ConnectPorts(processors_.at(i)->output_ports_.at(j), processors_.at(j)->inport_);
			std::cout << "After connect"
			        << dynamic_cast<PHOLDModelState*>(processors_.at(i)->state_)->events_.size()
			        << std::endl;
			}
		}
	}

}

cdevs_examples::PHOLD::~PHOLD()
{
}
