#ifndef BENCHMARKS_PHOLD_H_
#define BENCHMARKS_PHOLD_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"
#include <utility>
#include <string>
#include <list>
#include <random>
#include <limits>
#include <algorithm>

namespace cdevs_examples {

class PHOLDModelState: public cdevs::State
{
	friend class HeavyPHOLDProcessor;
public:
	PHOLDModelState();
	virtual ~PHOLDModelState();
	std::string string() const
	{
		std::string t = "";
		if (events_.empty())
			return "";
		for (auto p : events_) {
			t += "\n\t\t" + std::to_string(p.first) + "\t" + std::to_string(p.second);
		}
		return t;
	}
	std::string toXML() const
	{
	}
	std::string toJson() const
	{
	}
//protected:
	std::list<std::pair<int, double> > events_;
};

double getProcTime(int event);
int getNextDestination(int event, int nodenum, std::vector<int> local, std::vector<int> remote,
        double percentageremote);
int getRand(int event);

class HeavyPHOLDProcessor: public cdevs::AtomicDevs
{
	friend class PHOLD;
public:
	HeavyPHOLDProcessor(std::string name, int iterations, int totalAtomics, int modelnumber, std::vector<int> local,
	        std::vector<int> remote, double percentageremotes);
	cdevs::State * ExtTransition(std::map<cdevs::Port *, std::list<std::string> > inputs);
	cdevs::State * IntTransition();
	cdevs::State * ConfTransition(std::map<cdevs::Port *, std::list<std::string> > inputs);
	std::map<cdevs::Port *, std::list<std::string> > OutputFunction();

	double TimeAdvance();
protected:
	cdevs::Port * inport_;
	double percentageRemote_;
	std::vector<cdevs::Port*> outports_;
	int totalAtomics_;
	int modelnumber_;
	int iterations_;
	std::vector<int> local_;
	std::vector<int> remote_;
};

class PHOLD: public cdevs::CoupledDevs
{
public:
	PHOLD(int nodes, int atomicsPerNode, int iterations, double percentageremotes);
	virtual ~PHOLD();
protected:
	std::vector<HeavyPHOLDProcessor*> processors_;
	bool distributed_;
};

}

#endif /* BENCHMARKS_PHOLD_H_ */
