/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "PHOLD.h"
#include "Simulator.h"

using namespace cdevs;
using namespace cdevs_examples;

bool TerminateWhenStateIsReached(DevsTime clock, BaseDevs * model)
{
	return false;
}

int main(int argc, char * argv[])
{
	BaseDevs * model = new PHOLD(2, 2, 5,0);

	Simulator sim(model);

	// TODO: give termination time priority to condition?
	// sim.setTerminationCondition(std::function<bool(DevsTime, BaseDevs *)>(TerminateWhenStateIsReached));

	sim.setTerminationTime(100.0);

	 sim.setVerbose("out.txt");
	 //sim.setXml("classic.xml");
	 //sim.setJson("classic.json");

	sim.setClassicDEVS();

	sim.Simulate();

	delete model;

	return 0;
}
