#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <queue>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <functional>

class ThreadPool;

/**
 * Thread executing tasks from a given tasks queue
 */
class Worker {
public:
	Worker(ThreadPool*);
	~Worker();

	void operator()();

private:
	ThreadPool* _threadPool;
};

/**
 * Pool of threads consuming tasks from a queue
 */
class ThreadPool {
public:
	ThreadPool(size_t);
	virtual ~ThreadPool();

	template<class F>
	void AddTask(F f);
	std::function< void() > PopTask();

	void Wait(std::unique_lock< std::mutex >&);

	std::mutex& GetMutexReference();

	bool IsStopping();
	bool HasTasks();

	void SetStartTime(std::chrono::steady_clock::time_point);
private:
	std::vector< std::thread > workers_;
	std::queue< std::function< void() > > tasks_;

	std::mutex queue_mutex_;
	std::condition_variable condition_;

	std::chrono::steady_clock::time_point start_time_;

	bool stop_;
};


#endif /* THREADPOOL_H_ */
