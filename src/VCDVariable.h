#ifndef VCDVARIABLE_H
#define VCDVARIABLE_H

#include <string>

namespace cdevs {

class VcdVariable
{
public:
	VcdVariable(const std::string& model_name, const std::string& port_name);

	void set_model_name(std::string model_name);
	void set_port_name(std::string port_name);

	const std::string model_name();
	const std::string port_name();
private:
	std::string model_name_;
	std::string port_name_;
};

}
#endif
