#include "MessageScheduler.h"

namespace cdevs
{
/**
 * Constructor
 */
MessageScheduler::MessageScheduler()
{
}

/**
 * Destructor
 */
MessageScheduler::~MessageScheduler()
{
	while (!message_queue_.empty()) {
		Message * msg = message_queue_.top();
		message_queue_.pop();
		delete msg;
	}
	for (Message * msg: message_invalids_)
		delete msg;
	message_invalids_.clear();
	for (Message * msg: message_processed_)
		delete msg;
	message_processed_.clear();
}

/**
 * Insert several messages that were created elsewhere and merge them in.
 *
 * @param extraction: the output of the extract method on the other message scheduler
 * @param model_list: models that are inserted and for which extraction happened
 */
void MessageScheduler::Insert(std::vector<Message *> messages, std::vector<Model> models)
{
	throw RuntimeDevsException("MessageScheduler::Insert not implemented yet!");
}

/**
 * Extract messages from the message scheduler for when a model gets removed from this kernel.
 *
 * @param model_ids: iterable of model_ids of models that will be removed from this node
 * @return tuple -- extraction that needs to be passed to the insert method of another scheduler
 * */
void MessageScheduler::Extract(std::vector<int> modelIds)
{
	throw RuntimeDevsException("MessageScheduler::Extract not implemented yet!");
}

/**
 * Schedule a message for processing
 *
 * @param msg: the message to schedule
 */
void MessageScheduler::Schedule(Message * message)
{
	// remove it from invalidated messages
	auto it = message_invalids_.find(message);
	if(it != message_invalids_.end())
		message_invalids_.erase(it);

	// push it to the queue
	message_queue_.push(message);
}

/**
 * Unschedule several messages, this way it will no longer be processed.
 * @param uuids: iterable of UUIDs that need to be removed
 */
void MessageScheduler::MassUnschedule(std::list<Message *> messages)
{
	for(Message * msg : messages)
	{
		message_invalids_.insert(msg);
	}
}

/**
 * Returns the first (valid) message. Not necessarily O(1), as it could be
 * the case that a lot of invalid messages are still to be deleted.
 *
 * @return First message
 */
Message * MessageScheduler::ReadFirst()
{
	CleanFirst();

	if(message_queue_.empty())
		return 0;

	Message * msg = message_queue_.top();
	return msg;
}

/**
 * Notify that the first (valid) message is processed.
 *
 * @return msg -- the next first message that is valid
 */
void MessageScheduler::RemoveFirst()
{
	CleanFirst();

	if(message_queue_.empty())
		return;

	Message * message = message_queue_.top();
	message_queue_.pop();

	if(message != 0)
		message_processed_.push_back(message);
}

/**
 * Notify that the first (valid) message must be removed
 *
 * @return msg -- the next first message that is valid
 */
void MessageScheduler::PurgeFirst()
{
	CleanFirst();
	Message * msg = message_queue_.top();
	delete msg;
	message_queue_.pop();
}

/**
 * Clean all invalid messages at the front of the list.
 * Method MUST be called before any accesses should happen to the first element,
 * otherwise this first element might be a message that was just invalidated
 */
void MessageScheduler::CleanFirst()
{
	if(message_queue_.empty())
		return;

	// get the top message
	Message * message = message_queue_.top();

	if(message == 0)
		throw RuntimeDevsException("MessageScheduler::CleanFirst Got null pointer for message");

	// check if it is invalidated
	auto it = message_invalids_.find(message);

	// if it is in our invalids container, that means we can simply pop it off
	if(it != message_invalids_.end()) {
		delete *it;
		message_invalids_.erase(it);
		message_queue_.pop();
	}
}

/**
 * Revert the inputqueue to the specified time, will also clean up the list of processed elements
 * @param time Time to which revertion should happen
 */
void MessageScheduler::Revert(DevsTime targetTime)
{
	std::vector<Message *> incoming_messages;

	for(Message * message : message_processed_)
	{
		if(message->getTimeStamp() < targetTime)
			message_queue_.push(message);
		else
			incoming_messages.push_back(message);
	}

	message_processed_ = incoming_messages;

}
}
