#ifndef LOGGER_H_
#define LOGGER_H_

#include <string>

namespace cdevs {

class Logger { //TODO

void SetLogger(int loglevel);
bool Log(int level, std::string msg);
bool Debug(std::string msg);
bool Info(std::string msg);
bool Warn(std::string msg);
bool Error(std::string msg);

};

}

#endif /* LOGGER_H_ */
