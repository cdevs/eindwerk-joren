/*
 * SchedulerML.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerML.h"
#include <iostream>
#include <limits>

namespace cdevs {

SchedulerML::SchedulerML(std::list<std::shared_ptr<AtomicDevs> > models, float epsilon, unsigned int totalModels)
{
	this->models_ = models;
	this->epsilon_ = epsilon;
	this->totalmodels_ = totalModels;
}

SchedulerML::~SchedulerML()
{
	models_.clear();
}

void SchedulerML::Schedule(std::shared_ptr<AtomicDevs> model)
{
	if (model->time_next_ < this->minval_){
		this->minval_ = model->time_next_;
	}
}

void SchedulerML::Unschedule(std::shared_ptr<AtomicDevs> model)
{
	std::list<std::shared_ptr<AtomicDevs> >::iterator i = models_.begin();
	while (i != models_.end()){
		if (*i == model){
			i = models_.erase(i); //reseat iterator
		}
		else
		{
			++i;
		}
	}
	if (model->time_next_ == this->minval_){
		this->minval_.setX(std::numeric_limits<double>::infinity());
		this->minval_.setY(std::numeric_limits<double>::infinity());
		for ( auto& m : models_){
			if (m->time_next_ < this->minval_){
				this->minval_ = m->time_next_;
			}
		}
	}
}

void SchedulerML::MassReschedule(std::list<std::shared_ptr<AtomicDevs> > reschedule_set)
{
	this->minval_.setX(std::numeric_limits<double>::infinity());
	this->minval_.setY(std::numeric_limits<double>::infinity());
	for( auto& m : models_ ){
		if (m->time_next_ < this->minval_){
			this->minval_ = m->time_next_;
		}
	}
}

DevsTime SchedulerML::ReadFirst()
{
	return this->minval_;
}

std::list<std::shared_ptr<AtomicDevs> > SchedulerML::GetImminent(DevsTime time)
{
	std::list<std::shared_ptr<AtomicDevs> > b;
	for (auto& m : models_){
		if((fabs(m->time_next_.getX() - time.getX()) < this->epsilon_)
			&& m->time_next_.getY() == time.getY()){
			b.push_back(m);
			}
	}
	return b;
}

double SchedulerML::getEpsilon() const
{
	return epsilon_;
}

void SchedulerML::setEpsilon(double epsilon)
{
	epsilon_ = epsilon;
}

int SchedulerML::getTotalmodels() const
{
	return totalmodels_;
}

void SchedulerML::setTotalmodels(int totalmodels)
{
	totalmodels_ = totalmodels;
}

}
