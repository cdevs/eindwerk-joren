/*
 * SchedulerML_test.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerML.h"
#include "gtest/gtest.h"
#include "atomicmodel_test/model.h"

class SchedulerML_test : public ::testing::Test {
protected:
  virtual void SetUp() {
  }
  virtual void TearDown() {
    // Code here will be called immediately after each test
    // (right before the destructor).
  }



};

TEST_F(SchedulerML_test,Constructor_test){
	std::shared_ptr<cdevs::AtomicDevs> b1 = cdevs_examples::atomic::TrafficLight::create("Model1");
	std::shared_ptr<cdevs::AtomicDevs> b2 = cdevs_examples::atomic::TrafficLight::create("Model2");
	std::shared_ptr<cdevs::AtomicDevs> b3 = cdevs_examples::atomic::TrafficLight::create("Model3");

	std::list<std::shared_ptr<cdevs::AtomicDevs> > m = {b1, b2, b3};

	cdevs::SchedulerML myscheduler(m, 0.001, 42);

	EXPECT_NEAR(myscheduler.getEpsilon(), 0.001, 0.0005); //FP Error?
	EXPECT_EQ(myscheduler.getTotalmodels(), 42);
}

TEST_F(SchedulerML_test, ReadFirst_MassReschedule_test){
	std::shared_ptr<cdevs::AtomicDevs> b1 = cdevs_examples::atomic::TrafficLight::create("Model1");
	std::shared_ptr<cdevs::AtomicDevs> b2 = cdevs_examples::atomic::TrafficLight::create("Model2");
	std::shared_ptr<cdevs::AtomicDevs> b3 = cdevs_examples::atomic::TrafficLight::create("Model3");
	cdevs::DevsTime time1(1,0), time2(2,0), time3(3,0);
	b1->setTimeNext(time1);
	b2->setTimeNext(time2);
	b3->setTimeNext(time3);
	std::list<std::shared_ptr<cdevs::AtomicDevs> > m = {b1, b2, b3};
	cdevs::SchedulerML myscheduler(m, 0.001, 42);
	myscheduler.MassReschedule(m);
	EXPECT_EQ(myscheduler.ReadFirst(), time1);
}
