/*
 * SchedulerSL.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULERSL_H_
#define SCHEDULERSL_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"
#include <vector>


namespace cdevs {

/**
 * The Sorted List scheduler is the simplest scheduler available, though it has
 * extremely bad performance in several situations.
 * It simply keeps a list of all models, which is sorted on time_next. No
 * operations have any influence on this heap itself, as there is no real
 * internal representation. As soon as the imminent models are requested,
 * this list is sorted again and the first elements are returned.
 */
class SchedulerSL : public Scheduler
{
public:
	SchedulerSL();
	SchedulerSL(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels);
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	virtual ~SchedulerSL();
};

} /* namespace ns_DEVS */
#endif /* SCHEDULERSL_H_ */
