/*
 * SchedulerNA.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULERNA_H_
#define SCHEDULERNA_H_

#include "../schedulers/SchedulerHS.h"
#include "../AtomicDevs.h"
#include <vector>

namespace cdevs {

/**
 * The No Age scheduler is based on the Heapset scheduler, though it does not
 * take age into account.
 *
 * .. warning:: This scheduler does not take the age into account, making it
 * **unusable** in simulations where the *timeAdvance* function can return
 * (exactly) 0. If unsure, do **not** use this scheduler, but the more general
 * Heapset scheduler.
 *
 * The heap will contain only the timestamps of events that should happen.
 * One of the dictionaries will contain the actual models that transition at
 * the specified time. The second dictionary than contains a reverse relation:
 * it maps the models to their time_next. This reverse relation is necessary to
 * know the *old* time_next value of the model. Because as soon as the model has
 * its time_next changed, its previously scheduled time will be unknown. This
 * 'previous time' is **not** equal to the *timeLast*, as it might be possible
 * that the models wait time was interrupted.
 *
 * For a schedule, the model is added to the dictionary at the specified time_next.
 * In case it is the first element at this location in the dictionary, we also add
 * the timestamp to the heap. This way, the heap only contains *unique* timestamps
 * and thus the actual complexity is reduced to the number of *different* timestamps.
 * Furthermore, the reverse relation is also updated.
 *
 * Unscheduling is done similarly by simply removing the element from the dictionary.
 *
 * Rescheduling is a slight optimisation of unscheduling, followed by scheduling.
 *
 * This scheduler does still schedule models that are inactive (their time_next
 * is infinity), though this does not influence the complexity. The complexity is
 * not affected due to infinity being a single element in the heap that is always
 * present. Since a heap has O(log(n)) complexity, this one additional element
 * does not have a serious impact.
 *
 * The main advantage over the Activity Heap is that it never gets dirty and thus
 * doesn't require periodical cleanup. The only part that gets dirty is the actual
 * heap, which only contains small tuples. Duplicates of these will also be reduced
 * to a single element, thus memory consumption should not be a problem in most cases.
 *
 * This scheduler is ideal in situations where most transitions happen at exactly
 * the same time, as we can then profit from the internal structure and simply
 * return the mapped elements. It results in sufficient efficiency in most other
 * cases, mainly due to the code base being a lot smaller then the Activity Heap.
 */
class SchedulerNA: public SchedulerHS
{
public:
	SchedulerNA();
	SchedulerNA(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels);
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	virtual ~SchedulerNA();
};

} /* namespace ns_DEVS */
#endif /* SCHEDULERNA_H_ */
