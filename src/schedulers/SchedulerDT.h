/*
 * SchedulerDT.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULERDT_H_
#define SCHEDULERDT_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"
#include <vector>

namespace cdevs {

/**
 * .. warning:: Do **not** use this scheduler!
 *
 * This scheduler will only work if all models are scheduled at exactly the same
 * time, or are not scheduled at all (scheduling at infinity is allowed though).
 */
class SchedulerDT: public Scheduler
{
public:
	SchedulerDT();
	SchedulerDT(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels);
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	virtual ~SchedulerDT();
};

} /* namespace ns_DEVS */
#endif /* SCHEDULERDT_H_ */
