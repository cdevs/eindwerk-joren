/*
 * SchedulerAH.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULERAH_H_
#define SCHEDULERAH_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"

namespace cdevs {

/**
 * The Activity Heap is based on a heap, though allows for reschedules.
 *
 * To allow reschedules to happen, a model is accompagnied by a flag to
 * indicate whether or not it is still valid.
 * As soon as a model is rescheduled, the flag of the previously scheduled
 * time is set and another entry is added. This causes the heap to become *dirty*,
 * requiring a check for the flag as soon as the first element is requested.
 * Due to the possibility for a dirty heap, the heap will be cleaned up as
 * soon as the number of invalid elements becomes too high.
 * This cleanup method has O(n) complexity and is therefore only
 * ran when the heap becomes way too dirty.
 * Another problem is that it might consume more memory than other schedulers,
 * due to invalid elements being kept in memory.
 * However, the actual model and states are not duplicated as they are references.
 * The additional memory requirement should not be a problem in most situations.
 * The 'activity' part from the name stems from the fact that only models where
 * the *time_next* attribute is smaller than infinity will be scheduled.
 * Since these elements are not added to the heap, they aren't taken into account
 * in the complexity. This allows for severe optimisations in situations where
 * a lot of models can be scheduled for infinity.
 *
 * Of all provided schedulers, this one is the most mature due to it being the
 * oldest and also the default scheduler. It is also applicable in every situation
 * and it offers sufficient performance in most cases.
 *
 * This scheduler is ideal in situations where (nearly) no reschedules happen
 * and where most models transition at a different time.
 *
 * It results in slow behaviour in situations requiring lots of rescheduling,
 * and thus lots of dirty elements.
 *
 * This method is also applied in the VLE simulator and is the common approach
 * to heap schedulers that require invalidation. It varies from the scheduler in
 * ADEVS due to the heap from the heapq library being used, which doesn't offer
 * functions to restructure the heap.
 * Reimplementing these methods in pure Python would be unnecessarily slow.
 */
class SchedulerAH: public Scheduler
{
public:
	SchedulerAH();
	SchedulerAH(std::list<std::shared_ptr<AtomicDevs> > &models, float epsilon, unsigned int totalModels);
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::list<std::shared_ptr<AtomicDevs> > &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	virtual ~SchedulerAH();
};

} /* namespace ns_DEVS */
#endif /* SCHEDULERAH_H_ */
