/*
 * SchedulerNA.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerNA.h"

namespace cdevs {

SchedulerNA::SchedulerNA(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels)
{
}

void SchedulerNA::Schedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerNA::Unschedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerNA::MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set)
{
}

DevsTime SchedulerNA::ReadFirst()
{
}

SchedulerNA::SchedulerNA() {
}

std::list<std::shared_ptr<AtomicDevs> > SchedulerNA::GetImminent(DevsTime time)
{
}

SchedulerNA::~SchedulerNA() {
}

} /* namespace ns_DEVS */
