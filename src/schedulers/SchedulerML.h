/*
 * SchedulerML.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 *      Author: Nathan
 */

#ifndef SCHEDULERML_H_
#define SCHEDULERML_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"
#include <list>
#include "../utility.h"
#include <cfloat>
#include <cmath>

namespace cdevs {

/**
 * The Minimal List scheduler is the simplest scheduler available, though it
 * has extremely bad performance in most cases.
 *
 * It simply keeps a list of all models. As soon as a reschedule happens,
 * the list is checked for the minimal value, which is stored. When the imminent
 * models are requested, the lowest value that was found is used to immediatelly
 * return [], or it iterates the complete list in search of models that qualify.
 */
class SchedulerML: public Scheduler
{
public:
	//SchedulerML();
	SchedulerML(std::list<std::shared_ptr<AtomicDevs> > models, float epsilon, unsigned int totalModels);
	virtual ~SchedulerML();
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::list<std::shared_ptr<AtomicDevs> > reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	double getEpsilon() const;
	void setEpsilon(double epsilon);
	int getTotalmodels() const;
	void setTotalmodels(int totalmodels);

private:
	std::list<std::shared_ptr<AtomicDevs> > models_;
	DevsTime minval_;
	double epsilon_;
	int totalmodels_;

};

} /* namespace ns_DEVS */
#endif /* SCHEDULERML_H_ */
