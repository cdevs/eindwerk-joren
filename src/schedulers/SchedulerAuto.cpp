/*
 * SchedulerAuto.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerAuto.h"

namespace cdevs {

SchedulerAuto::SchedulerAuto(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels)
{
}

void SchedulerAuto::Schedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerAuto::Unschedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerAuto::MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set)
{
}

DevsTime SchedulerAuto::ReadFirst()
{
}

SchedulerAuto::SchedulerAuto() {
}

std::list<std::shared_ptr<AtomicDevs> > SchedulerAuto::GetImminent(DevsTime time)
{
}

SchedulerAuto::~SchedulerAuto() {
}

} /* namespace ns_DEVS */
