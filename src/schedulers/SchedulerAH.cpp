/*
 * SchedulerAH.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerAH.h"

namespace cdevs {

SchedulerAH::SchedulerAH(std::list<std::shared_ptr<AtomicDevs> > &models, float epsilon, unsigned int totalModels)
{
}

void SchedulerAH::Schedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerAH::Unschedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerAH::MassReschedule(std::list<std::shared_ptr<AtomicDevs> > &reschedule_set)
{
}

DevsTime SchedulerAH::ReadFirst()
{
}

SchedulerAH::SchedulerAH() {
}

std::list<std::shared_ptr<AtomicDevs> > SchedulerAH::GetImminent(DevsTime time)
{
}

SchedulerAH::~SchedulerAH() {
}

} /* namespace ns_DEVS */
