/*
 * SchedulerDT.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerDT.h"

namespace cdevs {

SchedulerDT::SchedulerDT(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels)
{
}

void SchedulerDT::Schedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerDT::Unschedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerDT::MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set)
{
}

DevsTime SchedulerDT::ReadFirst()
{
}

SchedulerDT::SchedulerDT() {
}

std::list<std::shared_ptr<AtomicDevs> > SchedulerDT::GetImminent(DevsTime time)
{
}

SchedulerDT::~SchedulerDT() {
}

} /* namespace ns_DEVS */
