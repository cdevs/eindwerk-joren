/*
 * SchedulerHS.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerHS.h"

namespace cdevs {

SchedulerHS::SchedulerHS(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels)
{
}

void SchedulerHS::Schedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerHS::Unschedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerHS::MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set)
{
}

DevsTime SchedulerHS::ReadFirst()
{
}

SchedulerHS::SchedulerHS() {
}

std::list<std::shared_ptr<AtomicDevs> > SchedulerHS::GetImminent(DevsTime time)
{
}

SchedulerHS::~SchedulerHS() {
}

} /* namespace ns_DEVS */
