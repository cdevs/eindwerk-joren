/*
 * SchedulerAuto.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULERAUTO_H_
#define SCHEDULERAUTO_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"
#include <vector>

namespace cdevs {

/**
 * Automaticly polymorphic scheduler. It will automatically adapt to your
 * scheduling requests, though at a slight overhead due to the indirection and
 * statistics gathering. If you know what is your optimal scheduler, please
 * choose this one. If the access pattern varies throughout the simulation,
 * this scheduler is perfect for you. It will choose between the HeapSet and
 * Minimal List scheduler.
 *
 * .. warning:: Barely tested, certainly not with distribution and relocation!!!
 * **Use with caution!!!***
 */
class SchedulerAuto: public Scheduler
{
public:
	SchedulerAuto();
	SchedulerAuto(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels);
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	virtual ~SchedulerAuto();
};

} /* namespace ns_DEVS */
#endif /* SCHEDULERAUTO_H_ */
