/*
 * SchedulerSL.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerSL.h"

namespace cdevs {

SchedulerSL::SchedulerSL(std::vector<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels)
{
}

void SchedulerSL::Schedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerSL::Unschedule(std::shared_ptr<AtomicDevs> model)
{
}

void SchedulerSL::MassReschedule(std::vector<std::shared_ptr<AtomicDevs>> &reschedule_set)
{
}

DevsTime SchedulerSL::ReadFirst()
{
}

SchedulerSL::SchedulerSL() {
}

std::list<std::shared_ptr<AtomicDevs> > SchedulerSL::GetImminent(DevsTime time)
{
}

SchedulerSL::~SchedulerSL() {
}

} /* namespace ns_DEVS */
