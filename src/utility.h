/*
 * utility.h
 *
 *  Created on: Mar 12, 2015
 *      Author: uauser
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#include <string>
#include <functional>
#include "DevsTime.h"

namespace cdevs {

typedef std::function<void()> Action;

class Edge{
	Edge();
	~Edge();
};


class StateBase
{
public:
	DevsTime time_last_;
	DevsTime time_next_;
	DevsTime elapsed_;

	/*bool friend operator< (const State& lhs, const State& rhs)
	{
	     return (lhs.value < rhs.value );
	}*/

	virtual ~StateBase();

	virtual StateBase const& clone() const = 0;

	virtual std::string string() const  = 0;
	virtual std::string toXML() const  = 0;
};

/**
 * \brief State uses the 'Curiously Recurring Template Pattern' to implement a
 * polymorphic copy method for StateBase.
 */
template <class ValueType = std::string, class Derived = StateBase>
class State : public StateBase
{
public:
	State(ValueType value) : value_(value) {};

	/**
	 * \brief Return a reference to a copy of this object.
	 *
	 * @return A reference to a new copy of this object.
	 */
	State<ValueType, Derived> const& clone() const {
		return *(new Derived(static_cast<Derived const&>(*this)));
	}

	virtual inline void setValue(ValueType value) {value_ = value; };
	virtual inline ValueType getValue() const { return value_; };
	virtual std::string string() const { return value_; };
	virtual std::string toXML() const { return ""; };
private:
	ValueType value_;
};

struct Event
{
	unsigned event_size_;
};

inline StateBase::~StateBase() {
}

} /* Dit is het einde van de namespace.*/

#endif /* UTILITY_H_ */
