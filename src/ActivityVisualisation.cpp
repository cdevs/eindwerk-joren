/*
 * ActivityVisualisation.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "ActivityVisualisation.h"

namespace cdevs {

void VisualizeLocations(const BaseSimulator &kernel)
{
}

void VisualizeActivity(const Simulator &sim)
{
}

void VisualizeMatrix(const std::list<std::list<void> > &matrix, std::string formatstring, std::string filename)
{
}

void VisualizeMatrix(const std::list<std::list<void> > &matrix, std::string formatstring, std::istream &file)
{
}

} /* namespace ns_DEVS */
