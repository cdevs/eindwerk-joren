/*
 * AtomicDevs.h
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#ifndef ATOMICDEVS_H_
#define ATOMICDEVS_H_

#include "BaseDEVS.h"
#include <memory>
#include <stack>

namespace cdevs {

class AtomicDevs: public BaseDevs
{
	friend class Solver;
public:
	AtomicDevs(std::string name);
	virtual ~AtomicDevs();

	virtual StateBase const& getState() const = 0;
	// template<class T, class S>
	virtual StateBase const& getState(DevsTime request_time, bool first_call = true) const { return getState(); };

	// template<class T, class S>
	virtual void setState(StateBase const& state) = 0;

	void FetchActivity(double time); //Should also pass reference to a container holding all activities.
	void SetGvt(double new_gvt, bool last_state_only); // should also pass a reference to a container holding all activities.
	void Revert(double time, bool memo);
	virtual StateBase const& ExtTransition(std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs) = 0;
	virtual StateBase const& IntTransition() = 0;
	virtual StateBase const& ConfTransition(std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs) = 0;
	virtual std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > OutputFunction();
	virtual double TimeAdvance();

	double PreActivityCalculation();
	double PostActivityCalculation(double prevalue);
	void FlattenConnections();
	void UnflattenConnections();
	void SetLocation(int location, bool force);
	//TODO: add `select_hierarchy` parameter
	int Finalize(std::string name, int model_counter,
			std::map<int, std::shared_ptr<BaseDevs>>& model_ids, std::list<std::shared_ptr<BaseDevs>> select_hierarchy);
	int GetModelLoad(std::map<int, double> loads);

	bool Revert(DevsTime time, bool memo);

	void AddDelayedTransition(DevsTime time);
	bool PerformDelayedTransition(DevsTime until_time);
private:
	bool relocatable_;
	DevsTime last_read_time_;
	std::list<std::shared_ptr<BaseDevs>> select_hierarchy_;
	std::stack<DevsTime> delayed_transitions_;
};

template<class Derived, class StateType = StateBase>
class Atomic : public DevsCRTP<AtomicDevs, Derived>
{
public:
	using DevsCRTP<AtomicDevs, Derived>::DevsCRTP;

	virtual ~Atomic() {};

	/**
	 * \brief Gets the state of Atomic
	 *
	 * @return State of Atomic
	 */
	// template <class T, class S>
	// State<T, S> const& AtomicDevs::getState() const
	StateType const& getState() const
	{
		return *state_;
	}

	/**
	 * \brief Sets the state of Atomic
	 *
	 * TODO: We should think this downcast through. We could hide the constructor of
	 * StateBase or try taking State as argument to this method. We could also simply
	 * throw an error if state doesn't inherit State.
	 *
	 * @return State of Atomic
	 */
	void setState(StateBase const& state)
	{
		state_.reset(static_cast<const StateType *>(&state.clone()));
	}

	virtual StateType const& ExtTransition(std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs)
	{
		return *state_;
	}

	virtual StateType const& IntTransition()
	{
		return *state_;
	}

	virtual StateType const& ConfTransition(std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs)
	{
		return *state_;
	}
protected:
	std::unique_ptr<const StateType> state_;
};

} /* namespace ns_DEVS */

#endif /* ATOMICDEVS_H_ */
