#include "BaseSimulator.h"
#include "VCDVariable.h"
#include "Graph.h"
#include "allocators/Allocator.h"
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>

namespace cdevs {
/**
 *  The controller class, which is a special kind of normal simulation kernel. This should always run on the node labeled 0.
 *  It contains some functions that are only required to be ran on a single node, such as GVT initiation
 */
class Controller: public BaseSimulator
{
public:
	//Controller();
	Controller(std::shared_ptr<RootDevs> model, unsigned n_kernels = 1);
	virtual ~Controller();

	BaseSimulator * getKernel(unsigned index);

	void BroadcastModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models, SchedulerType scheduler_type,
	        bool is_flattened);

	void SimulateAll();

	void setGlobals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
			StateSaver * statesaver, std::vector<std::shared_ptr<Tracer> > tracers);

	//GVT
	void GvtDone();
	void StartGvtThread(double gvt_interval);
	void ThreadGvt(int frequency);

	std::vector<VcdVariable> GetVcdVariables();

	//allocator
	Graph GetEventGraph();
	std::map<int, int> GetInitialAllocations();
	void RunAllocator();
	void SetAllocator(Allocator * initial_allocator);

	//simulation
	void SetTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition);
	void SetTerminationTime(DevsTime at_time);

	void RegisterTracer(std::shared_ptr<Tracer> tracer, bool recover);

	void RunTrace(Tracer * tracer, std::shared_ptr<AtomicDevs> model, std::string text);
private:
	std::vector<BaseSimulator *> kernels_;

	bool should_wait_for_gvt_;
	bool should_run_gvt_;

	std::thread gvt_thread_;
	std::unique_lock<std::mutex> gvt_lock_;
	std::condition_variable gvt_condition_;

	Allocator * initial_allocator_;

	Graph event_graph_;
	std::map<int, int> allocations_;

	bool has_graph_;
	bool has_allocations_;
};
}
