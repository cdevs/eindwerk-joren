#include <algorithm>
#include "Solver.h"
#include "Controller.h"
#include <cassert>
#include <limits>

namespace cdevs
{

std::mutex Solver::temp_mutex;

/**
 * Constructor
 */
Solver::Solver(Controller * controller, unsigned id) : controller_(controller), id_(id) {
	has_initial_allocator_ = false;
	is_simulation_irreversible_ = true;
	msg_copy_ = 0;
	do_some_tracing_ = false;
	model_ = 0;
	temporarily_irreversible_ = false;
	use_classic_devs_ = false;
	block_outgoing_ = DevsTime(-1, -1);
	checkpoint_restored_ = false;
	tmin_ = std::numeric_limits<double>::infinity();
	message_received_count_ = 0;
	message_sent_count_ = 0;
	color_ = kWhite1;

	//vchange_mutex_ = std::array<std::mutex, 4>(4, *(new std::mutex()));
	vchange_lock_ = {std::unique_lock<std::mutex>(vchange_mutex_[0]),
		std::unique_lock<std::mutex>(vchange_mutex_[1]),
		std::unique_lock<std::mutex>(vchange_mutex_[2]),
		std::unique_lock<std::mutex>(vchange_mutex_[3])};

	//vchange_cv_ = std::vector<std::condition_variable>(4, std::condition_variable());
}

/**
 * Destructor
 */
Solver::~Solver() {
	for (Message * msg : input_message_queue_)
		delete msg;

	input_message_queue_.clear();
}

/**
 * \brief Wrapper for the AtomicDEVS output function, which will save event counts
 *
 * @param aDEVS the AtomicDEVS model that generates the output
 * @param time the time at which the output must be generated
 * @return The generated output
 */
std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > Solver::AtomicOutputGenerationEventTracing(
		std::shared_ptr<AtomicDevs> adevs, DevsTime time) {
	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > retval =
			AtomicOutputGeneration(adevs, time);
	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > newmap;

	for (auto iterator = retval.begin(); iterator != retval.end();
			iterator++) {
		auto p = iterator->first.lock();
		auto pm = iterator->second;
		p->setMsgCount(p->getMsgCount() + pm.size());
		newmap.insert(std::pair<std::shared_ptr<Port>, std::list<std::string> >(p,pm));
	}
	return newmap;

}

/**
 * AtomicDEVS function to generate output, invokes the outputFnc function of the model.
 *
 * @param aDEVS: the AtomicDEVS model that generates the output
 * @param time: the time at which the output must be generated
 * @return The generated output
 */
std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > Solver::AtomicOutputGeneration(
		std::shared_ptr<AtomicDevs> adevs, DevsTime time) {
	adevs->my_output_ = adevs->OutputFunction();

	transitioning_[adevs->GetModelID()] |= 1;
	//# Being here means that this model created output, so it triggered its internal transition
	//# save this knowledge in the basesimulator for usage in the actual transition step

	return adevs->my_output_;
}

void Solver::MassAtomicTransitions(std::map<int, int> trans,
		DevsTime clock_now) {
	double t = clock_now.getX();
	double age = clock_now.getY();

	for (auto adevs_pair : trans) {
		int ttype = adevs_pair.second;

		std::shared_ptr<AtomicDevs>  adevs = FindDevsModelById(adevs_pair.first);

		if(adevs == 0)
		{
			throw RuntimeDevsException("[Solver::MassAtomicTransitions] Got a null atomic devs for id " + adevs_pair.first);
		}

		// Make a copy of the message before it is passed to the user
		if (msg_copy_ != 2) {
			// Prevent a pass statement
			if (msg_copy_ == 1) {
				for (auto& port : adevs->my_input_) {
					/*TODO:
					 aDEVS.my_input = {key:
					 [i.copy() for i in aDEVS.my_input[key]]
					 for key in aDEVS.my_input}
					 */
				}

			} else if (msg_copy_ == 0) {
				/*TODO:
				 aDEVS.my_input = {key:
				 pickle.loads(pickle.dumps(aDEVS.my_input[key],
				 pickle.HIGHEST_PROTOCOL))
				 for key in aDEVS.my_input}
				 */
			}
		}
		if (ttype == 1) {
			// Internal only
			adevs->setState(adevs->IntTransition());
		} else if (ttype == 2) {
			// External only
			adevs->setTimeElapsed(t - adevs->getTimeLast().getX());
			StateBase const& new_state = adevs->ExtTransition(adevs->my_input_);
			adevs->setState(new_state);
		} else if (ttype == 3) {
			// Confluent
			adevs->setTimeElapsed(0.0);
			StateBase const& new_state = adevs->ConfTransition(adevs->my_input_);
			adevs->setState(new_state);

		} else {
			//raise DEVSException("Problem in transitioning dictionary: unknown element %s" % ttype)
		}

		double ta = adevs->TimeAdvance();
		adevs->time_last_ = clock_now;

		if (ta < 0) {
			throw RuntimeDevsException(
				"Negative time advance in atomic model '" + adevs->GetModelFullName() + "' with value "
					+ std::to_string(ta) + " at time " + std::to_string(t));
		}

		// Update the time, this is just done in the timeNext, as this will propagate to the basesimulator
		DevsTime time_next = DevsTime(t + ta, !ta ? 1 : age + 1);
		adevs->setTimeNext(time_next);



		// this is different than pypdevs (again), just check the tracing here - Ian
		if(do_some_tracing_)
		{
			//tracers_.Trace(clock_now, "");
			switch(ttype)
			{
				case 1:
				{
					tracers_.TracesInternal(adevs);
					break;
				}
				case 2:
				{
					tracers_.TracesExternal(adevs);
					break;
				}
				case 3:
				{
					tracers_.TracesConfluent(adevs);
					break;
				}
				default:
				{
					throw RuntimeDevsException("[Solver::MassAtomicTransitions] Got unidentified transition");
				}
			}
		}

		adevs->my_input_.clear(); // TODO: This causes lots of problems with parallel... - Ian
	}
}

void Solver::AtomicInitialize(std::shared_ptr<AtomicDevs> adevs,
		DevsTime at_time) {
	DevsTime t;
	t.setX(at_time.getX() - adevs->getTimeElapsed());
	t.setY(1);

	double ta = adevs->TimeAdvance();

	if (ta < 0)
	{
		throw RuntimeDevsException("Negative time advance in atomic model '" + adevs->GetModelFullName() + "' with value "
			+ std::to_string(ta) + " at initialisation");
	}

	t.setX(t.getX() + ta);

	adevs->setTimeNext(t);

	/*
	 // Save the state
	 if (! irreversible_){
	 aDEVS.oldstate_s.append(self.state_saver(aDEVS.time_last,
	 aDEVS.time_next,
	 aDEVS.state,
	 0.0,
	 {},
	 0.0))
	 */


	// All tracing features
	tracers_.TracesInitialize(adevs, at_time);
}

/**
 * CoupledDEVS function to generate the output, calls the atomicDEVS models where necessary. Output is routed too.
 *
 * @param time The time at which output should be generated
 * @return The models that should be rescheduled
 *
 */
std::list<std::shared_ptr<AtomicDevs> > Solver::CoupledOutputGenerationClassic(DevsTime at_time) {
	std::list<std::shared_ptr<AtomicDevs> > immenent = model_->GetScheduler()->GetImminent(at_time);

	if (immenent.empty()) {
		// no immenent models, return everything in the transitioning right now
		std::list<std::shared_ptr<AtomicDevs> > transitioning_list_;

		// push every model to the transitioning list
		for(auto id_transitioning_pair : transitioning_)
		{
			transitioning_list_.push_back(FindDevsModelById(id_transitioning_pair.first));
		}

		// TODO is it ok if all of these are atomic? - Ian
		return transitioning_list_;
	}

	std::list<std::shared_ptr<AtomicDevs> > reschedule = immenent;
	for (auto& model : immenent) {
		DevsTime current_time_next = model->getTimeNext();
		DevsTime time_next = DevsTime(current_time_next.getX(),
				current_time_next.getY() + 1);

		model->setTimeNext(time_next);
	}
	std::shared_ptr<AtomicDevs>  child;
	// return value are the models to reschedule
	// self.transitioning are the models that must transition
	if (immenent.size() > 1) {
		immenent.sort(wayToSort);

		std::list<std::shared_ptr<AtomicDevs> > pending = immenent;

		int level = 1;

		while (pending.size() > 1) {
			// take the model each time,
			// as we need to make sure that the select
			// hierarchy is valid everywhere
			std::shared_ptr<BaseDevs> model = pending.front();
			pending.pop_front();
			//TODO select hierarchy?

			level += 1;
		}
		child = pending.front();
	} else
		child = immenent.front();


	// recorrect the timeNext of the model that will transition
	DevsTime current_time_next = child->getTimeNext();
	DevsTime time_next = DevsTime(current_time_next.getX(), current_time_next.getY() - 1);

	child->setTimeNext(time_next);
	child->my_output_ = ClassicDEVSWrapper(child).OutputFunc();

	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > outbag = child->my_output_;

	transitioning_[child->GetModelID()] = 1;

	for (auto outport_output_pair : outbag) {
		std::shared_ptr<Port> outport = outport_output_pair.first.lock();
		std::list<std::string> payload = outport_output_pair.second;

		for (auto inport_z_pair : outport->routing_outline_) {
			std::shared_ptr<Port> inport = inport_z_pair.first.lock();
			std::function<Event(Event)> z_function = inport_z_pair.second;

			if (z_function) {
				// TODO payload = [z(pickle.loads(pickle.dumps(m))) for m in payload] - Ian
			}

			std::shared_ptr<AtomicDevs>  host_devs = std::dynamic_pointer_cast<AtomicDevs>(inport->GetHostDEVS().lock());

			if(host_devs == 0)
			{
				std::cout << "Got 0 host DEVS for " << inport->name_ << std::endl;
				continue;
			}

			host_devs->my_input_[inport].clear();

			for (std::string payload_string : payload) {
				host_devs->my_input_[inport].push_back(payload_string);
			}

			transitioning_[host_devs->GetModelID()] = 2; //TODO: host_devs ?
			reschedule.push_back(host_devs);
		}
	}
	// self.transitioning = {ClassicDEVSWrapper(m): self.transitioning[m]
        // for m in self.transitioning}
	// TODO: see ClassicDEVSWrapper::outputFunc
	return reschedule;

}

//TODO: Better name
bool wayToSort(std::shared_ptr<BaseDevs> one, std::shared_ptr<BaseDevs> two) {
	return one->getTimeNext() < two->getTimeNext();
}

/**
 * CoupledDEVS function to generate the output, calls the atomicDEVS models where necessary. Output is routed too.
 *
 * @param Time the time at which output should be generated
 * @return The models that should be rescheduled
 */
std::list<std::shared_ptr<AtomicDevs> > Solver::CoupledOutputGeneration(
		DevsTime at_time) {
	std::map<std::shared_ptr<AtomicDevs> , std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > > remotes;
	for (auto& devs : model_->GetScheduler()->GetImminent(at_time)) {
		//std::map<Port, std::list<std::string> >
		auto outbag = AtomicOutputGeneration(devs, at_time);

		for (auto port_output_pair : outbag) {
			std::shared_ptr<Port> outport = port_output_pair.first.lock();
			auto payload = port_output_pair.second; // std::list<std::string>

			for (auto port_z_pair : outport->routing_outline_) {
				std::shared_ptr<Port> inport = port_z_pair.first.lock();
				std::function<Event(Event)> z_function = port_z_pair.second;

				std::shared_ptr<AtomicDevs>  host_devs = std::dynamic_pointer_cast<AtomicDevs>(inport->GetHostDEVS().lock());

				// if the function is assigned
				if (z_function) {
					//payload = [z(pickle.loads(pickle.dumps(m)))
					//for m in payload]
				}

				if(host_devs->GetLocation() == id_) {
					host_devs->my_input_[inport].insert(
							host_devs->my_input_[inport].end(), payload.begin(),
							payload.end());
					transitioning_[host_devs->GetModelID()] |= 2;
				}
				else {
					remotes[host_devs][inport] = payload;
				}
			}
		}
	}
	for(auto adevs_content_pair : remotes)
	{
		std::shared_ptr<AtomicDevs>  adevs = adevs_content_pair.first;
		Send(adevs, at_time, adevs_content_pair.second);
	}
	std::list<std::shared_ptr<AtomicDevs> > reschedule;
	for(auto id_transition_pair : transitioning_)
	{
		std::shared_ptr<AtomicDevs>  model = FindDevsModelById(id_transition_pair.first);
		if(model != 0)
			reschedule.push_back(model);
	}
	return reschedule;
}

/**
 * \brief CoupledDEVS function to initialise the model, calls all its _local_ children too.
 */
void Solver::CoupledInitialize() {
	DevsTime time_next(std::numeric_limits<double>::infinity(), 1);

	// different than pypdevs again... - Ian
	//tracers_.Trace(DevsTime(0.0, 0), "");

	//for each local model (pypdevs) = for each model (CDevs)
	for (auto& devs : model_->GetComponentSet()) {
		// initialize atom devs
		AtomicInitialize(devs, DevsTime(0.0, 0));

		// get minimum to time next, which we will set our model to
		if (devs->getTimeNext() < time_next) {
			time_next = devs->getTimeNext();
		}
	}

	// TODO NOTE do not immediately assign to the timeNext,
	// as this is used in the GVT algorithm to see whether a node has finished
	// I added this as to do since we do immediately set timeNext, however Yentl
	// makes a reference first? - Ian
	model_->setTimeNext(time_next);
	model_->SetScheduler(model_->getSchedulerType());
}

/**
 * Perform Dynamic Structure detection of the model
 *
 * @param transitioning Iteratable to be checked for a dynamic structure transiton
 */
void Solver::PerformDsDevs(std::vector<std::shared_ptr<AtomicDevs> > transitioning) {
	// TODO I don't think this is necessary, correct me if I'm wrong :3 - Ian
}

/**
 * Registers a tracer
 *
 * @param tracer The tracer to register
 * @param recover Are we recovering simulation?
 */
void Solver::RegisterTracer(std::shared_ptr<Tracer> tracer, bool recover)
{
	do_some_tracing_ = true;
	tracers_.RegisterTracer(tracer, recover);
}

/**
 * Finds the local model corresponding to the given id
 *
 * @return The id of the local model
 */
std::shared_ptr<AtomicDevs>  Solver::FindDevsModelById(int id) {
	for (auto& atomic_devs : models_) {
		if (atomic_devs->GetModelID() == id)
			return atomic_devs;
	}

	return 0;
}

/**
 * \brief Sends message with content to the appropriate kernels
 *
 * @param adevs The model for the output
 * @param time The time related to the content
 * @param content The content, a map with Port as key and a string list as values
 */
void Solver::Send(std::shared_ptr<AtomicDevs> adevs, DevsTime time, std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > content)
{
	if(block_outgoing_ == time and !checkpoint_restored_ )
		return;

	int destination = adevs->GetLocation();

	Message * msg = new Message(time, adevs->GetModelID(), content);

	NotifySend(destination, time, color_);

	output_message_queue_.push_back(msg);

	controller_->getKernel(destination)->Receive(msg);
}

/**
 * Sets the use of Classic DEVS instead of Parallel DEVS.
 *
 * @param classicDEVS: whether or not to use Classic DEVS
 */
void Solver::SetClassicDevs(bool classic_devs)
{
	use_classic_devs_ = classic_devs;
}

/**
 * \brief Notify the simulation kernel of the sending of a message. Needed for GVT calculation.
 *
 * @param destination: the name of the simulation kernel that will receive the sent message
 * @param timestamp: simulation time at which the message is sent
 * @param color: color of the message being sent (for Mattern's algorithm)
 */
void Solver::NotifySend(int destination, DevsTime timestamp, Color color)
{
	 message_sent_count_++;
	 if(colorVectors_[color].count(destination) == 1)
	 {
		 colorVectors_[color][destination] += 1;
	 }
	 else
		 colorVectors_[color][destination] = 1;

	 if(color == kRed1 || color == kRed2)
	 {
		 if(timestamp.getX() < tmin_)
			 tmin_ = timestamp.getX();
	 }
}

/**
 * Notify the simulation kernel of the receiving of a message. Needed for GVT calculation.
 *
 * @param color: the color of the received message (for Mattern's algorithm)
 */
void Solver::NotifyReceive(Color color)
{
	 message_received_count_++;
	 if(colorVectors_[color].count(id_) == 1)
	 {
		 colorVectors_[color][id_] -= 1;
	 }
	 else
		 colorVectors_[color][id_] = 1;

	 // notify all waiting threads
	 vchange_cv_[color].notify_all();
}

unsigned Solver::getId()
{
	return id_;
}

}
