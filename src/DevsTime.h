#ifndef SRC_DEVSTIME_H_
#define SRC_DEVSTIME_H_

#include <string>

namespace cdevs {
	/**
	 * \brief Our notation of time
	 *
	 * The time object consist of two fields:
	 * 	x - the time
	 * 	y - age (= number of loops in DEVS simulation)
	 */
	class DevsTime {
	public:
		DevsTime();
		DevsTime(double x, double y);

		double x_;
		int y_;

		double getX() const;
		void setX(double x);
		int getY() const;
		void setY(int y);

		bool operator>(const DevsTime& g) const;
		bool operator>=(const DevsTime& g) const;
		bool operator<(const DevsTime& g) const;
		bool operator<=(const DevsTime& g) const;
		void operator=(const DevsTime& d);
		bool operator==(const DevsTime& g) const;

		std::string string() const;
	};
}
#endif /* SRC_DEVSTIME_H_ */
