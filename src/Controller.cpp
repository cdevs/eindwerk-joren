#include "Controller.h"

namespace cdevs
{
/**
 * Constructor
 *
 * @param id: id of the controller
 * @param model: model to host at the kernel
 * @param server: the server to make requests on
 */
Controller::Controller(std::shared_ptr<RootDevs> model, unsigned n_kernels)
	: BaseSimulator(0, model, this)
{
	initial_allocator_ = 0;

	should_wait_for_gvt_ = false;
	should_run_gvt_ = false;

	has_allocations_ = false;
	has_graph_ = false;

	kernels_.push_back(this);
	for (unsigned i = 1; i < n_kernels; i++)
		kernels_.push_back(new BaseSimulator(i, model, this));
}

Controller::~Controller()
{
	for (unsigned i = 1; i < kernels_.size(); i++)
		delete kernels_.at(i);
}

BaseSimulator * Controller::getKernel(unsigned index)
{
	return kernels_.at(index);
}

void Controller::BroadcastModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models, SchedulerType scheduler_type,
        bool is_flattened)
{
	for (BaseSimulator * kernel : kernels_)
		kernel->SendModel(model, models, scheduler_type, is_flattened);
}

/**
 * \brief Start the simulation on all kernels concurrently.
 */
void Controller::SimulateAll()
{
	std::list<std::thread> threads;
	for (BaseSimulator * kernel : kernels_) {
		threads.push_back(std::thread(&BaseSimulator::Simulate, kernel));
	}

	for (auto& thread : threads)
		thread.join();
}

/**
 * Configure all 'global' variables for all kernel.
 *
 * @param loglevel Level of logging library.
 * @param checkpoint_frequency Frequency at which checkpoints should be made.
 * @param checkpoint_name Name of the checkpoint to save.
 * @param statesaver The instance of StateSaver to use.
 */
void Controller::setGlobals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
        StateSaver * statesaver, std::vector<std::shared_ptr<Tracer> > tracers)
{
	for (BaseSimulator * kernel : kernels_)
		kernel->setGlobals(loglevel, checkpoint_frequency, checkpoint_name, statesaver, kernels_.size(), tracers);
}

/**
 * Notify this simulation kernel that the GVT calculation is finished
 */
void Controller::GvtDone()
{
	// TODO: make this a lock
	should_wait_for_gvt_ = true;
}

/**
 * Start the GVT thread
 *
 * @param gvt_interval the interval between two successive GVT runs
 */
void Controller::StartGvtThread(double gvt_interval)
{
	should_run_gvt_ = true;
	gvt_thread_ = std::thread(&Controller::ThreadGvt, this, gvt_interval);
	gvt_thread_.detach();
}

/**
 * Run the GVT algorithm, this method should be called in its own thread,
 * because it will block
 *
 * @param frequency: the time to sleep between two GVT calculations in ms
 */
void Controller::ThreadGvt(int frequency)
{
	// pause for a brief moment before looping
	std::this_thread::sleep_for(std::chrono::milliseconds(frequency));

	while (should_run_gvt_) {
		// setup initial GVT control message
		GvtControlMessage message; // TODO determine field values
		message.x = std::numeric_limits<double>::infinity();
		message.y = message.x;
		message.z = accumulator_;
		message.q = {};

		// execute GVT calculations, starting from initial GVT control message
		ReceiveControl(message, true);

		// wait for lock
		gvt_condition_.wait(gvt_lock_);

		// Limit the GVT algorithm, otherwise this will flood the ring
		std::this_thread::sleep_for(std::chrono::milliseconds(frequency));
	}
}

/**
 * Generate a list of all variables that exist in the current scope
 *
 * TODO: Unused.
 *
 * @return All VCD variables in the current scope
 */
std::vector<VcdVariable> Controller::GetVcdVariables()
{
	std::vector<VcdVariable> variables;

//	 for each model in the component set, ...
//	 for(std::shared_ptr<AtomicDevs> model : total_model_->GetComponentSet())
//	 {
//		// ..., get each VcdVariable
//		for(VcdVariable variable : model->GetVcdVariables())
//		{
//			variables.push_back(variable);
//		}
//	}

	return total_model_->GetVcdVariables();
}


/**
 * Fetch a graph containing all connections and the number of events between the nodes. This is only useful when an initial allocator is chosen.
 *
 * @return ontaining source and destination, it will return the amount of events passed between them
 */
Graph Controller::GetEventGraph()
{
	RunAllocator();
	return event_graph_;
}

/**
 * Get a list of all initial allocations. Will call the allocator to get the result.
 *
 * @return containing all nodes and the models they host
 */
std::map<int, int> Controller::GetInitialAllocations()
{
	RunAllocator();
	return allocations_;
}

/**
 * Actually extract the graph of exchanged messages and run the allocator with this information.
 * Results are cached.
 *
 * @return tuple -- the event graph and the allocations
 */
void Controller::RunAllocator()
{
	/*
	 if(!has_graph_ && !has_allocations_)
	 {
	 if(!has_initial_allocator_)
	 {
	 has_graph_ = false;
	 has_allocations_ = false;
	 }
	 else
	 {
	 // TODO CONSTRUCT GRAPH???
	 std::map<int, int> allocs = initial_allocator_->Allocate(model_->GetComponentSet(), GetEventGraph(), kernel_count_, GetTotalActivity());
	 allocations_ = allocs;
	 has_initial_allocator_ = false;
	 }
	 }*/
}

/**
 * Sets the use of an initial relocator.
 *
 * @param initial_allocator: whether or not to use an initial allocator
 */
void Controller::SetAllocator(Allocator * initial_allocator)
{
	initial_allocator_ = initial_allocator;
	has_initial_allocator_ = true;
}

/**
 * Sets the termination condition of this simulation kernel.
 * As soon as the condition is valid, it will signal all nodes that they have to stop simulation as soon as they have progressed up to this simulation time.
 * @param termination_condition: a function that accepts two parameters: *time* and *model*. Function returns whether or not to halt simulation
 */
void Controller::SetTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition)
{
	for (BaseSimulator * kernel : kernels_)
		kernel->SetTerminationCondition(termination_condition);
}

/**
 * Sets the time at which simulation should stop, setting this will override
 * the local simulation condition for all kernels.
 *
 * @param time: the time at which the simulation should stop
 */
void Controller::SetTerminationTime(DevsTime at_time)
{
	for (BaseSimulator * kernel : kernels_)
		kernel->SetTerminationTime(at_time);
}

void Controller::RegisterTracer(std::shared_ptr<Tracer> tracer, bool recover)
{
	for (BaseSimulator * kernel : kernels_)
		kernel->RegisterTracer(tracer, recover);
}

void Controller::RunTrace(Tracer* tracer, std::shared_ptr<AtomicDevs> model, std::string text)
{
	//workaround, for some reason test fail when calling delayedaction for 1 kernel
	DelayedAction(tracer, model->getTimeLast(), model, text);
}
}
