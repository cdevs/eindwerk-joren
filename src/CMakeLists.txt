#=====================================================================#
# Sources                                                             #
#=====================================================================#
# SOURCES contains all the source files in the project except for those
# with only google test purposes. Google tests should always be written
# in a separate file and added to the TESTSOURCES set. Separate Test runs
# should not be added in the TESTSOURCES set but be added as executable
# with their dependencies linked.

set(SOURCES 
	#ActivityVisualisation.cpp 
	AtomicDevs.cpp 
	BaseDEVS.cpp 
	BaseSimulator.cpp 
	ClassicDEVSWrapper.cpp 
	Controller.cpp 
	CoupledDEVS.cpp 
	DevsTime.cpp 
	Logger.cpp 
	MessageScheduler.cpp 
	RootDEVS.cpp 
	SimulatorConfiguration.cpp 
	Simulator.cpp 
	Solver.cpp 
	StateSaver.cpp 
	ThreadPool.cpp
	Tracers.cpp 
	Port.cpp  
	allocators/AutoAllocator.cpp 
	allocators/GreedyAllocator.cpp
	exceptions/LogicDevsException.cpp
	exceptions/RuntimeDevsException.cpp
	schedulers/SchedulerAH.cpp 
	schedulers/SchedulerAuto.cpp 
	schedulers/SchedulerDT.cpp 
	schedulers/SchedulerHS.cpp 
	schedulers/SchedulerML.cpp 
	schedulers/SchedulerNA.cpp 
	schedulers/SchedulerSL.cpp     
	
	tracers/Tracer.cpp
	tracers/TracerAction.cpp 
	#tracers/TracerCell.cpp 
	tracers/TracerXML.cpp 
	tracers/TracerVerbose.cpp 
	#tracers/TracerJson.cpp 
	#tracers/TracerVCD.cpp        
	)


set(TESTSOURCES 
	ActivityVisualisation_test.cpp 
	AtomicDevs_test.cpp 
	BaseDEVS_test.cpp 
	BaseSimulator_test.cpp 
	ClassicDEVSWrapper_test.cpp 
	CoupledDEVS_test.cpp 
	RootDEVS_test.cpp 
	SimulatorConfiguration_test.cpp 
	Simulator_test.cpp 
	Solver_test.cpp 
	StateSaver_test.cpp 
	Tracers_test.cpp 
	tracers/TracerVerbose_test.cpp
	tracers/TracerXML_test.cpp
	schedulers/SchedulerML_test.cpp
	utility_test.cpp 
	scenario_test.cpp
	)
	
# Pull in the examples includes
include_directories(${PROJECT_SOURCE_DIR}/examples)

#=====================================================================#
# Subdirectories                                                      #
#=====================================================================#
#add_subdirectory(schedulers)
#add_subdirectory(templates)
#add_subdirectory(Tracers)
#add_subdirectory(allocators)

#=====================================================================#
# Executables                                                         #
#=====================================================================#
# All executables and their linkage should be done here

set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

add_library(cdevs SHARED ${SOURCES})

add_executable (test_all test_all.cpp ${TESTSOURCES} ${EXAMPLESOURCES})
target_link_libraries(test_all cdevs gtest gtest_main pthread dl)
