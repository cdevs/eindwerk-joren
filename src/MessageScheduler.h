#ifndef MESSAGESCHEDULER_H_
#define MESSAGESCHEDULER_H_

#include <vector>
#include <algorithm>
#include <list>
#include <queue>
#include <unordered_set>
#include "exceptions/RuntimeDevsException.h"
#include "Message.h"
#include "utility.h"

namespace cdevs {
class Model;

/**
 * \brief This structs provides a comparator for the priority queue in MessageScheduler.
 *
 * It sorts the Message pointers from high to low timestamp.
 */
struct message_compare
{
  bool operator() (const Message * lhs, const Message * rhs) const
  {
	  return (*lhs > *rhs);
  }
};

/**
 *  An efficient implementation of a message scheduler for the inputQueue,
 *  it supports very fast invalidations (O(1)) and fast retrievals of first
 *  element (O(log(n) in average case)
 */
class MessageScheduler
{
public:
	MessageScheduler();
	virtual ~MessageScheduler();

	void Insert(std::vector<Message *>, std::vector<Model>);
	void Extract(std::vector<int>);

	void Schedule(Message *);
	void MassUnschedule(std::list<Message *> messages);

	Message * ReadFirst();
	void RemoveFirst();

	void PurgeFirst();
	void CleanFirst();

	void Revert(DevsTime);

private:
	std::priority_queue<Message *, std::vector<Message *>, message_compare> message_queue_;
	std::unordered_set<Message *> message_invalids_;
	std::vector<Message *> message_processed_;
};
}

#endif /* MESSAGESCHEDULER_H_ */
