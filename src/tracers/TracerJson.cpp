#include "../tracers/TracerJson.h"

cdevs::TracerJson::TracerJson(int uid, std::string filename)
	: Tracer(uid, filename), first_event_(true)
{
}

cdevs::TracerJson::~TracerJson()
{
}

void cdevs::TracerJson::StartTracer(bool recover)
{
	cdevs::Tracer::StartTracer(recover);
	if (!recover)
		PrintString("{\n   \"trace\":{\n");
}

void cdevs::TracerJson::StopTracer()
{
	PrintString("\n]\n}\n}");
	cdevs::Tracer::StopTracer();
}

void cdevs::TracerJson::TraceInitialize(AtomicDevs& aDEVS, DevsTime t)
{
	std::string text = first_event_ ? "\"event\":[\n{" : ",\n{";
	text += "\n\"model\":\"" + aDEVS.GetModelFullName() + "\",\n";
	text += "\"time\":" + t.string() + ",\n";
	text += "\"kind\":\"EX\",\n";
	text += "\"state\":{" + aDEVS.getState()->toJson() + "}\n}";
	if (first_event_)
		first_event_ = false;
	PrintString(text);
}

void cdevs::TracerJson::TraceInternal(AtomicDevs& aDEVS)
{
	Trace(aDEVS.getTimeLast(), aDEVS.GetModelFullName(), MakeTrace(aDEVS, true));
}

void cdevs::TracerJson::TraceExternal(AtomicDevs& aDEVS)
{
	Trace(aDEVS.getTimeLast(), aDEVS.GetModelFullName(), MakeTrace(aDEVS, false));

}

void cdevs::TracerJson::TraceConfluent(AtomicDevs& aDEVS)
{
	return Trace(aDEVS.getTimeLast(), aDEVS.GetModelFullName(), MakeTrace(aDEVS, false)+MakeTrace(aDEVS, true));
}

void cdevs::TracerJson::Trace(DevsTime time, std::string name, std::string text)
{
	std::string text1 = ",\n";
	text1 += "{\n\"model\":\"" + name + "\",\n";
	text1 += "\"time\":" + time.string() + ",\n";
	text1 += text;
	text1 += "}";
	PrintString(text1);
}

std::string cdevs::TracerJson::PortsToJson(std::string port_kind, std::map<cdevs::Port *, std::list<std::string> >& my_output_)
{
	unsigned nr_ports = my_output_.size();
	unsigned p_count = 0;
	std::string text = "";
	for (auto port_msgs_pair : my_output_) {
		text += "\"" + port_kind + " name\":\"" + port_msgs_pair.first->GetPortName() + "\",\n";
		text += "\"message\":[";
		for (std::list<std::string>::iterator it = port_msgs_pair.second.begin();
		        it != port_msgs_pair.second.end(); ++it) {
			if (it != port_msgs_pair.second.begin())
				text += ",\n";
			else
				text += "\n";
			text += "\"" + *it + "\"\n";
		}
		text += "],\n";
		++p_count;
	}
	return text;
}

std::string cdevs::TracerJson::MakeTrace(AtomicDevs& aDEVS, bool internal){
	std::string trans_kind;
	std::string ports;
	if(internal){
		trans_kind = "IN";
		ports = PortsToJson("outport", aDEVS.my_output_);
	}else{
		trans_kind = "EX";
		ports = PortsToJson("inport", aDEVS.my_input_);
	}
	std::string text = "";
	text += "\"kind\":\""+trans_kind+"\",\n";
	text += ports;
	text += "\"state\":{" + aDEVS.getState()->toJson() + "}\n";
	return text;
}
