#include "../tracers/TracerCell.h"

namespace cdevs
{
TracerCell::~TracerCell() {
}

void TracerCell::StartTracer(bool recover) {
}

void TracerCell::StopTracer() {
}

void TracerCell::TraceInitialize(BaseDevs& aDEVS, time_t t) {
}

void TracerCell::TraceInternal(BaseDevs& aDEVS) {
}

void TracerCell::TraceExternal(BaseDevs& aDEVS) {
}

void TracerCell::TraceConfluent(BaseDevs& aDEVS) {
}

void TracerCell::Trace(time_t time, std::string text) {
}
} /* namespace cdevs */
