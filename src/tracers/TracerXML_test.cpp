#include "../tracers/TracerXML.h"
#include "../AtomicDevs.h"
#include "../../examples/atomicmodel_test/model.h"
#include "../exceptions/LogicDevsException.h"
#include <iostream>
#include "gtest/gtest.h"
#include "../Controller.h"
#include "atomicmodel_test/model.h"

namespace cdevs {

class TracerXmlTest: public ::testing::Test
{
protected:
	void Constructors()
	{
		TracerXml* t1 = new TracerXml(controller_, 1, "out.xml");
		EXPECT_EQ(t1->filename_, "out.xml");
		EXPECT_EQ(t1->uid_, 1);
		EXPECT_FALSE(t1->stream_);
		delete t1;

		TracerXml* t2 = new TracerXml(controller_, 1);
		EXPECT_EQ(t2->filename_, "");
		EXPECT_EQ(t2->uid_, 1);
		EXPECT_FALSE(t2->stream_);
		delete t2;

	}

	void StartStop()
	{
		TracerXml* t = new TracerXml(controller_, 1, "out.xml");
		EXPECT_FALSE(t->stream_);
		EXPECT_THROW(t->PrintString("Error"), LogicDevsException);
		t->StartTracer(false);
		EXPECT_TRUE(t->stream_);
		t->StopTracer();
		EXPECT_FALSE(t->stream_);
		delete t;
		remove("out.xml");

		t = new TracerXml(controller_, 1, "");
		std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
		std::ofstream fout("/dev/null");
		std::cout.rdbuf(fout.rdbuf()); // redirect 'cout' to a 'fout'
		t->StartTracer(false);
		EXPECT_EQ(&std::cout, t->stream_);
		t->StopTracer();
		std::cout.rdbuf(cout_sbuf); // restore the original stream buffer
		delete t;
	}

	void Print()
	{
		TracerXml* t = new TracerXml(controller_, 1, "out.xml");
		t->StartTracer(false);
		t->PrintString("This is a test");
		t->StopTracer();
		std::ifstream o("out.xml");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("<trace>This is a test</trace>", out.str());
		EXPECT_THROW(t->PrintString("Error!"), LogicDevsException);
		remove("out.xml");
		delete t;
	}

	void TraceInitialize()
	{
		TracerXml* t = new TracerXml(controller_, 1, "out.xml");
		std::string modelpath = "examples/libAtomicModelTest";
		std::shared_ptr<AtomicDevs> model = cdevs_examples::atomic::TrafficLight::create("VerkeersLicht");
		model->setTimeNext(DevsTime(60, 0));
		t->StartTracer(false);
		t->TraceInitialize(model, DevsTime());
		controller_->PerformActions();
		t->StopTracer();
		std::ifstream o("out.xml");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/xml/TraceInitialize.xml");
		std::stringstream exp;
		exp << e.rdbuf();
		EXPECT_EQ(exp.str(), out.str());
		remove("out.xml");
		delete t;
	}

	virtual void SetUp()
	{
		controller_ = new Controller(NULL);
	}
	virtual void TearDown()
	{
		delete controller_;
	}

	Controller * controller_;
};

TEST_F(TracerXmlTest, Constructors)
{
	Constructors();
}

TEST_F(TracerXmlTest, StartStop)
{
	StartStop();
}

TEST_F(TracerXmlTest, Print)
{
	Print();
}

TEST_F(TracerXmlTest, TraceInitialize)
{
	TraceInitialize();
}

} /* namespace ns_DEVS */
