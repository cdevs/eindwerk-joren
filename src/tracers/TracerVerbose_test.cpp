#include "../tracers/TracerVerbose.h"
#include "../AtomicDevs.h"
#include "../../examples/atomicmodel_test/model.h"
#include "../exceptions/LogicDevsException.h"
#include <iostream>
#include "gtest/gtest.h"
#include "../Controller.h"
#include "atomicmodel_test/model.h"

namespace cdevs {

class TracerVerboseTest: public ::testing::Test
{
protected:
	void Constructors()
	{
		TracerVerbose* t1 = new TracerVerbose(controller_,  1, "out.txt");
		EXPECT_EQ(t1->filename_, "out.txt");
		EXPECT_EQ(t1->prevtime_, cdevs::DevsTime(-1, -1));
		EXPECT_EQ(t1->uid_, 1);
		EXPECT_FALSE(t1->stream_);
		delete t1;

		TracerVerbose* t2 = new TracerVerbose(controller_,  1);
		EXPECT_EQ(t2->filename_, "");
		EXPECT_EQ(t2->prevtime_, cdevs::DevsTime(-1, -1));
		EXPECT_EQ(t2->uid_, 1);
		EXPECT_FALSE(t2->stream_);
		delete t2;

		TracerVerbose* t3 = new TracerVerbose(0);
		EXPECT_EQ(t3->filename_, "");
		EXPECT_EQ(t3->prevtime_, cdevs::DevsTime(-1, -1));
		EXPECT_EQ(t3->uid_, 0);
		EXPECT_FALSE(t3->stream_);
		delete t3;
		remove("out.txt");

	}

	void StartStop(){
		TracerVerbose* t = new TracerVerbose(controller_,  1, "out.txt");
		EXPECT_FALSE(t->stream_);
		EXPECT_THROW(t->PrintString("Error"),LogicDevsException);
		t->StartTracer(false);
		EXPECT_TRUE(t->stream_);
		t->StopTracer();
		EXPECT_FALSE(t->stream_);
		delete t;
		remove("out.txt");

		t = new TracerVerbose(controller_,  1, "");
		t->StartTracer(false);
		EXPECT_EQ(&std::cout, t->stream_);
		t->StopTracer();
		delete t;
	}

	void Print()
	{
		TracerVerbose* t = new TracerVerbose(controller_,  1, "out.txt");
		t->StartTracer(false);
		t->PrintString("This is a test");
		t->StopTracer();
		std::ifstream o("out.txt");
		std::stringstream out;
		out << o.rdbuf();
		EXPECT_EQ("This is a test", out.str());
		EXPECT_THROW(t->PrintString("Error!"), LogicDevsException);
		remove("out.txt");
		delete t;
	}


	void TraceInitialize()
	{
		TracerVerbose* t = new TracerVerbose(controller_,  1, "out.txt");
		std::string modelpath = "examples/libAtomicModelTest";
		std::shared_ptr<AtomicDevs> model = cdevs_examples::atomic::TrafficLight::create("VerkeersLicht");
		model->setTimeNext(DevsTime(60, 0));
		t->StartTracer(false);
		t->TraceInitialize(model, DevsTime());
		controller_->PerformActions();
		t->StopTracer();
		std::ifstream o("out.txt");
		std::stringstream out;
		out << o.rdbuf();
		std::ifstream e("expected/verbose/TraceInitialize.txt");
		std::stringstream exp;
		exp << e.rdbuf();
		EXPECT_EQ(exp.str(), out.str());
		remove("out.txt");
		delete t;
	}


	virtual void SetUp()
	{
		controller_ = new Controller(NULL, 1);
	}
	virtual void TearDown()
	{
		delete controller_;
	}

	Controller * controller_;
};

TEST_F(TracerVerboseTest, Constructors)
{
	Constructors();
}

TEST_F(TracerVerboseTest, StartStop)
{
	StartStop();
}

TEST_F(TracerVerboseTest, Print)
{
	Print();
}

TEST_F(TracerVerboseTest, TraceInitialize)
{
	TraceInitialize();
}

} /* namespace ns_DEVS */
