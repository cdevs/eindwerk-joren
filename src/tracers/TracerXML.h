#ifndef TRACERXML_H_
#define TRACERXML_H_

#include <vector>
#include "../BaseDEVS.h"
#include "../tracers/Tracer.h"

namespace cdevs {

class TracerXml: public Tracer
{
	friend class TracerXmlTest;
	/*
	 * The Tracer generates an XML-file describing the simulation
	 */
public:
	using Tracer::Tracer;
	virtual ~TracerXml();

	void StartTracer(bool recover);
	void StopTracer();
	void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t);
	void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS);
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string name, std::string text);

	// TODO: implement! ~David
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text);
private:
	void RunEventTraceAtController(std::shared_ptr<AtomicDevs> model, std::string text);
};

}

#endif /* TRACERXML_H_ */
