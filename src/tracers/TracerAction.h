
#ifndef SRC_TRACERS_TRACERACTION_H_
#define SRC_TRACERS_TRACERACTION_H_

#include <string>
#include "Tracer.h"
#include "../AtomicDevs.h"

namespace cdevs {

/**
 * \brief An object that is used to store delayed actions
 *
 * This will be used when tracing. When the tracer wants to output
 * it calls the controller. The controller makes sure that the text
 * it output at the right time. To store all these outputs all information
 * of this output, including the model, the text, the tracer that called,
 * and the time of output will be stored.
 */
class TracerAction
{
public:
	TracerAction();
	TracerAction(DevsTime time, std::shared_ptr<AtomicDevs> model, std::string text, Tracer * tracer);
	virtual ~TracerAction();

	const std::string& getText() const;
	void setText(const std::string& text);

	Tracer* getTracer() const;
	void setTracer(Tracer* tracer);

	std::shared_ptr<AtomicDevs> getModel() const;
	void setModel(std::shared_ptr<AtomicDevs> model);

	const DevsTime& getTime() const;
	void setTime(const DevsTime& time);

private:
	DevsTime time_;
	std::shared_ptr<AtomicDevs> model_;
	std::string text_;
	Tracer * tracer_;
};

} /* namespace cdevs */

#endif /* SRC_TRACERS_TRACERACTION_H_ */
