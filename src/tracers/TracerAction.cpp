/*
 * TracerAction.cpp
 *
 *  Created on: 21 May 2015
 *      Author: ianvermeulen
 */

#include "TracerAction.h"

namespace cdevs {

/**
 * Constructor
 */
TracerAction::TracerAction()
{
	model_ = 0;
	tracer_ = 0;
}

/**
 * Constructor
 *
 * @param time The time to perform the action
 * @param model The model that is used in the action
 * @param text The text that will be send to the tracer
 * @param tracer The tracer to be used
 */
TracerAction::TracerAction(DevsTime time, std::shared_ptr<AtomicDevs> model, std::string text, Tracer* tracer)
{
	time_ = time;
	model_ = model;
	text_ = text;
	tracer_ = tracer;
}

/**
 * Destructor
 */
TracerAction::~TracerAction()
{
}

/**
 * Gets the text of the actions
 *
 * @return Text of the action
 */
const std::string& TracerAction::getText() const
{
	return text_;
}

/**
 * Sets the text of the actions
 *
 * @param Text of the action
 */
void TracerAction::setText(const std::string& text)
{
	text_ = text;
}

/**
 * Gets the tracer that performs the actions
 *
 * @return tracer of the action
 */
Tracer* TracerAction::getTracer() const
{
	return tracer_;
}

/**
 * Sets the tracer that performs the actions
 *
 * @param tracer of the action
 */
void TracerAction::setTracer(Tracer* tracer)
{
	tracer_ = tracer;
}

/**
 * Gets the model that is used in the action
 *
 * @return Model of the action
 */
std::shared_ptr<AtomicDevs> TracerAction::getModel() const
{
	return model_;
}

/**
 * Sets the model that is used in the action
 *
 * @param Model of the action
 */
void TracerAction::setModel(std::shared_ptr<AtomicDevs> model)
{
	model_ = model;
}

/**
 * Gets the time to perform the action
 *
 * @return time to perform the action
 */
const DevsTime& TracerAction::getTime() const
{
	return time_;
}

/**
 * Sets the time to perform the action
 *
 * @param time to perform the action
 */
void TracerAction::setTime(const DevsTime& time)
{
	time_ = time;
}

} /* namespace cdevs */

