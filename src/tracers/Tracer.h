#ifndef TRACER_H_
#define TRACER_H_

#include "../AtomicDevs.h"
#include <iostream>
#include <gtest/gtest_prod.h>
#include <fstream>
#include <cstddef>

namespace cdevs {

class Controller;

class Tracer
{
	/*
	 Base class for Tracer.

	 This class provides a start and stop method, a print method.
	 */
public:
	Tracer(Controller * controller);
	Tracer(Controller * controller, int uid, std::string filename="");
	virtual ~Tracer();
	virtual void StartTracer(bool recover);
	virtual void StopTracer();
	virtual void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t) = 0;
	virtual void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS) = 0;
	virtual void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS) = 0;
	virtual void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS) = 0;
	virtual void Trace(std::shared_ptr<AtomicDevs> aDEVS, DevsTime time, std::string text) = 0;

	void PrintString(std::string text) const;

	void Flush();
protected:
	Controller * controller_;

	int uid_;
	std::string filename_;
	std::ostream* stream_;
};

}



#endif
