#ifndef TRACERCELL_H_
#define TRACERCELL_H_

#include <fstream>
#include <utility>
#include <string>
#include <vector>
#include "../BaseDEVS.h"
#include "../tracers/Tracer.h"

namespace cdevs {

class Cell 
{

};

class TracerCell : public Tracer
{
public:
	using Tracer::Tracer;
	virtual ~TracerCell();

	void StartTracer(bool recover);
	void StopTracer();
	void TraceInitialize(BaseDevs& aDEVS, time_t t);
	void TraceInternal(BaseDevs& aDEVS);
	void TraceExternal(BaseDevs& aDEVS);
	void TraceConfluent(BaseDevs& aDEVS);
	void Trace(time_t time, std::string text);

	// TODO: implement! ~David
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text) {};
private:
	int x_size_;
	int y_size_;
	bool multifile_;
	double prevtime_;

	int cell_count_;
	std::vector<std::vector<Cell> > cells_;
};

}

#endif /* TRACERCELL_H_ */
