#include "../tracers/TracerVCD.h"

namespace cdevs
{
TracerVcd::~TracerVcd()
{
}

void TracerVcd::StartTracer(bool recover)
{
}

void TracerVcd::StopTracer()
{
}

void TracerVcd::TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t)
{
}

void TracerVcd::TraceInternal(std::shared_ptr<AtomicDevs> aDEVS)
{
}

void TracerVcd::TraceExternal(std::shared_ptr<AtomicDevs> aDEVS)
{
}

void TracerVcd::TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS)
{
}

void TracerVcd::Trace(DevsTime time, std::string text)
{
}
} /* namespace cdevs */
