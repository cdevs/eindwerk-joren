#include "../tracers/Tracer.h"
#include "../exceptions/LogicDevsException.h"

namespace cdevs
{
/*
 * \brief Default Tracer constructor, sets id to 0 a,d name to ""
 */
Tracer::Tracer(Controller * controller)
	: controller_(controller), uid_(0), filename_(""), stream_(nullptr)
{
}

/*
 * \brief Tracer constructor, sets id and filename
 * @param uid: Tracer id
 * @param filename: Name of the file the tracer should write to. If
 * left empty, it writes to the standard output
 */
Tracer::Tracer(Controller * controller, int uid, std::string filename)
	: controller_(controller), uid_(uid), filename_(filename), stream_(nullptr)
{
}

/*
 * \brief Creates the stream to write to, either by opening a file,
 * or by referencing std::cout
 * @param recover: false if starting a new simulation, true if
 * continuing from an interrupted one.
 */
void Tracer::StartTracer(bool recover)
{
	if (filename_ == "") {
		stream_ = &std::cout;
	} else if (recover) {
		stream_ = new std::ofstream(filename_, std::ofstream::out | std::ios::app);
	} else {
		stream_ = new std::ofstream(filename_, std::ofstream::out | std::ios::out);
	}
}

/*
 * \brief Stops the tracer. If not using std::cout, deletes the stream.
 */
void Tracer::StopTracer()
{
	if (stream_ != &std::cout) {
		delete stream_;
	}
	stream_ = nullptr;
}

/*
 * \brief Tracer destructor, if a stream was created, destroys it.
 */
Tracer::~Tracer()
{
	if (stream_ && (stream_ != &std::cout)) {
		delete stream_;
	}
	stream_ = nullptr;

}

/*
 * \brief Prints the string to the write medium
 * @param text: The string to print
 */
void Tracer::PrintString(std::string text) const
{
	if (stream_==nullptr)
		throw LogicDevsException("Tracer::PrintString: Tracer was not started!\n"+text);
	*stream_ << text;
}

void Tracer::Flush()
{
	stream_->flush();
}
} /* namespace cdevs */
