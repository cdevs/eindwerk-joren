#include "../tracers/TracerXML.h"
#include "../Controller.h"

namespace cdevs
{
TracerXml::~TracerXml()
{
}

void TracerXml::StartTracer(bool recover)
{
	Tracer::StartTracer(recover);
	if (!recover)
		PrintString("<trace>");
}

void TracerXml::StopTracer()
{
	PrintString("</trace>");
	Tracer::StopTracer();
}

void TracerXml::TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t)
{
	std::string text = "\n";
	text += "<init>\n";
	text += "<model>" + aDEVS->GetModelFullName() + "</model>\n";
	text += "<time>" + t.string() + "</time>\n";
	text += "<kind>EX</kind>\n";
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	text += "</init>\n";
	controller_->RunTrace(this, aDEVS, text);
}

void TracerXml::TraceInternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "";
	text += "<kind>IN</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_output_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->GetPortName() + "\" category=\"O\">\n";
		for (auto msg : port_msgs_pair.second) {
			text += "<message>" + msg + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	RunEventTraceAtController(aDEVS, text);
}

void TracerXml::TraceExternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "";
	text += "<kind>EX</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_input_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->GetPortName() + "\" category=\"I\">\n";
		for (auto msg : port_msgs_pair.second) {
			text += "<message>" + msg + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	RunEventTraceAtController(aDEVS, text);
}

void TracerXml::TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "";
	text += "<kind>IN</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_output_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->GetPortName() + "\" category=\"O\">\n";
		for (auto msg : port_msgs_pair.second) {
			text += "<message>" + msg + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";

	text += "<kind>EX</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_input_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->GetPortName() + "\" category=\"I\">\n";
		for (auto msg : port_msgs_pair.second) {
			text += "<message>" + msg + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	RunEventTraceAtController(aDEVS, text);
}

void TracerXml::RunEventTraceAtController(std::shared_ptr<AtomicDevs> model, std::string text)
{
	std::string text1 = "\n";
	text1 += "<event>\n";
	text1 += "<model>" + model->GetModelFullName() + "</model>\n";
	text1 += "<time>" + model->getTimeLast().string() + "</time>\n";
	text1 += text;
	text1 += "</event>\n";

	controller_->RunTrace(this, model, text1);
}

void TracerXml::Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text)
{
	PrintString(text);
}
}
