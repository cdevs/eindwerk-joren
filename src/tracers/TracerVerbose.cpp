#include "../tracers/TracerVerbose.h"

#include "../Port.h"
#include "../Controller.h"
#include <iostream>
#include <string>
#include <cfloat>

namespace cdevs
{
TracerVerbose::TracerVerbose(Controller * controller)
	: Tracer(controller), prevtime_(-1,-1)
{
	prevtime_ = DevsTime(-1, -1);
}

TracerVerbose::TracerVerbose(Controller * controller, int uid, std::string filename)
	: Tracer(controller, uid, filename), prevtime_(-1,-1)
{
}

TracerVerbose::~TracerVerbose()
{
}

void TracerVerbose::TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t)
{
	tracemutex_.lock();
	std::string text = "\n";
	text += "\tINITIAL CONDITIONS in model <" + aDEVS->GetModelFullName() + ">\n";
	text += "\t\tInitial State: " + aDEVS->getState().string() + "\n";
	text += "\t\tNext scheduled internal transition at time " + aDEVS->getTimeNext().string() + "\n";
	controller_->RunTrace(this, aDEVS, text);
	// Trace(aDEVS, aDEVS->getTimeLast(), text);
	tracemutex_.unlock();
}

void TracerVerbose::TraceInternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	tracemutex_.lock();
	std::string text = "\n";
	text += "\tINTERNAL TRANSITION in model <" + aDEVS->GetModelFullName() + ">\n";
	text += "\t\tNew State: " + aDEVS->getState().string() + "\n";
	text += "\t\tOutput Port Configuration:\n";

	for (auto port_msgs_pair : aDEVS->my_output_) {
		text += "\t\t\tport <" + port_msgs_pair.first.lock()->GetPortName() + ">:\n";
		for (auto msg : port_msgs_pair.second) {
			text += "\t\t\t\t" + msg + "\n";
		}
	}

	text += "\t\tNext scheduled internal transition at time " + aDEVS->getTimeNext().string() + "\n";
	controller_->RunTrace(this, aDEVS, text);
	// Trace(aDEVS, aDEVS->getTimeLast(), text);
	tracemutex_.unlock();
}

void TracerVerbose::TraceExternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	tracemutex_.lock();
	std::string text = "\n";
	text += "\tEXTERNAL TRANSITION in model <" + aDEVS->GetModelFullName() + ">\n";
	text += "\t\tInput Port Configuration:\n";
	for (auto& port : aDEVS->getInputPorts()) {
		text += "\t\t\tport <" + port->GetPortName() + ">: \n";
		for (auto msg : aDEVS->my_input_[port]) {
			text += "\t\t\t\t" + msg + "\n";
		}
	}
	text += "\t\tNew State: " + aDEVS->getState().string() + "\n";
	text += "\t\tNext scheduled internal transition at time " + aDEVS->getTimeNext().string() + "\n";
	controller_->RunTrace(this, aDEVS, text);
	// Trace(aDEVS, aDEVS->getTimeLast(), text);
	tracemutex_.unlock();
}

void TracerVerbose::TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS)
{
	tracemutex_.lock();
	std::string text = "\n";
	text += "\tCONFLUENT TRANSITION in model <" + aDEVS->GetModelFullName() + ">\n";
	text += "\t\tInput Port Configuration:\n";
	for (auto& port : aDEVS->getInputPorts()) {
		text += "\t\t\tport <" + port->GetPortName() + ">: \n";
		for (auto msg : aDEVS->my_input_[port]) {
			text += "\t\t\t\t" + msg + "\n";
		}
	}
	text += "\t\tNew State: " + aDEVS->getState().string() + "\n";
	text += "\t\tOutput Port Configuration:\n";

	for (auto& port : aDEVS->getOutputPorts()) {
		text += "\t\t\tport <" + port->GetPortName() + ">:\n";
		for (auto msg : aDEVS->my_output_[port]) {
			text += "\t\t\t\t" + msg + "\n";
		}
	}

	text += "\t\tNext scheduled internal transition at time " + aDEVS->getTimeNext().string() + "\n";
	controller_->RunTrace(this, aDEVS, text);
	// Trace(aDEVS, aDEVS->getTimeLast(), text);
	tracemutex_.unlock();
}

void TracerVerbose::Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text)
{
	if (time > this->prevtime_) {
		text = "\n__  Current Time: " + time.string() + " " + std::string(42, '_') + " \n\n" + text;
		prevtime_ = time;
	}

	PrintString(text);
}
} /* namespace cdevs */
