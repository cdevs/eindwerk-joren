#ifndef TRACERVCD_H_
#define TRACERVCD_H_

#include <fstream>
#include <string>
#include <vector>
#include "../BaseDEVS.h"
#include "../tracers/Tracer.h"
#include "../VCDVariable.h"

namespace cdevs {

class TracerVcd: public Tracer
{
public:
	using Tracer::Tracer;
	virtual ~TracerVcd();

	void StartTracer(bool recover);
	void StopTracer();

	void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t);
	void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS);
	void Trace(DevsTime time, std::string text);

	// TODO: implement! ~David
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text) {};
private:
	std::vector<VcdVariable> vcd_var_list_;
	double vcd_prevtime;
};

}

#endif /* TRACERVCD_H_ */
