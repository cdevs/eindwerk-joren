#ifndef SRC_TRACERS_TRACERJSON_H_
#define SRC_TRACERS_TRACERJSON_H_

#include "TracerXML.h"
namespace cdevs {

class TracerJson: public Tracer
{
	friend class TracerJsonTest;
public:
	TracerJson(int uid, std::string filename="");
	virtual ~TracerJson();

	void StartTracer(bool recover);
	void StopTracer();
	void TraceInitialize(AtomicDevs& aDEVS, DevsTime t);
	void TraceInternal(AtomicDevs& aDEVS);
	void TraceExternal(AtomicDevs& aDEVS);
	void TraceConfluent(AtomicDevs& aDEVS);
	void Trace(DevsTime time, std::string name, std::string text);
private:
	std::string MakeTrace(AtomicDevs& aDEVS, bool internal);
	std::string PortsToJson(std::string port_kind, std::map<Port *, std::list<std::string> >& my_output_);
	bool first_event_;
};

} /* namespace cdevs */

#endif /* SRC_TRACERS_TRACERJSON_H_ */
