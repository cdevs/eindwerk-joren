#ifndef TRACERVERBOSE_H_
#define TRACERVERBOSE_H_

#include <fstream>
#include <string>
#include <vector>
#include <mutex>

#include "../tracers/Tracer.h"
#include "../utility.h"

namespace cdevs {

class TracerVerbose : public Tracer
{
	friend class TracerVerboseTest;

	/*
	 * A Tracer that traces the simulation in a verbose way.
	 */
public:
	TracerVerbose(Controller * controller);
	TracerVerbose(Controller * controller, int uid, std::string filename="");
	virtual ~TracerVerbose();

	void TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t);
	void TraceInternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceExternal(std::shared_ptr<AtomicDevs> aDEVS);
	void TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS);
	void Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text);

	template<typename T>
	void TraceUser(DevsTime time, std::shared_ptr<AtomicDevs> devs, std::string variable, T value);
private:
	DevsTime prevtime_;

	std::mutex tracemutex_;
};

template<typename T>
inline void TracerVerbose::TraceUser(DevsTime time, std::shared_ptr<AtomicDevs> devs,
		std::string variable, T value) {
}

} /* namespace cdevs */
#endif /* TRACERVERBOSE_H_ */
