/*
 * AtomicModelTest.cpp
 *
 *  Created on: 31-mrt.-2015
 *      Author: david
 */
#include "AtomicDevs.h"
#include "CoupledDEVS.h"
#include "utility.h"
#include <algorithm>
#include <limits>

using namespace cdevs;

class TrafficLightMode: public State<TrafficLightMode>
{
public:
	TrafficLightMode(std::string value = "red")
		: State(value)
	{
	}
};

class TrafficLight: public AtomicDevs
{
public:
	TrafficLight(std::string name) : AtomicDevs(name)
	{
		state_ = new TrafficLightMode();

		elapsed_ = 1.5;

		INTERRUPT = AddInPort("INTERRUPT");
		OBSERVED = AddOutPort("OBSERVED");

	}

	State * ExtTransition(std::map<Port, std::vector< std::string> > inputs)
	{
		std::string input = inputs.at(INTERRUPT).at(0);

		std::string state = state_->getValue();

		if (input == "toManual") {
			if (state == "manual") {
				return new TrafficLightMode("manual");
			} else if (state == "red" || state == "green" || state == "yellow") {
				return new TrafficLightMode("manual");
			}
		} else if (input == "toAutonomous") {
			if (state == "manual") {
				return new TrafficLightMode("red");
			} else if (state == "red" || state == "green" || state == "yellow") {
				return state_;
			}
		}
		// TODO: throw exception!
	}

	State * IntTransition()
	{
		std::string state = state_->getValue();

		if (state == "red")
			return new TrafficLightMode("green");
		else if (state == "green")
			return new TrafficLightMode("yellow");
		else if (state == "yellow")
			return new TrafficLightMode("red");
		// TODO: throw exception!
	}

	std::map<Port, std::vector<std::string> > OutputFunction()
	{
		std::string state = state_->getValue();

		if (state == "red")
			return { {	OBSERVED, {"grey"}}};
		else if (state == "green")
			return { {	OBSERVED, {"yellow"}}};
		else if (state == "yellow")
			return { {	OBSERVED, {"grey"}}};
		// TODO: throw exception!
	}

	double TimeAdvance()
	{
		std::string state = state_->getValue();

		if (state == "red")
			return 60;
		else if (state == "green")
			return 50;
		else if (state == "yellow")
			return 10;
		else if (state == "manual")
			return std::numeric_limits<double>::infinity();
		// TODO: throw exception!
	}

	Port INTERRUPT;
	Port OBSERVED;
};

class PolicemanMode: public State
{
public:
	PolicemanMode(std::string value = "idle")
		: State(value)
	{
	}
};

class Policeman: public AtomicDevs
{
public:
	Policeman(std::string name) : AtomicDevs(name)
	{
		state_ = new PolicemanMode();

		elapsed_ = 0.0;

		OUT = AddOutPort("OUT");
	}

	State * IntTransition()
	{
		std::string state = state_->getValue();

		if (state == "idle")
			return new PolicemanMode("working");
		else if (state == "working")
			return new PolicemanMode("idle");
		// TODO: throw exception!
	}

	std::map<Port, std::vector<std::string> > OutputFunction()
	{
		std::string state = state_->getValue();

		if (state == "idle")
			return { {	OUT, {	"toManual"}}};
		else if (state == "working")
			return { {	OUT, {	"toAutonomous"}}};
		// TODO: throw exception!
	}

	double TimeAdvance()
	{
		std::string state = state_->getValue();

		if (state == "idle")
			return 200;
		else if (state == "working")
			return 100;
		// TODO: throw exception!
	}

	Port OUT;
};

class TrafficSystem: public CoupledDevs
{
public:
	TrafficSystem(std::string name) : CoupledDevs(name)
	{
		// Declare the coupled model's output ports:
		// Autonomous, so no output ports

		// Declare the coupled model's sub-models:

		// The Policeman generating interrupts
		policeman = dynamic_cast<Policeman *>(AddSubModel(new Policeman("policeman")));

		// The TrafficLight
		trafficlight = dynamic_cast<TrafficLight *>(AddSubModel(new TrafficLight("trafficlight")));

		// Only connect ...
		ConnectPorts(&policeman->OUT, &trafficlight->INTERRUPT);
	}

	std::shared_ptr<BaseDevs> Select(std::vector<std::shared_ptr<BaseDevs>> imm_children) const
	{
		if (std::find(imm_children.begin(), imm_children.end(), policeman) == imm_children.end()) {
			return imm_children.at(0);
		} else {
			return policeman;
		}
	}
private:
	Policeman * policeman;
	TrafficLight * trafficlight;
};

int main(){
	TrafficSystem traffic("Traffic");
	return 0;
}
