/*
 * SimulatorConfiguration.cpp
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#include "SimulatorConfiguration.h"

namespace cdevs {

SimulatorConfiguration::SimulatorConfiguration()
{
	// TODO Auto-generated constructor stub

}

void SimulatorConfiguration::SetClassicDevs(bool classicdevs)
{
}

void SimulatorConfiguration::SetMemo(bool memo)
{
}

void SimulatorConfiguration::SetDsDevs(bool dsdevs)
{
}

void SimulatorConfiguration::SetAllowLocalReinit(bool allowed)
{
}

void SimulatorConfiguration::SetManualRelocator()
{
}

void SimulatorConfiguration::SetRelocationDirective(double time)
{
}

void SimulatorConfiguration::SetRelocationDirectives()
{
}

void SimulatorConfiguration::SetSchedulerCustom()
{
}

void SimulatorConfiguration::SetSchedulerActivityHeap(bool locations)
{
}

void SimulatorConfiguration::SetSchedulePoly(bool locations)
{
}

void SimulatorConfiguration::SetSchedulerDirtyheap(bool locations)
{
}

void SimulatorConfiguration::SetSchedulerDiscreteTime(bool locations)
{
}

void SimulatorConfiguration::SetScheduleSortedList(bool locations)
{
}

void SimulatorConfiguration::SetSchedulerMinimalList(bool locations)
{
}

void SimulatorConfiguration::SetSchedulerNoAge(bool locations)
{
}

void SimulatorConfiguration::SetSchedulerHeapSet(bool locations)
{
}

void SimulatorConfiguration::SetShowProgress(bool locations)
{
}

void SimulatorConfiguration::SetTerminationModel(bool model)
{
}

void SimulatorConfiguration::RegisterState()
{
}

void SimulatorConfiguration::SetDrawModel()
{
}

void SimulatorConfiguration::SetFetchAllAfterSimulation(bool fetch)
{
}

void SimulatorConfiguration::SetActivityTrackingVisual()
{
}

void SimulatorConfiguration::SetLocationCellMap()
{
}

void SimulatorConfiguration::SetTerminationCondition(bool (&f)(void))
{
}

void SimulatorConfiguration::SetTerminationTime(double time)
{
}

void SimulatorConfiguration::SetVerbose(std::string fname)
{

}

void SimulatorConfiguration::SetRemoveTracers()
{
}

void SimulatorConfiguration::SetCell(double x, double y, std::string filename)
{
}

void SimulatorConfiguration::SetXml(std::string filename)
{
}

void SimulatorConfiguration::SetVcd(std::string filename)
{
}

void SimulatorConfiguration::SetCustomTracer()
{
}

void SimulatorConfiguration::SetLogging()
{
}

void SimulatorConfiguration::SetGvtInterval(double gvt_int)
{
}

void SimulatorConfiguration::SetCheckpointing(std::string name, int checkpoint_interval)
{
}

void SimulatorConfiguration::SetStateSaving()
{
}

void SimulatorConfiguration::SetMessageCopy(std::string method)
{
}

void SimulatorConfiguration::SetRealtime(bool realtime, double scale)
{
}

void SimulatorConfiguration::SetRealtimeInputFile(std::string file)
{
}

void SimulatorConfiguration::SetRealtimePlatformThreads()
{
}

void SimulatorConfiguration::SetModelState()
{
}

void SimulatorConfiguration::SetModelStateAttributes()
{
}

void SimulatorConfiguration::SetModelAttribute()
{
}

void SimulatorConfiguration::SetActivityRelocatorCustom()
{
}

void SimulatorConfiguration::SetAcitvityRelocatorBasicBound()
{
}

void SimulatorConfiguration::SetGreedyAlloc()
{
}

void SimulatorConfiguration::SetAutoAlloc()
{
}

void SimulatorConfiguration::SetInitialAlloc()
{
}

SimulatorConfiguration::~SimulatorConfiguration()
{
	// TODO Auto-generated destructor stub
}

} /* namespace ns_DEVS */
