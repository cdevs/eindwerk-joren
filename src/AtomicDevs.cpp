/*
 * AtomicDevs.cpp
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#include "AtomicDevs.h"
#include <cmath>
#include <ctime>
#include <limits>
#include <iostream>

namespace cdevs {

AtomicDevs::AtomicDevs(std::string name) : BaseDevs(name), relocatable_(true)
{
	last_read_time_.setX(0);
	last_read_time_.setY(0);

}

void AtomicDevs::SetLocation(int location, bool force)
{
	if (location_ < 0 || force) {
		location_ = location;
	}
}

void AtomicDevs::FetchActivity(double time)
{
	double accumulator = 0.0;
	for (const StateBase* state : this->_old_states) {
		if (state->time_last_.getX() < time) {
			//accumulator += state.activity;
		}
	}
	// TODO: update activity dictionary
}

void AtomicDevs::SetGvt(double new_gvt, bool last_state_only)
{
	// float activity = 0.0;
	int copy = 0;
	bool copied = false;
	//for (State state : this->_old_states) {
	for(unsigned i = 0; i < this->_old_states.size(); i++) {
		StateBase* state = this->_old_states.at(i);
		if (state->time_last_.getX() >= new_gvt) {
			copy = std::fmax(0, i-1);
			copied = true;
		} else if (!last_state_only) {
			//activity += state.activity;
		}
	}

	if (this->_old_states.empty()) {
		// TODO: exception: "Model has no memory of the past!"
	} else if (!copied) {
		this->_old_states.erase(this->_old_states.begin(), this->_old_states.end()-2);
	} else {
		this->_old_states.erase(this->_old_states.begin(), this->_old_states.begin() + copy);
	}

	//if (last_state_only)

}

bool AtomicDevs::Revert(DevsTime time, bool memo)
{
	int new_state = this->_old_states.size() - 1; // index in _old_states
	for (std::vector<StateBase*>::reverse_iterator rit = this->_old_states.rbegin(); rit != this->_old_states.rend(); rit++) {
		if ((*rit)->time_last_ < time) {
			break;
		}
		new_state--;
	}

	StateBase* state = _old_states.at(new_state);
	this->time_last_ = state->time_last_;
	this->time_next_ = state->time_next_;

	// memo is for distributed DEVS

	_old_states.erase(_old_states.begin(), _old_states.begin() + new_state);

	if(last_read_time_ > time) {
		last_read_time_.setX(0);
		last_read_time_.setY(0);
		return true;
	}

	return false;
}

/*StateBase const& AtomicDevs::ExtTransition(std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs)
{
	return *state_;
}

StateBase const& AtomicDevs::IntTransition()
{
	return *state_;
}

StateBase const& AtomicDevs::ConfTransition(std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs)
{
	setState( IntTransition() );
	setState( ExtTransition(inputs) );
	return *state_;
}*/

std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > AtomicDevs::OutputFunction()
{
	return {};
}

double AtomicDevs::TimeAdvance()
{
	return std::numeric_limits<double>::infinity();
}

/**
 * \brief Performs the transitions up to the given time
 *
 * @param until_time The time up to which transitions should be performed
 * @returns true if the there was a delayed transition performed
 */
bool AtomicDevs::PerformDelayedTransition(DevsTime until_time)
{
	if(!delayed_transitions_.empty())
	{
		DevsTime transition = delayed_transitions_.top();

		if(transition < getTimeNext() && transition < until_time)
		{
			setState(ExtTransition(my_input_));
			delayed_transitions_.pop();
			return true;
		}
	}

	return false;
}

AtomicDevs::~AtomicDevs()
{
	// TODO Auto-generated destructor stub
}

double AtomicDevs::PreActivityCalculation()
{
	std::time_t timer;
	std::time(&timer);
	return static_cast<double>(timer);
}

double AtomicDevs::PostActivityCalculation(double prevalue)
{
	std::time_t timer;
	std::time(&timer);
	return static_cast<double>(timer) - prevalue;
}

void AtomicDevs::FlattenConnections()
{
	for(auto& port : input_ports_)
		port->SetHostDEVS(getPtr() );
	for(auto& port : output_ports_)
		port->SetHostDEVS(getPtr() );
}

void AtomicDevs::UnflattenConnections()
{
	for(auto& port : input_ports_)
		port->ResetHostDEVS();
	for(auto& port : output_ports_)
		port->ResetHostDEVS();
}

int AtomicDevs::Finalize(std::string name, int model_counter, std::map<int, std::shared_ptr<BaseDevs>> & model_ids, std::list<std::shared_ptr<BaseDevs>> select_hierarchy)
{
	// Give a name
	this->full_name_ = name + GetModelName();

	// Give a unique ID to the model itself
	model_id_ = model_counter;
	select_hierarchy_ = select_hierarchy; // optimizable by allocating enough space for this model as well
	select_hierarchy_.push_back(getPtr() );

	// Add the element to its designated place in the model_ids list
	model_ids[model_id_] = getPtr();
	//model_ids.insert(std::pair<model_id_, this>);
	// Do a quick check, since this is vital to correct operation
	if (model_ids.at(model_id_) != getPtr()) {
		// TODO: Exception!
	}

	//locations[location_].push_back(model_id_);

	// Return the unique ID counter, incremented so it stays unique
	return ++model_counter;
}

int AtomicDevs::GetModelLoad(std::map<int, double> loads)
{
	if (loads.find(location_) == loads.end()) {
		//TODO: exception! (or assert as precondition?)
	}
	loads[location_]++;
	num_children_ = 1;
	return num_children_;
}

/**
 * \brief Gets the state of the AtomicDevs
 *
 * @return State of AtomicDevs
 */
// template <class T, class S>
// State<T, S> const& AtomicDevs::getState() const
/*StateBase const& AtomicDevs::getState() const
{
	return *state_;
}*/

// template <class T, class S>
// State<T, S> const& AtomicDevs::getState(DevsTime request_time, bool first_call) const
/*StateBase const& AtomicDevs::getState(DevsTime request_time, bool first_call) const
{
	// TODO: implement
}*/

/**
 * \brief Sets the state of the AtomicDevs
 *
 * TODO: We should think this downcast through. We could hide the constructor of
 * StateBase or try taking State as argument to this method. We could also simply
 * throw an error if state doesn't inherit State.
 *
 * @return State of AtomicDevs
 */
// template <class T, class S>
// void AtomicDevs::setState(State<T, S> const& state)
/*void AtomicDevs::setState(StateBase const& state)
{
	state_.reset(&state.clone());
}*/

/**
 * \brief Adds a delayed transition to the AtomicDevs
 *
 * @param time The time to perform the transition
 * @param state The state to transition to
 */
void AtomicDevs::AddDelayedTransition(DevsTime time)
{
	delayed_transitions_.push(time);
}

}

