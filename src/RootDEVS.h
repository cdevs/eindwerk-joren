/*
 * RootDEVS.h
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#ifndef ROOTDEVS_H_
#define ROOTDEVS_H_

#include "BaseDEVS.h"
#include "templates/Scheduler.h"
#include "AtomicDevs.h"
#include "utility.h"
#include <cmath>

namespace cdevs {

class RootDevs: public BaseDevs, public std::enable_shared_from_this<RootDevs>
{
public:
	RootDevs(std::list<std::shared_ptr<AtomicDevs> > components, std::list<std::shared_ptr<BaseDevs> > models, SchedulerType type);
	virtual ~RootDevs();

	void UndoDirectConnect();
	std::list<std::shared_ptr<BaseDevs> > DirectConnect();

	void SetScheduler(SchedulerType scheduler_type);
	Scheduler * GetScheduler();
	void SetGvt(double gvt, std::vector<int> activities, bool last_state_only);

	void FetchActivity(DevsTime at_time, std::map<int, float>& activities);
	bool Revert(DevsTime at_time, bool memorize);

	void AddComponent(std::shared_ptr<AtomicDevs> component);
	std::list<std::shared_ptr<AtomicDevs> > GetComponentSet();

	SchedulerType getSchedulerType();

	void FlattenConnections();
	void UnflattenConnections();
	void SetLocation(int location, bool force);
	int GetModelLoad(std::map<int, double> loads);
	int Finalize(std::string name, int model_counter,
			std::map<int, std::shared_ptr<BaseDevs>>& model_ids, std::list<std::shared_ptr<BaseDevs>> select_hierarchy);
	void SetTimeNext();

	std::shared_ptr<BaseDevs> getPtr() { return shared_from_this(); };

private:
	std::list<std::shared_ptr<AtomicDevs> > components_;
	std::list<std::shared_ptr<BaseDevs> > models_;
	// TODO: unused! (david)
	// std::list<int> local_model_ids_;

	std::shared_ptr<Scheduler> scheduler_;
	SchedulerType scheduler_type_;

	bool direct_connected_;
};

} /* namespace ns_DEVS */

#endif /* ROOTDEVS_H_ */
