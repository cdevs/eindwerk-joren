#include "VCDVariable.h"

namespace cdevs
{
/**
 * Constructor
 *
 * @param model_name	New name of the model in the variable
 * @param port_name	New name of the port in the variable
 */
VcdVariable::VcdVariable(const std::string& model_name, const std::string& port_name)
{
	this->model_name_ = model_name;
	this->port_name_ = port_name;
}

/**
 * Sets model name
 *
 * @param model_name	New name of the model in the variable
 */
void VcdVariable::set_model_name(std::string model_name)
{
	this->model_name_ = model_name;
}

/**
 * Sets model name
 *
 * @param port_name	New name of the port in the variable
 */
void VcdVariable::set_port_name(std::string port_name)
{
	this->port_name_ = port_name;
}

/**
 * Gets model name
 *
 * @return	Name of the model in the variable
 */
const std::string VcdVariable::model_name()
{
	return model_name_;
}

/**
 * Gets port name
 *
 * @return	Name of the port in the variable
 */
const std::string VcdVariable::port_name()
{
	return port_name_;
}
} /* namespace cdevs */
