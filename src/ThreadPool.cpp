#include "ThreadPool.h"
#include <iostream>

/**
 * Constructor
 *
 * @param threadPool	Pointer to the threadpool
 *
 */
Worker::Worker(ThreadPool* threadPool)
{
	this->_threadPool = threadPool;

}

void Worker::operator()()
{
	std::function<void()> task;

	while (true) {
		{
			//enter critical (lock)
			std::unique_lock<std::mutex> lock(_threadPool->GetMutexReference());

			while (!_threadPool->IsStopping() && !_threadPool->HasTasks()) {
				_threadPool->Wait(lock);
			}

			if (_threadPool->IsStopping())
				return;

			task = _threadPool->PopTask();
			//leave critical
		}
		task();
	}
}

Worker::~Worker()
{
}

/**
 * Constructor
 *
 * @param nWorkers	Amount of workers in the threadpool
 *
 */
ThreadPool::ThreadPool(size_t nWorkers)
{
	stop_ = false;

	// create workers
	for (size_t i = 0; i < nWorkers; i++) {
		// create the worker thread

		// add the worker to out workers
		workers_.push_back(std::thread(Worker(this)));
	}
}

/**
 * Destructor
 *
 */
ThreadPool::~ThreadPool()
{
	// stop all threads
	stop_ = true;
	condition_.notify_all();

	for (size_t i = 0; i < workers_.size(); i++) {
		workers_.at(i).join();
	}
	auto end = std::chrono::steady_clock::now();

	// Store the time difference between start and end
	auto diff = end - start_time_;

	std::cout << std::chrono::duration<double, std::milli>(diff).count() << " ms" << std::endl;
}

/**
 * Enqueues a task to the threadpool
 *
 * All enqueued tasks will be handled by a series of workers (threads)
 *
 * @param task	The task to enqueue
 *
 */
template<class F>
void ThreadPool::AddTask(F f)
{
	{
		// acquire lock
		std::unique_lock<std::mutex> lock(queue_mutex_);

		// add the task
		tasks_.push(std::function<void()>(f));

		//release lock
	}
	// call a thread
	condition_.notify_one();
}

/**
 * Makes the thread pool wait (when there are no tasks)
 *
 * @param lock	The lock to use to block threads until condition variable is notified.
 */

void ThreadPool::Wait(std::unique_lock<std::mutex>& lock)
{
	this->condition_.wait(lock);
}

/**
 * Checks if ThreadPool is stopping
 *
 * @return	True if the ThreadPool is stopping, false if it isn't
 */
bool ThreadPool::IsStopping()
{
	return stop_;
}

/**
 * Checks if ThreadPool has tasks
 *
 * @return	True if the ThreadPool has tasks, false if it doesn't
 */
bool ThreadPool::HasTasks()
{
	return !(tasks_.empty());
}

/**
 * Pops a task from the queue
 *
 * @return	The task in front of the queue
 */
std::function<void()> ThreadPool::PopTask()
{
	// get the front task
	std::function<void()> task = this->tasks_.front();

	// pop it off
	this->tasks_.pop();

	// return it
	return task;
}

/**
 * Get mutex reference
 *
 * @return	A reference to the mutex object of the queue
 */
std::mutex& ThreadPool::GetMutexReference()
{
	return this->queue_mutex_;
}

void ThreadPool::SetStartTime(std::chrono::steady_clock::time_point startTime)
{
	this->start_time_ = startTime;
}
