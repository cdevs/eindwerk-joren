#ifndef TRACERS_H_
#define TRACERS_H_

#include <iostream>
#include <fstream>
#include <utility>
#include <string>
#include <vector>
#include <memory>
#include "AtomicDevs.h"
#include "tracers/Tracer.h"


namespace cdevs {

class Tracers
{
public:
	Tracers();
	virtual ~Tracers();
	void RegisterTracer(std::shared_ptr<Tracer>  tracer, bool recover);
	bool HasTracers() const;
	std::shared_ptr<Tracer> GetById(int uid) const;
	std::vector<std::shared_ptr<Tracer> > GetTracers() const;

	void StartTracers(bool recover);
	void StopTracers();

//	void Trace(DevsTime time, std::string text);
	template<typename T>
	void TracesUser(int time, std::shared_ptr<AtomicDevs> devs, std::string variable, T value);
	void TracesInitialize(std::shared_ptr<AtomicDevs> devs, DevsTime t);
	void TracesInternal(std::shared_ptr<AtomicDevs> devs);
	void TracesExternal(std::shared_ptr<AtomicDevs> devs);
	void TracesConfluent(std::shared_ptr<AtomicDevs> devs);
	void Reset();
private:
	std::vector<std::shared_ptr<Tracer> > tracers_;

	// In python: holds all information needed to start a tracer: (file, classname, [args])
	std::vector<std::shared_ptr<Tracer> > tracers_init_;
	int uid_;
};

template<typename T>
inline void Tracers::TracesUser(int time, std::shared_ptr<AtomicDevs> devs, std::string variable, T value)
{
}
}
#endif /* TRACERS_H_ */
