#include "CoupledDEVS.h"

namespace cdevs {

CoupledDevs::CoupledDevs(std::string name) :
		BaseDevs(name) {
	is_coupled_ = true;
}

void CoupledDevs::ForceSequential() {
	SetLocation(0, true);
}

std::shared_ptr<BaseDevs> CoupledDevs::Select(std::list<std::shared_ptr<BaseDevs>> imm_children) const {
	return imm_children.front();
}

int CoupledDevs::GetModelLoad(std::map<int, double> loads) {
	int children = 0;
	for (auto& child : component_set_) {
		children += child->GetModelLoad(loads);
	}
	num_children_ = children;
	return num_children_;
}

int CoupledDevs::Finalize(std::string name, int model_counter,
		std::map<int, std::shared_ptr<BaseDevs>>& model_ids,
		std::list<std::shared_ptr<BaseDevs>> select_hierarchy) {
	// Set name, even though it will never be requested
	this->full_name_ = name + GetModelName();

	for (auto& child : component_set_) {
		model_counter = child->Finalize(name, model_counter, model_ids,
				select_hierarchy);
	}

	// Return the unique ID counter
	return model_counter;
}

void CoupledDevs::SetLocation(int location, bool force) {
	if (!location_ < 0 || force) {
		location_ = location;
		for (auto& child : component_set_) {
			child->SetLocation(location, force);
		}
	}
}

void CoupledDevs::FlattenConnections() {
	for (auto& child : component_set_) {
		child->FlattenConnections();
	}
}

void CoupledDevs::UnflattenConnections() {
	for (auto& child : component_set_) {
		child->UnflattenConnections();
	}
}

void CoupledDevs::AddSubModel(std::shared_ptr<BaseDevs> model, int location) {
	// model->SetParent( getPtr() );

	// TODO: location checks!
	model->setFullName(full_name_ + "." + model->getFullName());
	model->SetLocation(location, true);
	if (std::dynamic_pointer_cast<std::shared_ptr<CoupledDevs> >(model)) {
		for (auto& child : component_set_) {
			child->SetLocation(model->GetLocation(), true);
		}
	}
	component_set_.push_back(model);
}

void CoupledDevs::RemoveSubmodel(std::weak_ptr<BaseDevs> model) {
	// TODO: remove or unschedule?
}

void CoupledDevs::DisconnectPorts(std::weak_ptr<Port> port1, std::weak_ptr<Port> port2) {
	auto port1_sp = port1.lock();
	auto port2_sp = port2.lock();
	std::vector<std::weak_ptr<Port>> new_connection;
	bool found = false;
	for (auto& port : port1_sp->outline_) {
		if (port.lock() == port2_sp && !found)
			found = true;
		else
			new_connection.push_back(port);
	}

	port1_sp->outline_ = new_connection;

	new_connection.clear();
	found = false;
	for (auto& port : port2_sp->inline_) {
		if (port.lock() == port1_sp && !found)
			found = true;
		else
			new_connection.push_back(port);
	}
	port2_sp->inline_ = new_connection;
	// TODO: self.server.getSelfProxy().dsUndoDirectConnect() ?
}

void CoupledDevs::ConnectPorts(std::weak_ptr<Port> port1, std::weak_ptr<Port> port2) {
	ports_to_connect_.push_back(std::make_pair(port1, port2));
}

void CoupledDevs::ActuallyConnectPorts() {
	for (auto& p : ports_to_connect_) {
		auto& port1 = p.first;
		auto& port2 = p.second;
		auto port1_sp = port1.lock();
		auto port2_sp = port2.lock();
		// Internal coupling
		if ((port1_sp->host_DEVS.lock()->GetParent().lock() == getPtr()
				&& port2_sp->host_DEVS.lock()->GetParent().lock() == getPtr())
				&& (!port1_sp->IsInput() && port2_sp->IsInput())) {
			if (port1_sp->host_DEVS.lock() == port2_sp->host_DEVS.lock()) {
				throw RuntimeDevsException(
						"Illegal coupling between " + port1_sp->name_ + " and "
								+ port2_sp->name_ + " host DEVS are the same!");
			} else {
				port1_sp->outline_.push_back(port2);
				port2_sp->inline_.push_back(port1);
			}
			// External input coupling
		} else if ((port1_sp->host_DEVS.lock() == getPtr()
				&& port2_sp->host_DEVS.lock()->GetParent().lock() == getPtr())
				&& (port1_sp->IsInput() && port2_sp->IsInput())) {
			port1_sp->outline_.push_back(port2);
			port2_sp->inline_.push_back(port1);
			// External output coupling
		} else if ((port1_sp->host_DEVS.lock() == getPtr()
				&& port2_sp->host_DEVS.lock()->GetParent().lock() == getPtr())
				&& (!port1_sp->IsInput() && !port2_sp->IsInput())) {
			port1_sp->outline_.push_back(port2);
			port2_sp->inline_.push_back(port1);
			// Other cases (illegal coupling):
		} else {
			throw RuntimeDevsException(
					"Illegal coupling between " + port1_sp->name_ + " and "
							+ port2_sp->name_);
		}

		// TODO: support transition functions
		// port1->z_functions[(*port2)] = z;
	}

	ports_to_connect_.clear();
}

/**
 * Returns the component set of this DEVS element
 *
 * @return The component set
 */
std::list<std::shared_ptr<BaseDevs> > CoupledDevs::GetComponentSet() {
	return component_set_;
}

/**
 * Adds component to our current set
 *
 * @param component	The component to add
 */
void CoupledDevs::AddComponent(std::weak_ptr<BaseDevs> component) {
	component_set_.push_back(component.lock());
}

std::shared_ptr<CoupledDevs> CoupledDevs::load(std::string path) {
	return std::dynamic_pointer_cast<CoupledDevs>( BaseDevs::load(path) );
}

/**
 * Perform direct connection on a list of components
 *
 * @param component_set the list of components to direct connect
 * @return the direct connected component_set
 */
std::list<std::shared_ptr<BaseDevs> > CoupledDevs::DirectConnect(
		std::list<std::shared_ptr<BaseDevs> > componentset) {
	std::list<std::shared_ptr<BaseDevs> > new_list;
	for (auto model : componentset) {
		std::shared_ptr<CoupledDevs> coupled = std::dynamic_pointer_cast<CoupledDevs>(model);
		if (coupled)
			componentset.merge(coupled->GetComponentSet());
		else
			new_list.push_back(model);
	}
	componentset = new_list;

	for (auto model : componentset) {
		for (auto& outp : model->getOutputPorts()) {
			outp->routing_outline_.clear();

			std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > > worklist;

			for (auto& p : outp->outline_) {
				worklist[p] = outp->z_functions[p];
			}

			for (auto& entry : worklist) {
				auto outline = entry.first.lock();

				std::function<Event(Event)>& z = entry.second;

				if (std::dynamic_pointer_cast<CoupledDevs>(outline->GetHostDEVS().lock())) {
					for (auto& inl : outline->outline_) {
						worklist.insert(
								std::pair<std::weak_ptr<Port>,
										std::function<Event(Event)> >(
										inl,
										AppendZ(z,
												outline->z_functions.at(inl))));
						if (!std::dynamic_pointer_cast<CoupledDevs>(inl.lock()->GetHostDEVS().lock())) {
							outp->routing_outline_.insert(
									std::pair<std::weak_ptr<Port>,
											std::function<
													Event(Event)> >(
											inl,
											AppendZ(z,
													outline->z_functions.at(
															inl))));
						}
					}
				} else {
					for (auto& entry2 : outp->routing_outline_) {
						if (entry2.first.lock() == outline) {
							z = entry2.second;
							break;
						}
					}
					outp->routing_outline_.insert(
							std::pair<std::weak_ptr<Port>,
									std::function<Event(Event)> >(
									outline, z));
				}
			}
		}
		for (auto& inp : model->getInputPorts()) {
			inp->routing_inline_.clear();

			for (auto& inline_port : inp->inline_) {
				auto inline_port_sp = inline_port.lock();
				if (std::dynamic_pointer_cast<CoupledDevs>(inline_port_sp->GetHostDEVS().lock())) {
					for (auto& outline : inline_port_sp->inline_) {
						inp->inline_.push_back(outline);

						if (std::dynamic_pointer_cast<CoupledDevs>(outline.lock()->GetHostDEVS().lock())) {
							inp->routing_inline_[outline] = std::function<
									Event(Event)>();
						}

					}
				} else if (inp->routing_inline_.find(inline_port)
						!= inp->routing_inline_.end()) {
					inp->routing_inline_[inline_port] = std::function<
							Event(Event)>();
				}
			}
		}
	}

	return componentset;
}

std::list<std::shared_ptr<BaseDevs> > CoupledDevs::DirectConnect() {
	return CoupledDevs::DirectConnect(component_set_);
}

/**
 * Converts component set to atomic devs
 *
 * @param the component set to convert to atomic devs
 * @return The list of atomicDevs
 */
std::list<std::shared_ptr<AtomicDevs> > CoupledDevs::ComponentSetToAtomic(
		std::list<std::shared_ptr<BaseDevs> > component_set) {
	std::list<std::shared_ptr<AtomicDevs> > total_component_set;

	for (auto& component : component_set) {
		if (component->isCoupled()) {
			// recurse if it is coupled
			// std::shared_ptr<CoupledDevs> coupled_component = (std::shared_ptr<CoupledDevs>) component;
			std::shared_ptr<CoupledDevs> coupled_component = std::dynamic_pointer_cast<CoupledDevs>(component );

			std::list<std::shared_ptr<BaseDevs> > current_components =
					coupled_component->GetComponentSet();
			std::list<std::shared_ptr<AtomicDevs> > atoms = ComponentSetToAtomic(
					current_components);

			for (auto& devs : atoms) {
				// add atoms
				total_component_set.push_back(devs);
			}
		} else {
			// if it's not coupled, we can just add it
			total_component_set.push_back(std::dynamic_pointer_cast<AtomicDevs>(component));
		}
	}

	return total_component_set;
}

CoupledDevs::~CoupledDevs() {
}

/**
 * Sets the component set to the given set
 *
 * @param component_set The component set to set to
 */
void CoupledDevs::SetComponentSet(std::list<std::shared_ptr<BaseDevs> > component_set) {
	component_set_ = component_set;
}

std::function<Event(Event)> AppendZ(
		std::function<Event(Event)> first_z,
		std::function<Event(Event)> new_z) {
	auto z = [&first_z, &new_z] (Event e) {return new_z(first_z(e));};
	return std::function < Event(Event) > (z);
}

} /* namespace ns_DEVS */
