/*
 * ClassicDEVSWrapper.h
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#ifndef CLASSICDEVSWRAPPER_H_
#define CLASSICDEVSWRAPPER_H_

#include "AtomicDevs.h"
#include "Port.h"
#include <string>
#include <map>

namespace cdevs {

/**
 * \brief A wrapper for AtomicDEVS models that are to be interpreted as Classic DEVS models
 *
 */
class ClassicDEVSWrapper
{
public:
	ClassicDEVSWrapper(std::shared_ptr<AtomicDevs> model);

	void SetAttributes(std::string attribute, int value);
	int GetAttributes(std::string attribute);

	void ExtTransition(std::map<int, int> input);
	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > OutputFunc();

	virtual ~ClassicDEVSWrapper();

private:
	std::shared_ptr<AtomicDevs> model_;
};

} /* namespace ns_DEVS */

#endif /* CLASSICDEVSWRAPPER_H_ */
