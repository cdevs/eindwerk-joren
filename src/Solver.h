#ifndef SOLVER_H_
#define SOLVER_H_

#include "AtomicDevs.h"
#include "RootDEVS.h"
#include "utility.h"
#include "ClassicDEVSWrapper.h"
#include "Message.h"
#include "Color.h"
#include <map>
#include <cfloat>
#include <array>
#include <condition_variable>
#include <mutex>
#include <list>
#include "Tracers.h"
#include "exceptions/RuntimeDevsException.h"

namespace cdevs {

class Controller;
/**
 * A unified DEVS solver, containing all necessary functions
 */
class Solver
{
public:
	Solver(Controller * controller, unsigned id);
	virtual ~Solver();

	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > AtomicOutputGenerationEventTracing(std::shared_ptr<AtomicDevs> adevs, DevsTime time);
	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > AtomicOutputGeneration(std::shared_ptr<AtomicDevs> adevs, DevsTime time);
	void MassAtomicTransitions(std::map<int, int> trans, DevsTime clock);
	void AtomicInitialize(std::shared_ptr<AtomicDevs> adevs, DevsTime at_time);

	std::list<std::shared_ptr<AtomicDevs> > CoupledOutputGenerationClassic(DevsTime at_time);
	std::list<std::shared_ptr<AtomicDevs> > CoupledOutputGeneration(DevsTime at_time);
	void CoupledInitialize();
	void NotifySend(int destination, DevsTime timestamp, Color color);
	void NotifyReceive(Color color);

	void Send(std::shared_ptr<AtomicDevs> adevs, DevsTime time, std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > content);

	void PerformDsDevs(std::vector<std::shared_ptr<AtomicDevs> >);

	std::shared_ptr<AtomicDevs> FindDevsModelById(int id);

	void RegisterTracer(std::shared_ptr<Tracer> tracer, bool recover);

	void SetClassicDevs(bool classic_devs);

	unsigned getId();
protected:
	Controller * controller_;

	unsigned id_; // name of the base simulator
	std::map<int, int> transitioning_;

	static std::mutex temp_mutex;
	int msg_copy_;

	bool has_initial_allocator_;
	bool do_some_tracing_;
	bool is_simulation_irreversible_;
	bool temporarily_irreversible_;

	/////////////////////////
	// Checkpointing
	/////////////////////////
	bool checkpoint_restored_;

	bool use_classic_devs_;

	DevsTime block_outgoing_;

	Tracers tracers_;

	std::shared_ptr<RootDevs> model_;
	std::list<std::shared_ptr<AtomicDevs> > models_;

	Color color_;

	//////////////////////////
	// COUNTERS
	/////////////////////////
	int message_sent_count_;
	int message_received_count_;

	//////////////////////////
	// MESSAGES
	//////////////////////////
	std::deque<Message *> input_message_queue_;
	std::deque<Message *> output_message_queue_;

	//////////////////////////
	// GVT
	//////////////////////////
	/**
	 * Mattern's GVT algorithm
	 */
	std::map<Color, std::map<unsigned, unsigned> > colorVectors_;

	std::array<std::mutex, 4> vchange_mutex_;
	std::array<std::unique_lock<std::mutex>, 4> vchange_lock_;
	std::array<std::condition_variable, 4> vchange_cv_;

	double tmin_;
};

bool wayToSort(std::shared_ptr<BaseDevs> one, std::shared_ptr<BaseDevs> two);

} /* namespace ns_DEVS */

#endif /* SOLVER_H_ */
