#include "ClassicDEVSWrapper.h"

namespace cdevs
{
/**
 * Constructor
 *
 * The model to wrap around
 */
ClassicDEVSWrapper::ClassicDEVSWrapper(std::shared_ptr<AtomicDevs> model)
{
	model_ = model;
}

void ClassicDEVSWrapper::SetAttributes(std::string attribute, int value)
{
}

int ClassicDEVSWrapper::GetAttributes(std::string attribute)
{
}

void ClassicDEVSWrapper::ExtTransition(std::map<int, int> input)
{
}

/**
 * Wrap around the outputFnc function by changing the returned dictionary
 *
 * @return the changed dictionary
 */
std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > ClassicDEVSWrapper::OutputFunc()
{
	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > retval = model_->OutputFunction();

	//TODO change type of AtomicDevs outputfunc??
	return retval;
}

ClassicDEVSWrapper::~ClassicDEVSWrapper() {
}
}
