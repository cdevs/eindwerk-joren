#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <cfloat>
#include <thread>

#include "Color.h"
#include "Controller.h"
#include "allocators/Allocator.h"
#include "tracers/Tracer.h"
#include "exceptions/RuntimeDevsException.h"
#include "tracers/TracerVerbose.h"
#include "tracers/TracerXML.h"
//#include "tracers/TracerJson.h"

namespace cdevs {

class Simulator
{
public:
	Simulator(std::shared_ptr<BaseDevs> model, unsigned n_kernels = 1);
	void GetAttribute();

	void RunStartup();

	void DrawModelHierarchy(std::string out_filename, std::shared_ptr<BaseDevs> model);
	void DrawModelConnections(std::string out_filename, BaseDevs& model, std::vector<Color> colors);

	void Checkpoint();
	void LoadCheckpoint();
	void LoadCheckpoint(std::string name);

	void StartAllocator();

	void Reinitialize();

	void ModifyState(int model_id, StateBase* state);
	void ModifyStateAttribute(int model_id, std::string attribute, int value);

	void setClassicDEVS();

	void setTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> condition);
	void setTerminationTime(double time);

	void Simulate();
	void RealSimulate();

	void RemoveTracers();
	void ShowProgress();

	virtual ~Simulator();

	void SetTracer(Tracer* tracer);
	void setVerbose(std::string filename = "");
	void setXml(std::string filename = "");
//	void setJson(std::string filename="", unsigned indent=3);

private:
	Allocator * allocator_;
	Controller * controller_;
	std::shared_ptr<BaseDevs> model_;
	std::map<int, std::shared_ptr<BaseDevs>> model_ids_;

	std::vector<int> termination_models_;
	std::vector<std::shared_ptr<Tracer>> tracers_;

	bool has_activity_tracking_;
	bool has_location_tracer_;
	bool is_in_progress_;
	bool progress_finished_;
	bool is_coupled_devs_;
	bool draw_model_hierarchy_;

	bool is_flattened_;
	bool setup_;

	int gvt_interval_;
	double termination_time_;

	std::string checkpoint_name_;
	int checkpoint_interval_;

	unsigned n_kernels_;

	std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition_;
};

} /* namespace ns_DEVS */

#endif /* SIMULATOR_H_ */
