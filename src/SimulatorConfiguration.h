/*
 * SimulatorConfiguration.h
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#ifndef SIMULATORCONFIGURATION_H_
#define SIMULATORCONFIGURATION_H_

#include <string>
namespace cdevs {

class SimulatorConfiguration
{
public:
	SimulatorConfiguration();

	void SetClassicDevs(bool classicdevs);

	void SetMemo(bool memo);

	void SetDsDevs(bool dsdevs);

	void SetAllowLocalReinit(bool allowed);
	void SetManualRelocator();

	void SetRelocationDirective(double time);
	void SetRelocationDirectives();

	void SetSchedulerCustom();
	void SetSchedulerActivityHeap(bool locations);
	void SetSchedulePoly(bool locations);
	void SetSchedulerDirtyheap(bool locations);
	void SetSchedulerDiscreteTime(bool locations);
	void SetScheduleSortedList(bool locations);
	void SetSchedulerMinimalList(bool locations);
	void SetSchedulerNoAge(bool locations);
	void SetSchedulerHeapSet(bool locations);

	void SetShowProgress(bool locations);

	void SetTerminationModel(bool model);

	void RegisterState();
	void SetDrawModel();
	void SetFetchAllAfterSimulation(bool fetch);
	void SetActivityTrackingVisual();
	void SetLocationCellMap();
	void SetTerminationCondition(bool (&f)(void));
	void SetTerminationTime(double time);

	void SetVerbose(std::string fname);
	void SetRemoveTracers();
	void SetCell(double x, double y, std::string filename);

	void SetXml(std::string filename);

	void SetVcd(std::string filename);

	void SetCustomTracer();
	void SetLogging();

	void SetGvtInterval(double gvt_int);
	void SetCheckpointing(std::string name, int checkpoint_interval);

	void SetStateSaving();
	void SetMessageCopy(std::string method);

	void SetRealtime(bool realtime, double scale);
	void SetRealtimeInputFile(std::string file);
	void SetRealtimePlatformThreads();

	void SetModelState();
	void SetModelStateAttributes();
	void SetModelAttribute();

	void SetActivityRelocatorCustom();
	void SetAcitvityRelocatorBasicBound();

	void SetGreedyAlloc();
	void SetAutoAlloc();
	void SetInitialAlloc();

	virtual ~SimulatorConfiguration();
};

} /* namespace ns_DEVS */

#endif /* SIMULATORCONFIGURATION_H_ */
