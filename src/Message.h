#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "utility.h"
#include "Port.h"

namespace cdevs{
class Message
{
public:
	Message(DevsTime timestamp, int destination, std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > content);
	const std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > >& getContent() const { return content_; }
	DevsTime getTimeStamp() const { return timestamp_; }
	int getDestination() const { return destination_; }
	bool operator<(const Message& right) const {
		return timestamp_ < right.getTimeStamp();
	}

	bool operator>(const Message& right) const {
		return timestamp_ > right.getTimeStamp();
	}
private:
	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > content_;
	DevsTime timestamp_;
	int destination_;
};

class GvtControlMessage
{
public:
	double x; // TODO ??????
	double y; // TODO ??????
	std::map<unsigned, int> z; // TODO ???????
	std::map<unsigned, int> q; // TODO ???????
};

inline Message::Message(DevsTime timestamp, int destination, std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > content)
{
	timestamp_ = timestamp;
	destination_ = destination;
	content_ = content;
}

}

#endif
