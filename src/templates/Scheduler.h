/*
 * Scheduler.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "../AtomicDevs.h"
#include <list>
#include "../utility.h"

namespace cdevs {

enum SchedulerType
{
	kAutoSchedulerType = 0,
	kAhSchedulerType,
	kDtSchedulerType,
	kHsSchedulerType,
	kMlSchedulerType,
	kNaSchedulerType,
	kSlSchedulerType
};

class Scheduler
{

public:
	/**
	 * TODO: delete if needed
	 */
	//Scheduler(void models, void epsilon, void totalModels);

	/**
	 * Destructor for the Scheduler class.
	 */
	//virtual ~Scheduler() = 0;

	/**
	 * Schedule a new model, that was NOT present in the scheduler before
	 * @param model the model to schedule
	 */
	virtual void Schedule(std::shared_ptr<AtomicDevs> model) = 0;

	/**
	 * Unschedule a model, so remove it from the scheduler for good
	 * @param model model to unschedule
	 */
	virtual void Unschedule(std::shared_ptr<AtomicDevs> model) = 0;

	/**
	 * Reschedule all models provided, all of them should already be scheduled
	 * previously and all should still be left in the scheduler after the rescheduling.
	 * @param reschedule_set iterable containing all models to reschedule
	 */
	virtual void MassReschedule(std::list<std::shared_ptr<AtomicDevs> > reschedule_set) = 0;

	/**
	 * Returns the time of the first model that has to transition
	 * @return Timestamp of the first model
	 */
	virtual DevsTime ReadFirst() = 0;

	/**
	 * Returns an iterable of all models that transition at the provided time,
	 * with the epsilon deviation (from the constructor) allowed. For efficiency,
	 * this method should only check the **first** elements, so trying to invoke
	 * this function with a timestamp higher than the value provided with the
	 * *readFirst* method, will **always** return an empty iterable.
	 * @param time timestamp to check for models
	 * @return Iterable containing all models for that time
	 */
	virtual std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time) = 0; // TODO: implement timestamp for 'time'
};

} /* namespace ns_DEVS */

#endif /* SCHEDULER_H_ */
