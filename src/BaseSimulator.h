#ifndef BASESIMULATOR_H_
#define BASESIMULATOR_H_

#include "templates/Scheduler.h"
#include "AtomicDevs.h"
#include "MessageScheduler.h"
#include "StateSaver.h"
#include "utility.h"
#include "Message.h"
#include "Tracers.h"
#include "StateSaver.h"

#include <array>
#include <ctime>
#include <chrono>
#include <map>
#include <list>
#include <functional>
#include <exception>
#include <queue>
#include <cfloat>
#include <cmath>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>
#include <limits>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "CoupledDEVS.h"
#include "RootDEVS.h"
#include "Solver.h"
#include "tracers/TracerAction.h"

#define DEF_MAX_LOOPS 1000
#define DEF_EPSILON 1E-6

namespace cdevs {
class CoupledDevs;
} /* namespace cdevs */

namespace cdevs {

class Controller;

class BaseSimulator: public Solver
{
public:
	BaseSimulator(unsigned id, std::shared_ptr<RootDevs> model, Controller * controller);

	void ResetSimulation(Scheduler * scheduler);

	void SetState(std::map<std::string, std::string> retdict);
	StateBase* GetState(int model_id);

	void Initialize();

	void SetTerminationTime(DevsTime at_time);
	void SetTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition);

	void setGlobals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
		StateSaver * statesaver, unsigned nKernels, std::vector<std::shared_ptr<Tracer> > tracers);

	void PrepareModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models, bool is_flattened);
	void SendModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models, SchedulerType scheduler_type,
	        bool is_flattened);

	//GVT
	void SetGvt(double gvt, std::vector<int> activities, bool last_state_only);
	double GetGvt();

	void Revert(DevsTime at_time);

	void Receive(Message * message);

	void ProcessIncomingMessages();
	void ReceiveAntimessages(DevsTime min_time, std::shared_ptr<AtomicDevs> model, std::list<Message*> messages, Color color);

	bool Check();
	void FinishAtTime(DevsTime at_time1);

	void MassDelayedActions(DevsTime at_time, const std::vector<TracerAction>& messages);
	void DelayedAction(Tracer * tracer, DevsTime at_time, std::shared_ptr<AtomicDevs> model, std::string text);
	void RemoveActions(std::vector<std::shared_ptr<AtomicDevs>> model_ids, DevsTime time);
	void PerformActions(double gvt = std::numeric_limits<double>::infinity());

	void RemoveTracers();

	DevsTime ProcessMessage(DevsTime clock);

	void Checkpoint();
	void LoadCheckpoint();

	void SimulateSync();
	void Simulate();
	void RunSimulation();

	void SetAttributes(int model_id, std::string attribute, int value);
	void SetStateAttributes(int model_id, std::string attribute, int value);

	void StartTracers(bool recover);
	void StopTracers();

	double GetTime();
	StateBase const& GetStateAtTime(int model_id, DevsTime request_time);

	bool WaitUntilOK(unsigned vector);
	void ReceiveControl(GvtControlMessage message, bool first = false);

	std::string GenerateUuid();

	void SetActivityTracking(bool activity_tracking);
	float GetActivity(int model_id);
	std::map<int, float> GetCompleteActivity();
	std::map<int, float> GetTotalActivity();
	std::map<int, float> GetTotalActivity(DevsTime at_time);

	virtual ~BaseSimulator();

	bool isFlattened() const
	{
		return is_flattened_;
	}

	void setIsFlattened(bool isFlattened)
	{
		is_flattened_ = isFlattened;
	}

private:
	//////////////////////////
	// STRINGS
	//////////////////////////
	std::string checkpoint_name_; // checkpoint names

	StateSaver * statesaver_;
	unsigned nKernels_;
	BaseSimulator * next_LP_;
	std::vector<BaseSimulator *> destinations_;

	//////////////////////////
	// MESSAGES
	//////////////////////////
	MessageScheduler message_input_scheduler_;

	GvtControlMessage control_msg_;

	//////////////////////////
	// TRANSITIONING
	//////////////////////////

	//////////////////////////
	// FLAGS
	/////////////////////////
	// model
	bool is_flattened_; // is the total model flattened

	// simulation
	bool has_simulation_finished_; // done?

	bool has_prevtime_finished_;

	bool is_finish_sent_;

	bool should_memorize_;

	bool has_simulation_reset_;

	bool has_activity_tracking_;

	bool has_termination_condition_; // Set this when changing termination_condition

	//////////////////////////
	// TIME
	//////////////////////////
	DevsTime termination_time_; // when should we stop?

	double gvt_; // the gvt to roll to
	double old_gvt_;

	DevsTime prev_time_;
	DevsTime clock_;
	DevsTime current_clock_;

	//////////////////////////
	// COUNTERS
	/////////////////////////
	int checkpoint_counter_;
	unsigned checkpoint_frequency_;

	int reverts_;

	/////////////////////////
	// TRACERS
	/////////////////////////
	std::map<int, float> activities_;
	std::map<int, float> total_activities_;

	std::vector<TracerAction> actions_;

	unsigned loglevel_;

	//////////////////////////
	// MUTEX LOCKS
	//////////////////////////
	bool simlock_request_;

	static std::mutex simlock_;

	std::mutex should_run_;

	std::mutex sim_finish_lock_;

	std::mutex vlock_;

	std::mutex actionmutex;

protected:
	//////////////////////////
	// MODELS
	//////////////////////////
	std::shared_ptr<BaseDevs> total_model_;

	/////////////////////////
	// CONDITION
	/////////////////////////
	std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition_;
	bool termination_time_check_;

	bool is_classic_devs_;

	//////////////////////////
	// GVT
	//////////////////////////
	std::map<unsigned, int> accumulator_;
};

} /* namespace ns_DEVS */

#endif /* BASESIMULATOR_H_ */
