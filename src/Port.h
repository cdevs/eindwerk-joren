/*
 * Port.h
 *
 *  Created on: 12-mrt.-2015
 *      Author: Ian
 */

#ifndef PORT_H_
#define PORT_H_

#include "utility.h"
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace cdevs {

// Forward class declaration
class BaseDevs;

class Port
{
	friend class Solver;
public:
	Port(bool is_input=false, std::string="");
	const std::string GetPortName() const;
	const std::string GetFullPortName() const;
	inline bool IsInput() const { return is_inport_; };
	void SetHostDEVS(std::weak_ptr<BaseDevs> model);
	void ResetHostDEVS();
	inline std::weak_ptr<BaseDevs> GetHostDEVS() const { return host_DEVS; };
	~Port();
	//TODO: only compare on name?
	bool operator<(const Port& b)const{return (name_.compare(b.name_) < 0); }
	bool operator==(const Port& b)const{return (name_.compare(b.name_) == 0) && (is_inport_ == b.is_inport_);}

	unsigned getMsgCount() const {
		return msg_count_;
	}

	void setMsgCount(unsigned msgCount) {
		msg_count_ = msgCount;
	}

public: //TODO: make private and add helper methods
	std::vector<std::weak_ptr<Port>> inline_;
	std::vector<std::weak_ptr<Port>> outline_;

	std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > > routing_inline_;
	std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > > routing_outline_;
	std::weak_ptr<BaseDevs> host_DEVS;
	unsigned msg_count_;

	std::string name_;
	bool is_inport_;
	std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > > z_functions;

};

} /* namespace ns_DEVS */

#endif /* PORT_H_ */
