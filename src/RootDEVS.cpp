#include "RootDEVS.h"
#include "CoupledDEVS.h"
#include "AtomicDevs.h"
#include "schedulers/SchedulerML.h"
#include <iostream>

namespace cdevs
{
/**
 * Basic constructor.
 *
 * @param components: the atomic DEVS models that are the chsildren, only those that are ran locally should be mentioned
 * @param models: all models that have to be passed to the scheduler
 * @param scheduler_type: type of scheduler to use (string representation)
 */
RootDevs::RootDevs(std::list<std::shared_ptr<AtomicDevs> > components, std::list<std::shared_ptr<BaseDevs> > models,
        SchedulerType scheduler_type)
	: BaseDevs("ROOT model")
{
	components_ = components;
	models_ = models;
	scheduler_type_ = scheduler_type;
	//TODO create right scheduler here
//	scheduler_ = new SchedulerML(models_, std::numeric_limits<double>::epsilon(), models_.size());
	// scheduler_ = new SchedulerML(components_, 0.00001, models_.size());

	// TODO: local_model_ids_ is unused! (david)
	/*for (std::shared_ptr<AtomicDevs> component : components_) {
		local_model_ids_.push_back(component->GetModelID());
	}*/
	direct_connected_ = true;
}

RootDevs::~RootDevs()
{

}

void RootDevs::UndoDirectConnect()
{
	direct_connected_ = false;
}

std::list<std::shared_ptr<BaseDevs> > RootDevs::DirectConnect()
{
	if (!direct_connected_) {
		models_ = CoupledDevs::DirectConnect(models_);
		direct_connected_ = true;
	}

	return models_;
}

void RootDevs::SetScheduler(SchedulerType scheduler_type)
{
	scheduler_type_ = scheduler_type;
	switch (scheduler_type) {
	default:
	case SchedulerType::kMlSchedulerType:
		scheduler_ = std::make_shared<SchedulerML>(components_, 0.00001, models_.size()); // TODO: correct parameters
		break;
		/*
		 case SchedulerType::kAhSchedulerType:
		 scheduler_ = new SchedulerAH();
		 break;
		 case SchedulerType::kAutoSchedulerType:
		 scheduler_ = new SchedulerAuto();
		 break;
		 case SchedulerType::kDtSchedulerType:
		 scheduler_ = new SchedulerDT();
		 break;
		 case SchedulerType::kHsSchedulerType:
		 scheduler_ = new SchedulerHS();
		 break;
		 case SchedulerType::kNaSchedulerType:
		 scheduler_ = new SchedulerNA();
		 break;
		 case SchedulerType::kSlSchedulerType:
		 scheduler_ = new SchedulerSL();
		 break;
		 */
	}
}

void RootDevs::SetGvt(double gvt, std::vector<int> activities, bool last_state_only)
{
	for (auto& component : components_) {
		component->SetGvt(gvt, last_state_only);
	}
}

void RootDevs::FetchActivity(DevsTime at_time, std::map<int, float>& activities)
{
}

bool RootDevs::Revert(DevsTime at_time, bool memorize)
{
	std::list<std::shared_ptr<AtomicDevs> > reschedules;
	bool controller_revert = false;
	for (auto& component : components_) {
		if (!(component->getTimeLast() < at_time)) {
			controller_revert |= component->Revert(at_time, memorize);
			reschedules.push_back(component);
		}
		component->my_input_.clear();
	}
	scheduler_->MassReschedule(reschedules);
	SetTimeNext();
	return controller_revert;
}

void RootDevs::SetTimeNext()
{
	time_next_ = scheduler_->ReadFirst();
}

/**
 * Returns the component set of this DEVS element
 *
 * @return The component set
 */
std::list<std::shared_ptr<AtomicDevs> > RootDevs::GetComponentSet()
{
	return components_;
}

void RootDevs::FlattenConnections()
{

}

void RootDevs::UnflattenConnections()
{

}

void RootDevs::SetLocation(int location, bool force)
{
}

int RootDevs::GetModelLoad(std::map<int, double> loads)
{
}

int RootDevs::Finalize(std::string name, int model_counter,
	std::map<int, std::shared_ptr<BaseDevs>>& model_ids, std::list<std::shared_ptr<BaseDevs>> select_hierarchy)
{
}

/**
 * Adds component to our current set
 *
 * @param component	The component to add
 */
void RootDevs::AddComponent(std::shared_ptr<AtomicDevs> component)
{
	components_.push_back(component);
}

Scheduler * RootDevs::GetScheduler()
{
	return scheduler_.get();
}

/**
 * \brief Gets the scheduler type
 *
 * This functions returns what type of scheduler this RootDEVS is using
 * It can be:
 * 	- Activity Heap
 * 	- Automatic
 * 	- Heapset
 * 	- Minimal List
 * 	- No Age
 * 	- Sorted List
 *
 * @return The type of scheduler
 */
SchedulerType RootDevs::getSchedulerType()
{
	return scheduler_type_;
}
} /* namespace cdevs */
