#include "Allocator.h"
#include <vector>

namespace cdevs
{
/**
 * Allocate all models in a static manner, simply trying to divide the number of models equally.
 * Our 'heuristic' is to allocate in chunks as defined in the root coupled model.
 */


class AutoAllocator : public Allocator {
public:
	AutoAllocator();

	std::map<int, int> Allocate(std::list<std::shared_ptr<BaseDevs>> models, Graph edges,int nr_nodes, std::map<int, float> total_activities);
	int GetTerminationTime();

	virtual ~AutoAllocator();
};
}
