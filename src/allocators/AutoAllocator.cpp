#include "AutoAllocator.h"

namespace cdevs
{
/**
 * Calculate allocations for the nodes, using the information provided.
 *
 * @param models: the models to allocte
 * @param edges: the edges between the models
 * @param nr_nodes: the number of nodes to allocate over. Simply an upper bound!
 * @param total_activities: activity tracking information from each model
 * @return allocation that was found
*/
std::map<int, int> AutoAllocator::Allocate(std::list<std::shared_ptr<BaseDevs>> models, Graph edges, int nr_nodes, std::map<int, float> total_activities)
{
	int k = models.size();
	for ( auto &model : models){

	}
}
	/*
	 *         allocation = {}

        allocated_topmost = {}
        current_node = 0

        total_models = len(models)

        for model in models:
            # Not yet allocated, so allocate it somewhere
            child = model
            searchmodel = model
            while searchmodel.parent is not None:
                child = searchmodel
                searchmodel = searchmodel.parent
            # searchmodel is now the root model
            # child is its 1st decendant, on which we will allocate
            try:
                node = allocated_topmost[child]
            except KeyError:
                current_node = (current_node + 1) % nr_nodes
                allocated_topmost[child] = current_node
                node = current_node
            allocation[model.model_id] = node

        return allocation
	 */
AutoAllocator::AutoAllocator() {
}

/**
 * Returns the time it takes for the allocator to make an 'educated guess' of the advised allocation.
 * This time will not be used exactly, but as soon as the GVT passes over it. While this is not exactly
 * necessary, it avoids the overhead of putting such a test in frequently used code.
 *
 * @return float -- the time at which to perform the allocations (and save them)
*/
int AutoAllocator::GetTerminationTime()
{
	return 0;
}

AutoAllocator::~AutoAllocator() {
}
}
