#include "Allocator.h"
#include <vector>

namespace cdevs
{
/**
 *  Allocate all models in a greedy manner: make the most heavy link local and extend from there on until an average load is reached.
 */
class GreedyAllocator : public Allocator {
public:
	GreedyAllocator();

	std::map<int, int> Allocate(std::list<std::shared_ptr<BaseDevs>> models, Graph edges, int nr_nodes, std::map<int, float> total_activities);
	int GetTerminationTime();
	virtual ~GreedyAllocator();
};
}
