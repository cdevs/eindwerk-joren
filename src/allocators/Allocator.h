#ifndef ALLOCATOR_H_
#define ALLOCATOR_H_

#include <vector>
#include <list>
#include "../utility.h"
#include "../AtomicDevs.h"
#include "../Graph.h"

namespace cdevs
{
class Allocator
{
public:
	virtual std::map<int, int> Allocate(std::list<std::shared_ptr<BaseDevs>> models, Graph edges, int nr_nodes, std::map<int, float> total_activities) = 0;
	virtual int GetTerminationTime() =0;
};
}
#endif
