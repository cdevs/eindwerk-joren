#include "GreedyAllocator.h"

namespace cdevs
{
/**
 * Calculate allocations for the nodes, using the information provided.
 *
 * @param models: the models to allocte
 * @param edges: the edges between the models
 * @param nr_nodes: the number of nodes to allocate over. Simply an upper bound!
 * @param total_activities: activity tracking information from each model
 * @return allocation that was found
*/
std::map<int, int> GreedyAllocator::Allocate(std::list<std::shared_ptr<BaseDevs>> models, Graph edges, int nr_nodes, std::map<int, float> total_activities)
{
}

GreedyAllocator::GreedyAllocator() {
}

/**
 * Returns the time it takes for the allocator to make an 'educated guess' of the advised allocation.
 * This time will not be used exactly, but as soon as the GVT passes over it. While this is not exactly
 * necessary, it avoids the overhead of putting such a test in frequently used code.
 *
 * @return float -- the time at which to perform the allocations (and save them)
*/
int GreedyAllocator::GetTerminationTime()
{
	return 0;
}

GreedyAllocator::~GreedyAllocator() {
}
}
