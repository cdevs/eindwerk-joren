/*
 * BaseSimulator.cpp
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#include "BaseSimulator.h"
#include "Controller.h"
#include "exceptions/LogicDevsException.h"
#include "exceptions/RuntimeDevsException.h"
#include <limits>
#include <set>

namespace cdevs
{
std::mutex BaseSimulator::simlock_;
/**
 * Constructor
 */
BaseSimulator::BaseSimulator(unsigned id, std::shared_ptr<RootDevs> model, Controller * controller)
	: Solver(controller, id)
{
	Initialize();

	checkpoint_name_ = "(none)";

	model_ = model;
	total_model_ = 0;

	is_flattened_ = false;

	is_simulation_irreversible_ = true;
	has_simulation_finished_ = false;
	has_prevtime_finished_ = false;

	has_termination_condition_ = false;

	should_memorize_ = false;

	termination_time_check_ = true;
	is_finish_sent_ = false;

	has_activity_tracking_ = false;

	has_simulation_reset_ = false;

	simlock_request_ = false;

	checkpoint_counter_ = 0;
	checkpoint_frequency_ = 5;

	gvt_ = -std::numeric_limits<double>::infinity();
	old_gvt_ = 0.0;

	reverts_ = 0;
}

/**
 * Resets the simulation kernel to the saved version; can only be invoked after a previous simulation run.
 *
 * @param scheduler: the scheduler to set
 */
void BaseSimulator::ResetSimulation(Scheduler * scheduler)
{
	Initialize();
	transitioning_.clear();
	tracers_.Reset();
	is_simulation_irreversible_ = false;
	checkpoint_restored_ = false;
	has_simulation_reset_ = true;
}

/**
 * For pickling
 * @param retdict: dictionary containing the attributes to set
 */
void BaseSimulator::SetState(std::map<std::string, std::string> retdict)
{

}

/**
 *
 */
StateBase* BaseSimulator::GetState(int model_id)
{
	//return State();
	//TODO state?
}

/**
 * Initialise the simulation kernel, this is split up from the constructor to
 * make it possible to reset the kernel without reconstructing the kernel.
 */
void BaseSimulator::Initialize()
{
	models_.clear();

	termination_time_ = DevsTime();
	termination_condition_ = [](DevsTime time, std::shared_ptr<BaseDevs> model) -> bool {return true;};

	has_prevtime_finished_ = false;

	gvt_ = -std::numeric_limits<double>::infinity();
	tmin_ = std::numeric_limits<double>::infinity();
	prev_time_.setX(0);
	prev_time_.setY(0);
	clock_.setX(0);
	clock_.setY(0);

	input_message_queue_.clear();
	output_message_queue_.clear();

	message_input_scheduler_ = MessageScheduler();

	//vchange_mutex_ = std::array<std::mutex, 4>(4, *(new std::mutex()));
	vchange_lock_ = {std::unique_lock<std::mutex>(vchange_mutex_[0], std::defer_lock),
		std::unique_lock<std::mutex>(vchange_mutex_[1], std::defer_lock),
		std::unique_lock<std::mutex>(vchange_mutex_[2], std::defer_lock),
		std::unique_lock<std::mutex>(vchange_mutex_[3], std::defer_lock)};

	//vchange_cv_ = std::vector<std::condition_variable>(4, std::condition_variable());

	colorVectors_ = { {}, {}, {}, {}};
}

/**
 * Configure all 'global' variables for this kernel.
 *
 * @param loglevel Level of logging library.
 * @param checkpoint_frequency Frequency at which checkpoints should be made.
 * @param checkpoint_name Name of the checkpoint to save.
 * @param statesaver The instance of StateSaver to use.
 * @param kernels Number of simulation kernels in total.
 */
void BaseSimulator::setGlobals(unsigned loglevel, unsigned checkpoint_frequency, std::string checkpoint_name,
        StateSaver * statesaver, unsigned nKernels, std::vector<std::shared_ptr<Tracer> > tracers)
{
	for (auto& tracer : tracers)
		tracers_.RegisterTracer(tracer, checkpoint_restored_);

	// tracers_.StartTracers(false);

	loglevel_ = loglevel;
	nKernels_ = nKernels;
	next_LP_ = controller_->getKernel((id_ + 1) % nKernels);
	is_simulation_irreversible_ = (nKernels == 1);
	temporarily_irreversible_ = is_simulation_irreversible_;
	// TODO: state saving
	// TODO: set logger
	checkpoint_name_ = checkpoint_name;
	checkpoint_frequency_ = checkpoint_frequency;
	checkpoint_counter_ = 0;
}

/**
 * Sets the time at which simulation should stop, setting this will override the local simulation condition.
 *
 * @param time: the time at which the simulation should stop
 */
void BaseSimulator::SetTerminationTime(DevsTime at_time)
{
	termination_time_ = at_time;

	//TODO: mutex
}

/**
 * Sets the termination condition of this simulation kernel.
 * As soon as the condition is valid, it will signal all nodes that they have to stop simulation as soon as they have progressed up to this simulation time.
 * @param termination_condition: a function that accepts two parameters: *time* and *model*. Function returns whether or not to halt simulation
 */
void BaseSimulator::SetTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> termination_condition)
{
	termination_condition_ = termination_condition;
	termination_time_check_ = false;
	has_termination_condition_ = true;
}

/**
 * Prepare a model to send
 *
 * @param model: the model to send
 * @param model_ids: list contain all models
 * @param is_flattened: whether or not the model had its ports decoupled from the models to allow pickling
 */
void BaseSimulator::PrepareModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models, bool is_flattened)
{
	is_flattened_ = is_flattened;

	if (is_flattened)
		model->UnflattenConnections();

	total_model_ = model;
	models_ = models;
}

/**
 * Send a model to this simulation kernel, as this one will be simulated
 *
 * @param model: the model to set
 * @param models: list containing all models
 * @param scheduler_type: string representation of the scheduler to use
 * @param flattened: whether or not the model had its ports decoupled from the models to allow pickling
 */
void BaseSimulator::SendModel(std::shared_ptr<BaseDevs> model, std::list<std::shared_ptr<AtomicDevs>> models, SchedulerType scheduler_type,
        bool is_flattened)
{

	//prepare the model
	PrepareModel(model, models, is_flattened);

	/*std::vector<BaseSimulator *> destinations_;
	destinations_ = std::vector<BaseSimulator *>(models_.size());*/

	std::list<std::shared_ptr<AtomicDevs>> local;
	for (auto& a_devs : models) {
		if (a_devs->GetLocation() == id_)
			local.push_back(a_devs);
	}

	std::list<std::shared_ptr<BaseDevs>> base_models;
	std::shared_ptr<CoupledDevs> coupled = std::dynamic_pointer_cast<CoupledDevs>(model);
	if (coupled)
		base_models = coupled->GetComponentSet();
	else
		base_models.push_back(model);

	model_ = std::make_shared<RootDevs>(local, base_models, scheduler_type);

	// Could still be useful to test the auto allocator:
//	for (auto comp : model_->GetComponentSet())
//		std::cout << "\t" << comp->GetModelName() << " : " << comp->GetLocation() << std::endl;
}

/**
 * Sets the GVT of this simulation kernel. This value should not be smaller than
 * the current GVT (this would be impossible for a correct GVT calculation). Also
 * cleans up the input, output and state buffers used due to time-warp.
 * Furthermore, it also processes all messages scheduled before the GVT.
 *
 * @param gvt: the desired GVT
 * @param activities: the activities of all seperate nodes as a list
 * @param last_state_only: whether or not all states should be considered or only the last
 */
void BaseSimulator::SetGvt(double gvt, std::vector<int> activities, bool last_state_only)
{
	if (gvt < gvt_)
		throw 200;
	else if (gvt == gvt_) {
		// TODO call GvtDone() on Controller!
	}

	simlock_request_ = true;

	// acquire lock
	simlock_.lock();

	simlock_request_ = false;
	old_gvt_ = gvt_;
	gvt_ = gvt;

	PerformActions(gvt);

	bool found = false;

	// message_input_scheduler_.CleanUp(gvt, 1);

	for (unsigned i = 0; i < output_message_queue_.size(); i++) {
		if (output_message_queue_.at(i)->getTimeStamp().getX() >= gvt) {
			found = true;
			output_message_queue_.erase(output_message_queue_.begin(), output_message_queue_.begin() + i);
			break;
		}
	}
	if (!found)
		output_message_queue_.clear();

	activities_.clear();
	// TODO model_->SetGvt(gvt, activities_, last_state_only); ?
	for (auto& activity_key_pair : activities_) {
		auto itr = total_activities_.find(activity_key_pair.first);
		if (itr == total_activities_.end())
			total_activities_.insert(activity_key_pair);
		else
			total_activities_[activity_key_pair.first] += activity_key_pair.second;

	}

	if (temporarily_irreversible_) {
		for (std::shared_ptr<AtomicDevs> atomic_model : model_->GetComponentSet()) {
			//TODO activities??? David & Ian
		}
	}

	if (checkpoint_counter_ == checkpoint_frequency_) {
		Checkpoint();
		checkpoint_counter_ = 0;
	} else
		checkpoint_counter_ += 1;

	// release lock
	simlock_.unlock();

	next_LP_->SetGvt(gvt, activities, last_state_only);
}

/**
 * Return the GVT of this kernel
 *
 * @return The current GVT
 */
double BaseSimulator::GetGvt()
{
	return gvt_;
}

/**
 *  Revert the current simulation kernel to the specified time. All messages
 *  sent after this time will be invalidated, all states produced after this
 *  time will be removed.
 *
 *  @param time: the desired time for revertion.
 *
 *  .. note:: Clearly, this time should be >= the current GVT
 */
void BaseSimulator::Revert(DevsTime time)
{
	if (time.getX() < gvt_)
		throw std::exception();

	transitioning_.clear();
	reverts_ += 1;

	if (do_some_tracing_) {
		std::vector<std::shared_ptr<AtomicDevs>> locals;
		for(std::shared_ptr<AtomicDevs> model : models_)
		{
			locals.push_back(model);
		}
		controller_->RemoveActions(locals, time);
	}

	// Also revert the input message scheduler
	message_input_scheduler_.Revert(time);

	// Now revert all local models
	bool controller_revert = model_->Revert(time, should_memorize_);

	prev_time_ = time;
	clock_ = prev_time_;

	// invalidate all output messages after or at time
	auto end = output_message_queue_.end();
	std::map<std::shared_ptr<AtomicDevs>, std::list<Message *> > unschedules;
	std::map<std::shared_ptr<AtomicDevs>, DevsTime> unschedules_min_time;

	for (Message * message : output_message_queue_) {
		if (message->getTimeStamp() > time) {
			int model_id = message->getDestination();
			std::shared_ptr<AtomicDevs> model = FindDevsModelById(model_id);

			// set default min_time
			DevsTime min_time(std::numeric_limits<double>::infinity(), 0);

			// check if key exists, if so, update the min_time
			if(unschedules_min_time.find(model) != unschedules_min_time.end())
				min_time = unschedules_min_time[model];

			// set the min_time for the model equal to the minimum of the min_time and the message timestamp
			if(min_time < message->getTimeStamp())
				unschedules_min_time[model] = min_time;
			else
				unschedules_min_time[model] = message->getTimeStamp();

			// if we can find the key for this model, add it to the unschedules
			unschedules[model].push_back(message);

		}
		else
			//TODO: !!!!! not sure... David? !!!! - Ian
			end = std::find(output_message_queue_.begin(), output_message_queue_.end(), message);
	}

	if(end != output_message_queue_.end())
		output_message_queue_.erase(end + 1, output_message_queue_.end());

	if(output_message_queue_.empty())
		block_outgoing_ = DevsTime(-1, -1);
	else
		block_outgoing_ = output_message_queue_.back()->getTimeStamp();

	for(auto model_messages_pair : unschedules)
	{
		// get the model
		std::shared_ptr<AtomicDevs> model = model_messages_pair.first;

		// get the model's destination
		unsigned destination_kernel = model->GetLocation();

		// revert to relocation itself... This should be impossible
		if(destination_kernel == id_)
			throw RuntimeDevsException("Revert due to relocation to self... This is impossible");

		// get the min time for this model
		DevsTime min_time = unschedules_min_time[model];

		// notify the sending of a message
		NotifySend(destination_kernel, min_time, color_);

		// call the destination kernel to receive antimessages to be unscheduled
		controller_->getKernel(destination_kernel)->ReceiveAntimessages(min_time, model, unschedules[model], color_);

		if(controller_revert)
		{
			NotifySend(controller_->getId(), time, color_);
			controller_->ReceiveAntimessages(time, 0, {}, color_);
		}
		//TODO should_run.set() ?? - Ian
	}
}

/**
 *
 */
void BaseSimulator::Receive(Message * message)
{
	input_message_queue_.push_back(message);
}

/**
 * Process all incomming messages and return.
 * This is part of the main simulation loop instead of being part of the message receive method, as we require the simlock for this. Acquiring the simlock elsewhere might take some time!
 */
void BaseSimulator::ProcessIncomingMessages()
{
	while (!input_message_queue_.empty()) {
		Message * message = input_message_queue_.front();
		input_message_queue_.pop_front();

		//look up destination model
		std::shared_ptr<AtomicDevs> destination_model = FindDevsModelById(message->getDestination());

		if (destination_model == 0)
			throw RuntimeDevsException("BaseSimulator::ProcessIncomingMessages() has a null pointer for a message");

		if (message->getTimeStamp() <= prev_time_)
			Revert(message->getTimeStamp());
		else if (has_prevtime_finished_)
			prev_time_ = message->getTimeStamp();

		has_prevtime_finished_ = false;

		message_input_scheduler_.Schedule(message);

		DevsTime min_time;

		if (model_->getTimeNext() < message->getTimeStamp())
			min_time = model_->getTimeNext();
		else
			min_time = message->getTimeStamp();

		model_->setTimeNext(min_time);
	}
}

/**
 * \brief Process a (possibly huge) batch of anti messages for the same model
 *
 * @param mintime: the lowest timestamp of all messages being cancelled
 * @param model: the receiving model whose messages need to be negated, None to indicate a general rollback
 * @param messages: list of all messages to cancel
 * @param color: color for Mattern's algorithm
 *
 * note:: the *model* is only required to check whether or not the model is still local to us
 */
void BaseSimulator::ReceiveAntimessages(DevsTime min_time, std::shared_ptr<AtomicDevs> model, std::list<Message*> messages, Color color)
{
	// I'm omitting priorcount here, this seems no use for us - Ian

	// acquire simulation lock
	simlock_.lock();

	int destination = model->GetLocation();
	if(destination != id_)
	{
		// the model is not ours, send it the right kernel
		NotifyReceive(color);
		controller_->getKernel(destination)->ReceiveAntimessages(min_time, model, messages, color);
		NotifySend(destination, min_time, color);

		return;
	}

	// timestamp is before previous time, so revert to the time
	if(min_time <= prev_time_)
		Revert(min_time);
	// previous time is irrelevant, simulation has finished
	else if(has_prevtime_finished_)
		prev_time_ = min_time;

	has_prevtime_finished_ = false;
	NotifyReceive(color);

	if(model != 0)
		message_input_scheduler_.MassUnschedule(messages);
}

/**
 * Checks whether or not simulation should still continue. This will either
 * call the global time termination check, or the local state termination
 * check, depending on configuration.
 * Using the global time termination check is a lot FASTER and should be used
 * if possible.
 *
 * @return Whether or not to stop simulation
 */
bool BaseSimulator::Check()
{
	// Max loops check
	if (prev_time_.getY() > DEF_MAX_LOOPS)
		throw std::exception();

	if (termination_time_check_) {
		if (model_->getTimeNext() > termination_time_) {
			Message * message = message_input_scheduler_.ReadFirst();

			if(message == 0)
				return true;

			DevsTime timestamp = message->getTimeStamp();
			return timestamp > termination_time_;

		} else
			return false;
	} else {
		if (prev_time_.getX() == std::numeric_limits<double>::infinity()
		        || termination_condition_(prev_time_, total_model_)) {
			if (!is_finish_sent_) {

				FinishAtTime(prev_time_);
				is_finish_sent_ = true;
			} else if (is_finish_sent_) {
				DevsTime time;
				time.setX(std::numeric_limits<double>::infinity());
				time.setY(std::numeric_limits<double>::infinity());
				FinishAtTime(time);
				is_finish_sent_ = false;
			}
			return false;
		}
	}

	return false;
}

/**
 * Signal this kernel that it may stop at the provided time
 *
 * @param clock: the time to stop at
 */
void BaseSimulator::FinishAtTime(DevsTime at_time)
{
	//TODO [DEADLINE BETA VERSIE] support multiple kernels
	SetTerminationTime(at_time);
}

/**
 * Call the delayedAction function multiple times in succession.
 *
 * Mainly implemented to reduce the number of round trips when tracing.
 *
 * @param time: the time at which the action should happen
 * @param msgs: list containing elements of the form (model_id, action)
 */
void BaseSimulator::MassDelayedActions(DevsTime at_time, const std::vector<TracerAction>& messages)
{
	for (auto action : messages) {
		DelayedAction(action.getTracer(), at_time, action.getModel(), action.getText());
	}
}

/**
 * Perform an irreversible action (I/O, prints, global messages, ...). All
 * these actions will be performed in the order they should be generated
 * in a non-distributed simulation. All these messages might be reverted in
 * case a revertion is performed by the calling model.
 *
 * @param tracer the tracer to send the text to
 * @param time the simulation time at which this command was requested
 * @param model the model that requested this command
 * @param action the actual command to be executed as soon as it is safe
 */
void BaseSimulator::DelayedAction(Tracer * tracer, DevsTime at_time, std::shared_ptr<AtomicDevs> model, std::string text)
{
	if (at_time.getX() < gvt_)
		throw RuntimeDevsException("Can't execute action " + text + " before GVT " + std::to_string(gvt_));

	if (is_simulation_irreversible_ && actions_.size() > 0
	        && at_time > actions_.back().getTime())
		PerformActions();

	// concurrency support
	actionmutex.lock();
	actions_.push_back(TracerAction(at_time, model, text, tracer));
	actionmutex.unlock();

}

/**
 * Remove all actions specified by a model, starting from a specified time.
 * This function should be called when the model is reverted and its actions
 * have to be undone
 *
 * @param model_ids: the model_ids of all reverted models
 * @param time: time up to which to remove all actions
 */
void BaseSimulator::RemoveActions(std::vector<std::shared_ptr<AtomicDevs>> models, DevsTime at_time)
{
	if (at_time.getX() < gvt_)
		throw std::exception();

	actionmutex.lock();

	// make a list of the actions that are still to be executed
	std::vector<TracerAction> current_actions;

	// Actions are unsorted, so we have to go through the complete list
	for (auto& action : actions_) {
		std::shared_ptr<AtomicDevs> model = action.getModel();
		if (std::find(models.begin(), models.end(), model) == models.end()
		        && action.getTime() >= at_time) {
			current_actions.push_back(action);
		}
	}

	actions_ = current_actions;

	actionmutex.unlock();
}

/**
 * Perform all irreversible actions up to the provided time.
 * If time is not specified, all queued actions will be executed (in case simulation is finished)
 *
 * @param gvt: the time up to which all actions should be executed
 */
void BaseSimulator::PerformActions(double gvt)
{
	if (gvt >= termination_time_.getX() && has_termination_condition_)
		gvt = termination_time_.getX() + DEF_EPSILON;

	actionmutex.lock();

	std::vector<TracerAction> lst;
	std::vector<TracerAction> my_remainder;

	if (gvt != std::numeric_limits<double>::infinity()) {
		for (auto& action : actions_) {
			if (action.getTime().getX() < gvt)
				lst.push_back(action);
			else
				my_remainder.push_back(action);
		}
	} else {
		lst = actions_;
		my_remainder.clear();
	}
	actions_ = my_remainder;

	actionmutex.unlock();

	std::sort(lst.begin(), lst.end(),
		[] (const TracerAction& first, const TracerAction& second) { return first.getTime() < second.getTime(); });

	std::set<Tracer *> tracers;

	for (auto& action : lst) {
		action.getTracer()->Trace(action.getModel(), action.getTime(), action.getText());
		tracers.insert(action.getTracer());
	}

	// Flush all tracers now. (Could be that we need to wait with this until no rollback can possibly occur)
	for (auto& tracer: tracers)
		tracer->Flush();
}

/**
 *  Removes all currently registered tracers. This does not clean them up, as this should already be done by the code at the end of the simulation.
 */
void BaseSimulator::RemoveTracers()
{
	tracers_.Reset();
}

/**
 * Find the first external message smaller than the clock and process them if necessary. Return the new time_next for simulation.
 *
 * @param clock timestamp of the next internal transition
 * @return timestamp of the next transition, taking into account external messages
 */
DevsTime BaseSimulator::ProcessMessage(DevsTime clock)
{
	Message * message = message_input_scheduler_.ReadFirst();

	if(message == 0)
		return clock;

	if (message->getTimeStamp() < clock) {
		// The message is sent before the timenext, so update the clock
		clock = message->getTimeStamp();
	}

	while (message != 0 && std::abs(clock.getX() - message->getTimeStamp().getX()) < std::numeric_limits<double>::epsilon()
		&& clock.getY() == message->getTimeStamp().getY()) {
		for (auto port_content_pair : message->getContent()) {
			std::shared_ptr<AtomicDevs> adevs = std::dynamic_pointer_cast<AtomicDevs>( port_content_pair.first.lock()->host_DEVS.lock() );
			auto& input_now = adevs->my_input_[port_content_pair.first];
			input_now.insert(input_now.begin(), port_content_pair.second.begin(), port_content_pair.second.end());
			//TODO something weird happening here...
			transitioning_[adevs->GetModelID()] |= 2;

		}
		message_input_scheduler_.RemoveFirst();
		message = message_input_scheduler_.ReadFirst();
	}

	return clock;
}

/**
 * Save a checkpoint of the current basesimulator, this function will assume
 * that no messages are still left in the medium, since these are obviously
 * not saved by pickling the base simulator.
 */
void BaseSimulator::Checkpoint()
{
	// form file name string
	std::stringstream checkpoint_name_stream;
	checkpoint_name_stream << checkpoint_name_ << "_SIM.pdc";

	// create file stream
	std::ofstream dump_stream(checkpoint_name_stream.str());

	if (is_flattened_) {
		model_->FlattenConnections();
		//TODO: serialize
		model_->UnflattenConnections();
	} else {
		//TODO: serialize
	}

	dump_stream.close();
}

/**
 * Alert this kernel that it is restoring from a checkpoint
 */
void BaseSimulator::LoadCheckpoint()
{
	color_ = Color::kWhite1;
	transitioning_.clear();
	colorVectors_ = { {}, {}, {}, {}};
	tmin_ = std::numeric_limits<double>::infinity();
	control_msg_ = GvtControlMessage();
	// TODO: self.waiting = 0
	checkpoint_restored_ = true;

	std::vector<std::shared_ptr<Tracer> > tracerlist = tracers_.GetTracers();
	tracers_ = Tracers();

	setGlobals(loglevel_, checkpoint_frequency_, checkpoint_name_, statesaver_, nKernels_, tracerlist);

	if (is_flattened_)
		model_->UnflattenConnections();

	message_sent_count_ = 0;
	message_received_count_ = 0;
	// TODO: prior stuff
	input_message_queue_ = std::deque<Message *>();
	output_message_queue_.clear();

	message_input_scheduler_ = MessageScheduler();
	actions_.clear();
	vlock_.lock();
	Revert(DevsTime(gvt_, 0));
	vlock_.unlock();
}

/**
 * A small wrapper around the simulate() function,
 * though with a different name to allow a much simpler MPI one way check
 */
void BaseSimulator::SimulateSync()
{
	Simulate();
}

/**
 * Simulate CURRENTLY USELESS
 */
void BaseSimulator::Simulate()
{
	// TODO: David, checkt gij dit? Conditie moet true zijn normaal gezien... - Ian
	//if (!simlock_.owns_lock() && !simlock_.try_lock()) {
	// We already had the lock, so normal simulation
	// Send the init message
	if (gvt_ == -std::numeric_limits<double>::infinity()) {
		// To make sure that the GVT algorithm won't start already and see that this
		// model has nothing to simulate

		DevsTime new_time_next;
		model_->setTimeNext(new_time_next);
		CoupledInitialize();
	}
	//}

	// release lock
	simlock_.unlock();

	has_simulation_finished_ = false;

	while (true) {
		RunSimulation();

		if (is_simulation_irreversible_) {
			// TODO should_run_.unlock();
			break;
		}
		// TODO should_run_cv_.wait(should_run_);
		// TODO should_run_.unlock();

		if (has_simulation_finished_)
			break;
	}
	sim_finish_lock_.lock();
}

/**
 * Run a complete simulation run.
 * Can be run multiple times
 */
void BaseSimulator::RunSimulation()
{
	while (true) {
		if (!input_message_queue_.empty()) {
			simlock_.lock();
			//TODO Vlock
			ProcessIncomingMessages();
			simlock_.unlock();
		}

		while (simlock_request_) {
			std::this_thread::sleep_for(std::chrono::microseconds(10));
		}

		// acquire lock
		simlock_.lock();

		if (Check()) {
			has_prevtime_finished_ = true;
			has_simulation_finished_ = true;

			simlock_.unlock();
			break;
		}

		DevsTime time_next; //TODO: ???
		// Check the external-thread messages only if there is a possibility for them to arrive
		// This is a slight optimization for local simulation

		if (!is_simulation_irreversible_)
			time_next = ProcessMessage(model_->getTimeNext());
		else
			time_next = model_->getTimeNext();

		if (time_next.getX() == std::numeric_limits<double>::infinity()) {
			transitioning_.clear();
			has_prevtime_finished_ = true;
			has_simulation_finished_ = true;

			simlock_.unlock();

			break;
		}

		// Round of the current clock time, which is necessary for revertions later on
		current_clock_.setX(time_next.getX());
		current_clock_.setY(time_next.getY());

		// TODO: mutex
		std::list<std::shared_ptr<AtomicDevs>> reschedule;
		if (use_classic_devs_)
			reschedule = CoupledOutputGenerationClassic(current_clock_);
		else
			reschedule = CoupledOutputGeneration(current_clock_);

		MassAtomicTransitions(transitioning_, current_clock_);
		model_->GetScheduler()->MassReschedule(reschedule);


		// TODO: mutex
		model_->SetTimeNext();
		transitioning_.clear();

		if(!output_message_queue_.empty())
			block_outgoing_ = output_message_queue_.back()->getTimeStamp();
		else
			block_outgoing_ = DevsTime(-1, -1);

		prev_time_ = current_clock_;
		clock_ = model_->getTimeNext();

		// release lock
		simlock_.unlock();
	}
}

/**
 * Sets an attribute of a model.
 *
 * @param model_id: the id of the model to alter
 * @param attr: string representation of the attribute to alter
 * @param value: value to set
 */
void BaseSimulator::SetAttributes(int model_id, std::string attribute, int value)
{
	//TODO do we really need to do this? We'll most likely just use setters directly...
}

/**
 * Sets an attribute of the state of a model
 *
 * @param model_id: the id of the model to alter
 * @param attr: string representation of the attribute to alter
 * @param value: value to set
 */
void BaseSimulator::SetStateAttributes(int model_id, std::string attribute, int value)
{
	//TODO do we really need to do this? We'll most likely just use setters directly...
}

/**
 * Start all tracers
 */
void BaseSimulator::StartTracers(bool recover)
{
	tracers_.StartTracers(recover);
}

/**
 * Stop all tracers
 */
void BaseSimulator::StopTracers()
{
	tracers_.StopTracers();
}

/**
 * Return the current time of this kernel
 *
 * @return the current simulation time
 */
double BaseSimulator::GetTime()
{
	return prev_time_.getX();
}

/**
 * Gets the state of a model at a specific time
 *
 * @param model_id: model_id of which the state should be fetched
 * @param request_time: time of the state
 */
StateBase const& BaseSimulator::GetStateAtTime(int model_id, DevsTime request_time)
{
	return FindDevsModelById(model_id)->getState(request_time, false);
}

/**
 * Create a unique enough ID for a message
 *
 * @return a unique string for the specific name and number of sent messages
 */
std::string BaseSimulator::GenerateUuid()
{
	std::stringstream ss;
	message_sent_count_ += 1;

	ss << id_ << "-" << message_sent_count_;

	return ss.str();
}

/**
 * Returns the activity for a certain model id from the previous iteration
 *
 * @param model_id: the model_id to check
 * @return the activity
 */
float BaseSimulator::GetActivity(int model_id)
{
	if (activities_.find(model_id) != activities_.end())
		return activities_[model_id];
	return 0.0f;
}

/**
 * Returns the complete dictionary of all activities
 *
 * @return mapping of all activities
 */
std::map<int, float> BaseSimulator::GetCompleteActivity()
{
	return activities_;
}

/**
 * Returns a dictionary containing the total activity through the complete simulation run
 *
 * @return mapping of all activities, but simulation-wide
 */
std::map<int, float> BaseSimulator::GetTotalActivity()
{
	DevsTime default_time;
	default_time.setX(std::numeric_limits<double>::infinity());
	default_time.setY(std::numeric_limits<double>::infinity());

	return GetTotalActivity(default_time);
}

/**
 * Returns a dictionary containing the total activity through the complete simulation run
 *
 * @param time time up to which to return activity
 * @return mapping of all activities, but simulation-wide
 */
std::map<int, float> BaseSimulator::GetTotalActivity(DevsTime at_time)
{
	if (!is_simulation_irreversible_) {
		std::map<int, float> activities;

		model_->FetchActivity(at_time, activities);
		activities.insert(total_activities_.begin(), total_activities_.end());

		return activities;
	}

	return total_activities_;
}

/**
 * \brief Returns as soon as all messages to this simulation kernel are received.
 * Needed due to Mattern's algorithm. Uses events to prevent busy looping.
 *
 * @param vector The vector number to wait for. Should be 0 for colors kWhite1
 * and kRed1, should be 1 for kWhite2 and kRed2.
 * @return False when done.
 */
bool BaseSimulator::WaitUntilOK(unsigned vector)
{
	std::map<unsigned, unsigned>& v = colorVectors_[(Color)vector];
	int sent = (v.find(id_) == v.end()) ? 0 : v.at(id_);
	int received = (control_msg_.z.find(id_) == control_msg_.z.end()) ? 0 : control_msg_.z.at(id_);
	while (sent + received > 0) {
		vlock_.unlock();
		vchange_cv_.at(vector).wait(vchange_lock_.at(vector));
		vchange_lock_.at(vector).unlock();
		vlock_.lock();

		sent = (v.find(id_) == v.end()) ? 0 : v.at(id_);
		received = (control_msg_.z.find(id_) == control_msg_.z.end()) ? 0 : control_msg_.z.at(id_);
	}
	return false;
}

/**
 * Receive a GVT control message and process it. Method will block until the GVT is actually found, so make this an asynchronous call, or run it on a seperate thread.
 * This code implements Mattern's algorithm with a slight modification: it uses 4 different colours to distinguish two subsequent runs. Furthermore, it always requires 2 complete passes before a GVT is found.
 *
 * @param message The GVT control message
 * @param first Flag determining whether this is the first pass
 */
void BaseSimulator::ReceiveControl(GvtControlMessage message, bool first)
{
	control_msg_ = message;
	double m_clock = control_msg_.x;
	double m_send = control_msg_.y;
	std::map<unsigned, int> waiting_vector = control_msg_.z;
	std::map<unsigned, int> accumulating_vector = control_msg_.q;

	vlock_.lock();

	Color prevcolor = (color_ == Color::kWhite1) ? Color::kRed2 : static_cast<Color>(color_ - 1);
	Color color = color_;
	bool finished = (!id_ && !first && (color == Color::kWhite1 || color == Color::kWhite2));
	if (!id_ && !first) {
		bool bug = false;
		for (auto& pair : waiting_vector) {
			if (pair.second != 0) {
				bug = true;
				break;
			}
		}
		if (bug)
			throw LogicDevsException("GVT bug detected");
		waiting_vector = accumulating_vector;
		accumulating_vector.clear();
	}
	if (finished) {
		double gvt = std::floor(std::min(m_clock, m_send));
		if (gvt < gvt_)
			throw RuntimeDevsException("GVT is decreasing");
		accumulator_ = waiting_vector;
		// relocation ommited
		SetGvt(gvt, std::vector<int>(), false); // TODO: last state only?
		return;
	} else {
		WaitUntilOK(prevcolor);
		waiting_vector.insert(colorVectors_.at(prevcolor).begin(), colorVectors_.at(prevcolor).end());
		accumulating_vector.insert(colorVectors_.at(color).begin(), colorVectors_.at(color).end());
		colorVectors_.at(prevcolor).clear();
		colorVectors_.at(color).clear();
		double localtime =
		        (has_prevtime_finished_) ? std::numeric_limits<double>::infinity() : prev_time_.getX();
		double ntime = id_ ? std::min(m_clock, localtime) : localtime;
		message = GvtControlMessage();
		message.x = ntime;
		message.y = std::min(m_send, tmin_);
		message.z = waiting_vector;
		message.q = accumulating_vector;
		tmin_ = std::numeric_limits<double>::infinity();
	}
	color_ = static_cast<Color>((color_ + 1) % 4);

	vlock_.unlock();

	next_LP_->ReceiveControl(message);
}

/**
 * Sets whether we should be doing activity tracking
 *
 * @param activity_tracking Determines whether we should do activity tracking
 */
void BaseSimulator::SetActivityTracking(bool activity_tracking)
{
	has_activity_tracking_ = activity_tracking;
}

/**
 * Destructor
 */
BaseSimulator::~BaseSimulator()
{
}
}
