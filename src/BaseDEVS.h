/*
 * BaseDEVS.h
 *
 *  Created on: Feb 23, 2015
 *      Author: paul
 */

#ifndef BASEDEVS_H_
#define BASEDEVS_H_

#include <ctime>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <memory>

#include "Port.h"
#include "VCDVariable.h"
#include "utility.h"
#include "Predefines.h"

/**
 * Load the correct header files for the used compiler
 */
#if defined(PREDEF_COMPILER_VISUALC) // Microsoft compiler
    #include <windows.h>
#elif defined(PREDEF_COMPILER_GCC) // GNU compiler or Mac OS X
    #include <dlfcn.h>
#else
	#error Your compiler is not supported
#endif

namespace cdevs {
class Simulator;

/**
 Abstract base class for AtomicDEVS and CoupledDEVS classes.

 This class provides basic DEVS attributes and query/set methods.
 */
class BaseDevs
{
public:
	friend class SchedulerML;
	BaseDevs(std::string name);
	virtual ~BaseDevs();

	void SimSettings(Simulator const&);

	bool ModelTransition(StateBase const&);

	void SetParent(const std::weak_ptr<BaseDevs> parent);
	inline const std::weak_ptr<BaseDevs> GetParent() const { return parent_; };

	std::vector<VcdVariable> GetVcdVariables();

	std::weak_ptr<Port> AddPort(std::string, bool);
	void RemovePort(std::weak_ptr<Port>);
	std::weak_ptr<Port> AddInPort(std::string);
	std::weak_ptr<Port> AddOutPort(std::string);

	const virtual std::string GetModelName();
	const std::string GetModelFullName();

	void SetModelID(int id);
	int GetModelID() const;

	DevsTime getTimeLast() const;
	void setTimeLast(const DevsTime& timeLast);
	DevsTime getTimeNext() const;
	void setTimeNext(const DevsTime& timeNext);
	bool operator==(const BaseDevs& rhs);

	void setTimeElapsed(double timeElapsed);

	void setHandle(void* h);
	void* getHandle() const;

	static std::shared_ptr<BaseDevs> load(std::string path);

	const std::vector<std::shared_ptr<Port>>& getInputPorts()
	{
		return input_ports_;
	}

	const std::vector<std::shared_ptr<Port>>& getOutputPorts()
	{
		return output_ports_;
	}

	double getTimeElapsed();

	virtual void FlattenConnections();
	virtual void UnflattenConnections();
	virtual void SetLocation(int location, bool force) = 0;
	inline int GetLocation() const { return location_; };
	virtual int GetModelLoad(std::map<int, double> loads) = 0;
	virtual int Finalize(std::string name, int model_counter,
		std::map<int, std::shared_ptr<BaseDevs>>& model_ids, std::list<std::shared_ptr<BaseDevs>> select_hierarchy) = 0;

	bool isCoupled() const
	{
		return is_coupled_;
	}

	const std::string& getFullName() const {
		return full_name_;
	}

	void setFullName(const std::string& fullName) {
		full_name_ = fullName;
	}

	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > my_output_;
	std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > my_input_;

	static int basedevs_counter;

	virtual std::shared_ptr<BaseDevs> getPtr() = 0; // { return this->shared_from_this(); };

protected:
	bool is_coupled_;

private:
	void* handle_;
protected:
	std::string name_;
	std::string full_name_;
	int model_id_;
	std::weak_ptr<BaseDevs> parent_;

	std::vector<std::shared_ptr<Port> > input_ports_;
	std::vector<std::shared_ptr<Port> > output_ports_;
	// std::vector<std::shared_ptr<Port>> _ports;

	DevsTime time_last_;
	DevsTime time_next_;

	int location_;
	std::vector<StateBase*> _old_states;
	unsigned num_children_;

	double elapsed_;
};

template<class Base, class Derived>
class DevsCRTP : public Base, public std::enable_shared_from_this<Derived>
{
public:
	using Base::Base;

	std::shared_ptr<BaseDevs> getPtr()
	{
		return std::enable_shared_from_this<Derived>::shared_from_this();
	}

	/////////////////////////////////////////////////////////
	// CRTP style model factory methods
	/////////////////////////////////////////////////////////
	template<typename ... T>
	static std::shared_ptr<Derived> create( T&& ... all)
	{
		std::shared_ptr<Derived> model = std::shared_ptr<Derived>( new Derived( std::forward<T>(all)... ));
		std::shared_ptr<BaseDevs> m = model->getPtr(); // std::static_pointer_cast<BaseDevs>(model);
		for (auto& p : m->getInputPorts())
			p->SetHostDEVS(m);
		for (auto& p : m->getOutputPorts())
			p->SetHostDEVS(m);

		return model;
	}
};

/*template<class Derived, typename ... T>
std::shared_ptr<BaseDevs> Devs::create( T&& ... all)
{
}*/

} /* namespace ns_DEVS */


#endif /* BASEDEVS_H_ */
