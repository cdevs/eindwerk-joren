#include "Simulator.h"
#include <limits>

namespace cdevs
{
/**
 *  \brief Constructor of the simulator.
 *
 *  TODO: Actually construct a RootDEVS from the given BaseDEVS.
 *
 *  @param model: a valid model (created with the provided functions)
 */
Simulator::Simulator(std::shared_ptr<BaseDevs> model, unsigned n_kernels)
{
	// allocator/controller
	allocator_ = 0;

	// simulation - general
	has_activity_tracking_ = false;
	has_location_tracer_ = false;
	setup_ = false;

	// simulation - progress
	is_in_progress_ = false;
	progress_finished_ = false;

	// simulation - time
	gvt_interval_ = 1;
	termination_time_ = std::numeric_limits<double>::infinity();

	// checkpoint
	checkpoint_interval_ = 1;
	checkpoint_name_ = "(none)";

	// model
	model_ = model;
	is_coupled_devs_ = false;
	controller_ = new Controller(NULL, n_kernels);
	controller_->SetClassicDevs(false);

	//model_->Finalize("",0.0,this->model_ids_,)

	is_flattened_ = false;

	draw_model_hierarchy_ = false;

	n_kernels_ = n_kernels;
}

void Simulator::GetAttribute()
{
}

/**
 * Perform startup of the simulator right before simulation
 */
void Simulator::RunStartup()
{

	//if (draw_model_hierarchy_)
	//	DrawModelHierarchy("Simulator", model_);
	//TODO Are we implementing Drawing the models?

	if (model_->isCoupled()) {
		std::shared_ptr<CoupledDevs> coupled_model = std::dynamic_pointer_cast<CoupledDevs>(model_);
		coupled_model->SetComponentSet(coupled_model->DirectConnect());
	} else {
		for (auto& port : model_->getInputPorts()) {
			port->routing_outline_.clear();
			port->routing_inline_.clear();
		}

		for (auto& port : model_->getOutputPorts()) {
			port->routing_outline_.clear();
			port->routing_inline_.clear();
		}
	}

	this->setup_ = true;

	/*
	 if (allocator_ != 0 && allocator_->GetTerminationTime() == 0) {
	 //TODO
	 std::list<std::shared_ptr<BaseDevs>> alloc_models =
	 model_->isCoupled() ?
	 ((std::shared_ptr<CoupledDevs>) model_)->GetComponentSet() :
	 std::list<std::shared_ptr<BaseDevs>>(1, model_);
	 std::map<int, int> allocations = allocator_->Allocate(alloc_models,
	 Graph(), 1, std::map<int, float>());
	 }

	 if (draw_model_hierarchy_ && allocator_ != 0) {
	 //TODO
	 //DrawModelConnections("test.txt", 0, std::vector<Colors>());
	 }
	 */

	if(!tracers_.empty())
	{
		for(auto& tracer : tracers_)
		{
			controller_->RegisterTracer(tracer, false);
		}
	}
}

void Simulator::DrawModelHierarchy(std::string out_filename, std::shared_ptr<BaseDevs> model)
{
}

void Simulator::DrawModelConnections(std::string out_filename, BaseDevs& model,
        std::vector<Color> colors)
{
}

/**
 * Create a checkpoint of this object
 */
void Simulator::Checkpoint()
{
	// form file name string
	std::stringstream checkpoint_name_stream;
	checkpoint_name_stream << checkpoint_name_ << "_SIM.pdc";

	// create file stream
	std::ofstream dump_stream(checkpoint_name_stream.str());

	if (is_flattened_) {
		model_->FlattenConnections();
		//TODO: serialize
		model_->UnflattenConnections();
	} else {
		//TODO: serialize
	}

	dump_stream.close();
}

/**
 * Alert the Simulator that it was restored from a checkpoint
 * and thus can take some shortcuts
 */
void Simulator::LoadCheckpoint()
{
	RealSimulate();
}

/**
 * \brief Load a previously created simulation from a saved checkpoint.
 *
 * @param name 	The name of the model to provide some distinction between different simulation models
 * @returns	Either None if no recovery was possible, or the Simulator object after simulation
 */
void Simulator::LoadCheckpoint(std::string name)
{
	// TODO: we will temporarily make use of only one file
	// open .pdc file
	std::ifstream checkpoint_filestream(name + "_SIM.pdc");

	// if the file is empty, return immediately
	if(checkpoint_filestream.eof())
		throw RuntimeDevsException("Could not load " + name + "_SIM.pdc. File does not exist!");

	double max_gvt = 0.0;

}

void Simulator::StartAllocator()
{
	if (allocator_ != 0) {
		has_activity_tracking_ = true;

		// TODO: ??? multi thread simulation to main 'relocation' ???
	}

}

/**
 * Reinitialize the model, so that a new *simulate()* call will restart the simulation anew.
 * This is possible in both local and distributed simulation,
 * though it requires the *setAllowLocalReinit* method to be called first if you are running local simulation.
 * The additional method call is required as allowing reinitialisation requires the complete model to be saved twice (a reinit copy and a working copy).
 *
 * **Do NOT call this method directly, but call it through the simconfig file**
 */
void Simulator::Reinitialize()
{
}

/**
 * Modify the state of a specific model.
 *
 * **Do NOT call this method directly, but call it through the simconfig interface**
 *
 * @param model_id: the model_id of the model to modify the state from
 * @param state: the state to configure
 */
void Simulator::ModifyState(int model_id, StateBase* state)
{
}

/**
 * Start simulation with the previously set options. Can be reran afterwards to continue the simulation of
 * the model (or reinit it first and restart simulation), possibly after altering some aspects of the model with the provided methods.
 */
void Simulator::Simulate()
{
	if (!this->setup_) {
		this->RunStartup();
		std::list<std::shared_ptr<AtomicDevs> > models;

//		for (auto& atomic : model_ids_) {
//			std::shared_ptr<AtomicDevs> adevs = dynamic_cast<std::shared_ptr<AtomicDevs>>(atomic.second);
//			if (!adevs) {
//				throw RuntimeDevsException("Not an AtomicDevs!");
//			}
//			models.push_back(adevs);
//		}
		if(model_->isCoupled())
		{
			std::shared_ptr<CoupledDevs> coupled_model = std::dynamic_pointer_cast<CoupledDevs>(model_);
			for (auto& component : coupled_model->GetComponentSet())
				models.push_back(std::dynamic_pointer_cast<AtomicDevs>(component));
		} else {
			models.push_back(std::dynamic_pointer_cast<AtomicDevs>(model_));
		}

		is_flattened_ = false;
		controller_->BroadcastModel(model_, models,  SchedulerType::kMlSchedulerType, is_flattened_);
		//Prevent further startup.
		this->setup_ = true;
		controller_->StartTracers(false);
	}else{
		controller_->StartTracers(true);
	}

	controller_->SetAllocator(allocator_);
	controller_->SetActivityTracking(has_activity_tracking_);
	controller_->setGlobals(0, 0, std::string(""), NULL, std::vector<std::shared_ptr<Tracer>>());
	//TODO

	//tracers_.clear();

	if (this->termination_condition_ != 0) {
		controller_->SetTerminationCondition(this->termination_condition_);
	} else {
		controller_->SetTerminationTime(
		        DevsTime(this->termination_time_, std::numeric_limits<double>::infinity()));
	}
	if (checkpoint_interval_ > 0)
		Checkpoint();

	this->RealSimulate();

	this->controller_->StopTracers();
}

/**
 * The actual simulation part, this is identical for the 'start from scratch' and 'start from checkpoint' algorithm
 * thus it was split up
 */
void Simulator::RealSimulate()
{
	// check if we are in progress already
	/*	if (is_in_progress_) {
	 std::cout << "Simulation In progress" << std::endl;
	 // check if we have a valid termination time
	 if (termination_time_ == std::numeric_limits<double>::infinity())
	 is_in_progress_ = false;
	 else {
	 has_progress_finished_ = false;

	 // create a seperate thread to show the progress
	 std::thread progress_thread = std::thread(&Simulator::ShowProgress,
	 this);

	 if (checkpoint_interval_ < 0) {
	 //do mutex stuff here
	 }

	 progress_thread.detach();
	 }
	 }
	 */
	// if we have a checkpoint interval, start the GVT thread
	//if (checkpoint_interval_ > 0)
	//	controller_->StartGvtThread(gvt_interval_);
	/*
	 * Calls the simulation of the BaseSimulator
	 * This is the ACTUAL ACTUAL simulation.
	 *
	 */
	this->controller_->SimulateAll();
	this->controller_->PerformActions();

	this->progress_finished_ = true;
}

void Simulator::RemoveTracers()
{
}

void Simulator::ModifyStateAttribute(int model_id, std::string attribute, int value)
{
}

void Simulator::setClassicDEVS()
{
	controller_->SetClassicDevs(true);
}

void Simulator::setTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)> condition)
{
	termination_condition_ = condition;
}

void Simulator::setTerminationTime(double time)
{
	termination_time_ = time;
	controller_->SetTerminationTime(DevsTime(time, -1)); //TODO set termination time to double?
}

/**
 * Shows the progress of the simulation by polling all locations that are passed. Should run on a seperate thread as this blocks!
 *
 * TODO: REVIEW FOR MULTITHREADING!
 *
 * @param locations: list of all locations to access
 */
void Simulator::ShowProgress()
{
	const int console_width = 80;
	const int console_prefix = 4;
	const int console_suffix = 5;
	int bar_width = console_width - console_prefix - console_suffix;

	double finish_time = termination_time_;
	bool first = true;

	double gvt = 0.0;

	while (true) {
		if (checkpoint_interval_ > -1)
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		else {
			// see RealSimulate Event lock
		}
		std::cout << "." << std::endl;
		// TODO: multi threading!!
		gvt = finish_time;

	}
}

/**
 * Sets the verbosity of our simulator
 *
 * @param filename The name of the file to write to - empty is stdout
 */
void Simulator::setVerbose(std::string filename)
{
	SetTracer(new TracerVerbose(controller_, 0, filename));
}

void Simulator::setXml(std::string filename)
{
	SetTracer(new TracerXml(controller_, 0, filename));
}
/*
void Simulator::setJson(std::string filename, unsigned indent)
{
	SetTracer(new TracerJson(controller_, 0, filename, indent));
}
*/
Simulator::~Simulator()
{
	delete controller_;
}

void Simulator::SetTracer(Tracer* tracer)
{
	tracers_.push_back(std::shared_ptr<Tracer>(tracer));
}
} /* namespace cdevs */
