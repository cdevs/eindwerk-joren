#include "DevsTime.h"

namespace cdevs
{
/**
 * Constructor
 */
DevsTime::DevsTime()
{
	x_ = 0;
	y_ = 0;
}

/**
 * Constructor
 *
 * @param x The time
 * @param y The age
 */
DevsTime::DevsTime(double x, double y)
{
	x_ = x;
	y_ = y;
}

/**
 * \brief Gets the time
 *
 * @return The time
 */
double DevsTime::getX() const
{
	return x_;
}

/**
 * \brief Sets the time
 *
 * @param x The time to set
 */
void DevsTime::setX(double x)
{
	x_ = x;
}

/**
 * \brief Gets the age
 *
 * @return The age
 */
int DevsTime::getY() const
{
	return y_;
}

/**
 * \brief Sets the time
 *
 * @param x The age to set
 */
void DevsTime::setY(int y)
{
	y_ = y;
}

/**
 * \brief Smaller-than operator
 *
 * @param g The DevsTime to compare to
 * @return True if smaller than, false otherwise
 */
bool DevsTime::operator <(const DevsTime& g) const
{
	return ((*this).getX() < g.getX());
}

/**
 * \brief Smaller-than-or-equal-to operator
 *
 * @param g The DevsTime to compare to
 * @return True if smaller than or equal to, false otherwise
 */
bool DevsTime::operator <=(const DevsTime& g) const
{
	return ((*this).getX() <= g.getX());
}

/**
 * \brief Greater-than-or-equal-to operator
 *
 * @param g The DevsTime to compare to
 * @return True if greater than or equal to, false otherwise
 */
bool DevsTime::operator >=(const DevsTime& g) const
{
	return ((*this).getX() >= g.getX());
}

/**
 * \brief Assign-to operator
 *
 * @param g The DevsTime to assign to
 */
void DevsTime::operator =(const DevsTime& d)
{
	(*this).setX(d.getX());
	(*this).setY(d.getY());
}

/**
 * \brief Greater-than operator
 *
 * @param g The DevsTime to compare to
 * @return True if greater than, false otherwise
 */
bool DevsTime::operator >(const DevsTime& g) const
{
	return ((*this).getX() > g.getX());
}

/**
 * \brief Equal-to operator
 *
 * @param g The DevsTime to compare to
 * @return True if equal to, false otherwise
 */
bool DevsTime::operator ==(const DevsTime& g) const
{
	return ((*this).getX() == g.getX());
}

/**
 * \brief Converts DevsTime to string
 *
 * Converts the time parameter to a string object
 *
 * @return The converted DevsTime
 */
std::string DevsTime::string() const
{
	return std::to_string(x_);
}
} /* namespace cdevs */
