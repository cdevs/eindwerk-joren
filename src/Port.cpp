#include "Port.h"
#include "BaseDEVS.h"

namespace cdevs {

Port::Port(bool is_input, std::string name)
{
	this->is_inport_ = is_input;
	this->name_ = name;
	msg_count_ = 0;
}


const std::string Port::GetPortName() const
{
	return this->name_;
}


const std::string Port::GetFullPortName() const
{
	return this->name_;
}

void Port::SetHostDEVS(std::weak_ptr<BaseDevs> model)
{
	host_DEVS = model;
}

void Port::ResetHostDEVS()
{
	host_DEVS.reset();
}

Port::~Port()
{
}

} /* namespace ns_DEVS */
