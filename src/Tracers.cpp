#include "Tracers.h"

namespace cdevs
{
Tracers::Tracers()
{
	uid_ = 0;
}

Tracers::~Tracers()
{
}

void Tracers::RegisterTracer(std::shared_ptr<Tracer> tracer, bool recover)
{
	this->tracers_.push_back(tracer);
	this->tracers_init_.push_back(tracer);
	this->uid_++;
	// tracer->StartTracer(recover);
}

bool Tracers::HasTracers() const
{
	return this->tracers_.size() > 0;
}

std::shared_ptr<Tracer> Tracers::GetById(int uid) const
{
	return this->tracers_[uid];
}

std::vector<std::shared_ptr<Tracer> > Tracers::GetTracers() const
{
	return tracers_;
}

void Tracers::StopTracers()
{
	for (auto& tracer : this->tracers_) {
		tracer->StopTracer();
	}
}

void Tracers::TracesInitialize(std::shared_ptr<AtomicDevs> devs, DevsTime t)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceInitialize(devs, t);
	}
}

void Tracers::TracesInternal(std::shared_ptr<AtomicDevs> devs)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceInternal(devs);
	}
}

void Tracers::TracesExternal(std::shared_ptr<AtomicDevs> devs)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceExternal(devs);
	}
}

void Tracers::TracesConfluent(std::shared_ptr<AtomicDevs> devs)
{
	for (auto& tracer : this->tracers_) {
		tracer->TraceConfluent(devs);
	}
}

void Tracers::Reset()
{
	tracers_.clear();
}

void Tracers::StartTracers(bool recover)
{
	for(auto tracer: tracers_){
		tracer->StartTracer(recover);
	}
}
/*
void Tracers::Trace(DevsTime time, std::string text)
{
	for(auto& tracer : this->tracers_)
	{
		tracer->Trace(time, text);
	}
}
*/
} /* namespace cdevs */
