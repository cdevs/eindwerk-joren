#ifndef STATESAVER_H_
#define STATESAVER_H_

#include "BaseDEVS.h"

namespace cdevs {

class StateSaver
{
public:
	StateSaver();
	virtual ~StateSaver();
	std::shared_ptr<BaseDevs> LoadState(); //TODO: :returns: state - copy of the state that was saved
};

}

#endif /* STATESAVER_H_ */
