#ifndef ACTIVITYVISUALISATION_H_
#define ACTIVITYVISUALISATION_H_

#include "BaseSimulator.h"
#include "Simulator.h"
#include <istream>
#include <list>
#include <string>

namespace cdevs {

/**
 * Visualize the locations in a Cell DEVS way
 * @param kernel a basesimulator object, to fetch the location of every model
 */
void VisualizeLocations(const BaseSimulator &kernel);

/**
 * Visualize the activity in a Cell DEVS way
 * @param sim the simulator object, to access the model and their activity
 */
void VisualizeActivity(const Simulator &sim);

/**
 * Perform the actual visualisation in a matrix style
 * @param matrix the 2D matrix to visualize, should be a list of lists
 * @param formatstring the string to use to format the values, most likely something like "%f"
 * @param filename of the file to write the matrix to.
 */
void VisualizeMatrix(const std::list<std::list<void> > &matrix, std::string formatstring, std::string filename);

/**
 * Perform the actual visualisation in a matrix style
 * @param matrix the 2D matrix to visualize, should be a list of lists
 * @param formatstring the string to use to format the values, most likely something like "%f"
 * @param file stream to write the matrix to.
 */
void VisualizeMatrix(const std::list<std::list<void> > &matrix, std::string formatstring, std::istream &filename);

} /* namespace ns_DEVS */

#endif /* ACTIVITYVISUALISATION_H_ */
