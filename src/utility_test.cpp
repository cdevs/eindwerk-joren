/*
 * utility_test.cpp
 *
 *  Created on: 03 Apr 2015
 *      Author: jweiren
 */

#include "utility.h"

#include <limits.h>
#include "gtest/gtest.h"


class DevsTimeTest : public ::testing::Test {
protected:
  virtual void SetUp() {
  }
  virtual void TearDown() {
    // Code here will be called immediately after each test
    // (right before the destructor).
  }
};

TEST_F(DevsTimeTest,gettersandsetters)
/*
 * Tests basic functionality of the getters and setters
 */
{
	cdevs::DevsTime time;
	time.setX(4);
	EXPECT_EQ(time.getX(), 4);
	time.setY(4);
	EXPECT_EQ(time.getY(), 4);
	time.setX(5);
	EXPECT_EQ(time.getX(), 5);
}

TEST_F(DevsTimeTest, operatortests)
/*
 * Tests the implementation of the operators
 */
{
	cdevs::DevsTime time1;
	cdevs::DevsTime time2;
	time1.setX(4);
	time2.setX(5);
	time1.setY(4);
	time2.setY(5);

	EXPECT_TRUE(time1 < time2);
	EXPECT_FALSE(time1 > time2);
	EXPECT_FALSE(time1 == time2);
	time1 = time2;
	EXPECT_TRUE(time1 == time2);
}

TEST_F(DevsTimeTest, stringtest)
/*
 * Tests the implementation of the string() function
 */
{
	cdevs::DevsTime time;
	time.setX(4);
	double fourd = 4;
	std::string four = std::to_string(fourd);
	ASSERT_STREQ(time.string().c_str(), four.c_str());
}




