/*
 * model.h
 *
 *  Created on: 7-mei-2015
 *      Author: david
 */

#ifndef EXAMPLES_TRAFFICLIGHT_CLASSIC_MODEL_H_
#define EXAMPLES_TRAFFICLIGHT_CLASSIC_MODEL_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"

namespace cdevs_examples
{
namespace parallel
{
class TrafficLightMode: public cdevs::State<std::string, TrafficLightMode>
{
public:
	TrafficLightMode(std::string value = "red");
	std::string toXML() const {
		return "<color>"+getValue()+"</color>\n";
	}
};

class TrafficLight: public cdevs::Atomic<TrafficLight, TrafficLightMode>
{
public:
	TrafficLight(std::string name);

	TrafficLightMode const& ExtTransition(std::map<std::weak_ptr<cdevs::Port>, std::list< std::string>, std::owner_less<std::weak_ptr<cdevs::Port> > > inputs);

	TrafficLightMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::string>, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> INTERRUPT;
	std::weak_ptr<cdevs::Port> OBSERVED;
private:
	const TrafficLightMode state_red_ = TrafficLightMode("red");
	const TrafficLightMode state_green_ = TrafficLightMode("green");
	const TrafficLightMode state_yellow_ = TrafficLightMode("yellow");
	const TrafficLightMode state_manual_ = TrafficLightMode("manual");
};

class PolicemanMode : public cdevs::State<std::string, PolicemanMode>
{
public:
	PolicemanMode(std::string value = "idle");
	std::string toXML() const {
		return "<state>"+getValue()+"</state>\n";
	}
};

class Policeman : public cdevs::Atomic<Policeman, PolicemanMode>
{
public:
	Policeman(std::string name);

	PolicemanMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::string>, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> OUT;
private:
	const PolicemanMode state_idle_ = PolicemanMode("idle");
	const PolicemanMode state_working_ = PolicemanMode("working");
};

class TrafficSystem: public cdevs::Coupled<TrafficSystem>
{
public:
	TrafficSystem(std::string name);

	std::shared_ptr<Policeman> getPoliceMan() const;
	std::shared_ptr<TrafficLight> getTrafficLight() const;
private:
	std::shared_ptr<Policeman> policeman;
	std::shared_ptr<TrafficLight> trafficlight;
};

}
}

#endif /* EXAMPLES_TRAFFICLIGHT_CLASSIC_MODEL_H_ */
