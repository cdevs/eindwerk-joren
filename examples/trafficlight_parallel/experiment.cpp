/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"
#include <iostream>

using namespace cdevs;
using namespace cdevs_examples::parallel;

bool TerminateWhenStateIsReached(DevsTime clock, std::shared_ptr<BaseDevs> model)
{
	std::shared_ptr<TrafficLight> tl = std::dynamic_pointer_cast<TrafficLight>(model);
	if (tl) {

		return (tl->getState().getValue() == "manual");
	}
	return false;
}

int main(int argc, char * argv[]) {
	std::shared_ptr<TrafficSystem> model = TrafficSystem::create("trafficSystem");

	Simulator sim(model, 2);

	// TODO: give termination time priority to condition?
	// sim.setTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)>(TerminateWhenStateIsReached));

	sim.setTerminationTime(400.0);

	sim.setVerbose();

	sim.Simulate();
	
	std::cout << "\nSimulation terminated with traffic light in state " << model->getTrafficLight()->getState().getValue() << std::endl;

	return 0;
}
