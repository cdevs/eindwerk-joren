/*
 * AtomicModelTest.cpp
 *
 *  Created on: 31-mrt.-2015
 *      Author: david
 */
#include "model.h"
#include <algorithm>
#include <limits>
#include <iostream>

using namespace cdevs;

namespace cdevs_examples
{
namespace parallel
{
TrafficLightMode::TrafficLightMode(std::string value) :
		State(value) {
}

TrafficLight::TrafficLight(std::string name) :
		Atomic(name) {
	setState( state_red_ );
	elapsed_ = 1.5;

	INTERRUPT = AddInPort("INTERRUPT");
	OBSERVED = AddOutPort("OBSERVED");
}

TrafficLightMode const& TrafficLight::ExtTransition(
		std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs) {
	std::string input = inputs.at(INTERRUPT).front();

	std::string state = getState().getValue();

	if (input == "toManual") {
		if (state == "manual") {
			return state_manual_;
		} else if (state == "red" || state == "green" || state == "yellow") {
			return state_manual_;
		}
	} else if (input == "toAutonomous") {
		if (state == "manual") {
			return state_red_;
		} else if (state == "red" || state == "green" || state == "yellow") {
			return getState();
		}
	}
	// TODO: throw exception!
}

TrafficLightMode const& TrafficLight::IntTransition() {
	std::string state = getState().getValue();

	if (state == "red")
		return state_green_;
	else if (state == "green")
		return state_yellow_;
	else if (state == "yellow")
		return state_red_;
	// TODO: throw exception!
}

std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > TrafficLight::OutputFunction() {
	std::string state = getState().getValue();

	if (state == "red")
		return { {OBSERVED, {"grey"}}};
	else if (state == "green")
		return { {OBSERVED, {"yellow"}}};
	else if (state == "yellow")
		return { {OBSERVED, {"grey"}}};
	// TODO: throw exception!
}

double TrafficLight::TimeAdvance() {
	std::string state = getState().getValue();

	if (state == "red")
		return 60;
	else if (state == "green")
		return 50;
	else if (state == "yellow")
		return 10;
	else if (state == "manual")
		return std::numeric_limits<double>::infinity();

	// TODO: throw exception!
}

PolicemanMode::PolicemanMode(std::string value) :
		State(value) {
}

Policeman::Policeman(std::string name) :
		Atomic(name) {
	setState( state_idle_ );

	elapsed_ = 0.0;

	OUT = AddOutPort("OUT");
}

PolicemanMode const& Policeman::IntTransition() {
	std::string state = getState().getValue();

	if (state == "idle")
		return state_working_;
	else if (state == "working")
		return state_idle_;
	// TODO: throw exception!
}

std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > Policeman::OutputFunction() {
	std::string state = getState().getValue();

	if (state == "idle")
		return { {OUT, {"toManual"}}};
	else if (state == "working")
		return { {OUT, {"toAutonomous"}}};
	// TODO: throw exception!
}

double Policeman::TimeAdvance() {
	std::string state = getState().getValue();

	if (state == "idle")
		return 200;
	else if (state == "working")
		return 100;
	// TODO: throw exception!
}

TrafficSystem::TrafficSystem(std::string name) :
		Coupled(name) {
	// Declare the coupled model's output ports:
	// Autonomous, so no output ports

	// Declare the coupled model's sub-models:

	// The Policeman generating interrupts
	policeman = Policeman::create("policeman");
	AddSubModel(policeman, 0);

	// The TrafficLight
	trafficlight = TrafficLight::create("trafficlight");
	AddSubModel(trafficlight, 1);

	// Only connect ...
	ConnectPorts(policeman->OUT, trafficlight->INTERRUPT);
}

std::shared_ptr<Policeman> TrafficSystem::getPoliceMan() const
{
	return policeman;
}

std::shared_ptr<TrafficLight> TrafficSystem::getTrafficLight() const
{
	return trafficlight;
}

}
}
