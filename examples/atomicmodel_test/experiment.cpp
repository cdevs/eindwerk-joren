/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"

using namespace cdevs;
using namespace cdevs_examples::atomic;

bool TerminateWhenStateIsReached(DevsTime clock, std::shared_ptr<BaseDevs> model)
{
	std::shared_ptr<TrafficLight> tl = std::dynamic_pointer_cast<TrafficLight>(model);
	if (tl) {
		return (tl->getState().getValue() == "manual");
	}
	return false;
}

int main(int argc, char * argv[]) {
	std::shared_ptr<TrafficLight> model = TrafficLight::create("VerkeersLicht");

	Simulator sim(model);

	sim.setTerminationCondition(std::function<bool(DevsTime, std::shared_ptr<BaseDevs>)>(TerminateWhenStateIsReached));

	sim.setTerminationTime(400.0);

	// sim.setVerbose();

	sim.setClassicDEVS();

	sim.Simulate();

	return 0;
}
