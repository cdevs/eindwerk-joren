/*
 * AtomicModelTest.cpp
 *
 *  Created on: 31-mrt.-2015
 *      Author: david
 */
#include "model.h"
#include <algorithm>
#include <limits>

using namespace cdevs;

namespace cdevs_examples
{
namespace atomic
{
TrafficLightMode::TrafficLightMode(std::string value) :
		State(value) {
}

TrafficLight::TrafficLight(std::string name) :
		Atomic(name) {
	setState( state_red_ );
	elapsed_ = 1.5;

	INTERRUPT = AddInPort("INTERRUPT");
	OBSERVED = AddOutPort("OBSERVED");
}

TrafficLightMode const& TrafficLight::ExtTransition(
		std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > inputs) {
	std::string input = inputs.at(INTERRUPT).front();

	std::string state = getState().getValue();

	if (input == "toManual") {
		if (state == "manual") {
			return state_manual_;
		} else if (state == "red" || state == "green" || state == "yellow") {
			return state_manual_;
		}
	} else if (input == "toAutonomous") {
		if (state == "manual") {
			return state_red_;
		} else if (state == "red" || state == "green" || state == "yellow") {
			return getState();
		}
	}
	// TODO: throw exception!
}

TrafficLightMode const& TrafficLight::IntTransition() {
	std::string state = getState().getValue();

	if (state == "red")
		return state_green_;
	else if (state == "green")
		return state_yellow_;
	else if (state == "yellow")
		return state_red_;
	// TODO: throw exception!
}

std::map<std::weak_ptr<Port>, std::list<std::string>, std::owner_less<std::weak_ptr<Port> > > TrafficLight::OutputFunction() {
	std::string state = getState().getValue();

	if (state == "red")
		return { {OBSERVED, {"grey"}}};
	else if (state == "green")
		return { {OBSERVED, {"yellow"}}};
	else if (state == "yellow")
		return { {OBSERVED, {"grey"}}};
	// TODO: throw exception!
}

double TrafficLight::TimeAdvance() {
	std::string state = getState().getValue();

	if (state == "red")
		return 60;
	else if (state == "green")
		return 50;
	else if (state == "yellow")
		return 10;
	else if (state == "manual")
		return std::numeric_limits<double>::infinity();

	// TODO: throw exception!
}

}
}
