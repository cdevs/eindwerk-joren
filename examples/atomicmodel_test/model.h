/*
 * model.h
 *
 *  Created on: 7-mei-2015
 *      Author: david
 */

#ifndef EXAMPLES_ATOMICMODEL_TEST_MODEL_H_
#define EXAMPLES_ATOMICMODEL_TEST_MODEL_H_

#include "../../src/AtomicDevs.h"

namespace cdevs_examples
{
namespace atomic
{
class TrafficLightMode: public cdevs::State<std::string, TrafficLightMode>
{
public:
	TrafficLightMode(std::string value = "red");
	std::string toXML() const {
		return "<color>"+getValue()+"</color>\n";
	}
};

class TrafficLight: public cdevs::Atomic<TrafficLight, TrafficLightMode>
{
public:
	TrafficLight(std::string name);

	TrafficLightMode const& ExtTransition(std::map<std::weak_ptr<cdevs::Port>, std::list< std::string>, std::owner_less<std::weak_ptr<cdevs::Port> > > inputs);

	TrafficLightMode const& IntTransition();

	std::map<std::weak_ptr<cdevs::Port>, std::list<std::string>, std::owner_less<std::weak_ptr<cdevs::Port> > > OutputFunction();

	double TimeAdvance();

	std::weak_ptr<cdevs::Port> INTERRUPT;
	std::weak_ptr<cdevs::Port> OBSERVED;
private:
	const TrafficLightMode state_red_ = TrafficLightMode("red");
	const TrafficLightMode state_green_ = TrafficLightMode("green");
	const TrafficLightMode state_yellow_ = TrafficLightMode("yellow");
	const TrafficLightMode state_manual_ = TrafficLightMode("manual");
};

}
}
#endif /* EXAMPLES_ATOMICMODEL_TEST_MODEL_H_ */
